<?php

/**
 * @version $Id$
 * @create 2013/10/13 12:51:32 By xjiujiu
 * @description HongJuZi Framework
 * @copyRight Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

HClass::import('app.base.action.frontaction');

/**
 * 公用控制父类
 *
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @package app.public.action
 * @since 1.0.0
 */
class CmsAction extends BaseAction
{
    protected $_goodsCategory;
    protected $_color;
    protected $_shape;
    protected $_rim;
    protected $_material;
    protected $_goods;

    public function __construct()
    {
        parent::__construct();
        $this->_goodsCategory = HClass::quickLoadModel('goodscategory');
        $this->_color = HClass::quickLoadModel('color');
        $this->_shape = HClass::quickLoadModel('shape');
        $this->_rim   = HClass::quickLoadModel('rim');
        $this->_material = HClass::quickLoadModel('material');
        $this->_goods    = HClass::quickLoadModel('goods');
    }

    public function beforeAction()
    {
        parent::beforeAction();
        $this->_assignArticleCat();
        $this->_assignCateList();
        $this->_assignSiteCfg();
    }

    private function _assignArticleCat()
    {
        $articleCat = HClass::quickLoadModel('articlecat');
        $articleCat->getPopo()->setFieldAttribute('sort_num', 'is_order', 'ASC');
        $list = $articleCat->getAllRows('`shop_id` = 1');
        HResponse::setAttribute('articleCatList', $list);
    }

    protected function _assignRecList()
    {
      $list = $this->_goodsCategory->getAllRowsByFields('`id`,`name`,`image_path`',
          '`status` = 2 AND `is_recommend` = 2 AND `parent_id` > 0');
      HResponse::setAttribute('rec_list', $list);
    }

    protected function _assignCateList()
    {
        $list = $this->_goodsCategory->getAllRowsByFields('`id`,`name`',
            '`status` = 2 AND `parent_id` <= 0');
        $subList = $this->_goodsCategory->getAllRowsByFields('`id`,`name`,`parent_id`',
            '`status` = 2 AND ' . HSqlHelper::whereInByListMap('parent_id', 'id', $list)
        );
        $subarr = array();
        foreach ($subList as $item) {
            $subarr[$item['parent_id']][] = $item;
        }
        foreach ($list as &$item) {
            $item['slist'] = $subarr[$item['id']];
        }

        HResponse::setAttribute('clist', $list);
    }

    private function _assignSiteCfg()
    {
        $information = HClass::quickLoadModel('information');
        $siteCfg = $information->getRecordByWhere('1 = 1');

        $user   = HClass::quickLoadModel('user');
        $record = $user->getRecordByWhere('`parent_id` = 1');
        unset($record['name']);
        HResponse::setAttribute('siteCfg', array_merge($siteCfg, $record));
    }

    protected function _assignColorList()
    {
        $list = $this->_color->getAllRowsByFields(
            '`id`,`name`,`image_path`,`content`',
            '`status` = 2',
            'sort_num ASC'
        );
        HResponse::setAttribute('color_list', $list);
    }

    protected function _assignShapeList()
    {
        $list = $this->_shape->getAllRowsByFields(
            '`id`,`name`,`content`,`image_path`',
            '`status` = 2',
            'sort_num ASC'
        );
        $goodsList = $this->_goods->getAllRowsByFieldsAndGroup(
            '`shape`,count(id) AS total',
            HSqlHelper::whereInByListMap('shape', 'id', $list),
            'shape',
            'total DESC'
        );
        $shapeMap = HArray::turnItemValueAsKey($goodsList, 'shape');
        foreach ($list as &$item) {
            $item['content'] = $shapeMap[$item['id']]['total'] ?: 0;
        }
        HResponse::setAttribute('shape_list', $list);
    }

    protected function _assignRimList()
    {
        $list = $this->_rim->getAllRowsByFields(
            '`id`,`name`,`content`',
            '`status` = 2',
            'sort_num ASC'
        );
        $goodsList = $this->_goods->getAllRowsByFieldsAndGroup(
            '`rim`,count(id) AS total',
            HSqlHelper::whereInByListMap('rim', 'id', $list),
            'rim',
            'total DESC'
        );
        $rimMap = HArray::turnItemValueAsKey($goodsList, 'rim');
        foreach ($list as &$item) {
            $item['content'] = $rimMap[$item['id']]['total'] ?: 0;
        }
        HResponse::setAttribute('rim_list', $list);
    }

    protected function _assignMaterialList()
    {
        $list = $this->_material->getAllRowsByFields(
            '`id`,`name`,`content`',
            '`status` = 2',
            'sort_num ASC'
        );
        HResponse::setAttribute('material_list', $list);
    }

    protected function _getSelectList()
    {
        $this->_getColorTagList();
        $this->_getShapeTagList();
        $this->_getRimTagList();
        $this->_getMaterTagList();
    }

    private function _getColorTagList()
    {
        $color = array_unique(explode(',', I('color')));
        $list = $this->_color->getAllRowsByFields(
            '`id`,`name`',
            HSqlHelper::whereIn('id', $color));
        HResponse::setAttribute('color_tag_list', $list);
    }

    private function _getShapeTagList()
    {
        $color = array_unique(explode(',', I('shape')));
        $list = $this->_shape->getAllRowsByFields(
            '`id`,`name`',
            HSqlHelper::whereIn('id', $color));

        HResponse::setAttribute('shape_tag_list', $list);
    }

    private function _getRimTagList()
    {
        $color = array_unique(explode(',', I('rim')));
        $list = $this->_rim->getAllRowsByFields(
            '`id`,`name`',
            HSqlHelper::whereIn('id', $color));
        HResponse::setAttribute('rim_tag_list', $list);
    }

    private function _getMaterTagList()
    {
        $color = array_unique(explode(',', I('mater')));
        $list = $this->_material->getAllRowsByFields(
            '`id`,`name`',
            HSqlHelper::whereIn('id', $color));
        HResponse::setAttribute('mater_tag_list', $list);
    }

    protected function _assignCurCateList()
    {
        $parentId = HRequest::getParameter('parent_id') ?: 0;
        $cate     = HRequest::getParameter('cate') ?: 0;
        $list     = $this->_goodsCategory->getAllRowsByFields('`id`,`name`,`image_path`',
            '`status` = 2 AND `parent_id` =' . $parentId);
        $curCate  = $this->_goodsCategory->getRecordById($parentId);
        $cateInfo = $this->_goodsCategory->getRecordById($cate);
        HResponse::setAttribute('cate_record', $curCate);
        HResponse::setAttribute('cateInfo', $cateInfo);
        HResponse::setAttribute('sonlist', $list);
    }

}
