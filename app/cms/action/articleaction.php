<?php 

/**
 * @version $Id$
 * @create 2013-8-6 10:20:09 By xjiujiu
 * @description HongJuZi Framework
 * @copyRight Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');
HClass::import('config.popo.articlepopo, app.cms.action.cmsaction, model.articlemodel');
class ArticleAction extends CmsAction
{
    /**
     * 构造函数 
     * 
     * 初始化类里的变量 
     * 
     * @access public
     */
    private $_articleCate;

    public function __construct() 
    {
        parent::__construct();
        $this->_popo    = new ArticlePopo();
        $this->_model   = new ArticleModel($this->_popo);
        $this->_popo->setFieldAttribute('content', 'is_show', true);
        $this->_articleCate = HClass::quickLoadModel('articleCat');
    }

    public function index()
    {
        $id = HRequest::getParameter('id');
        if ($id) {
            $this->_model->incFieldByWhere('total_visits', '`id` = ' . $id, 1);
            $record = $this->_model->getRecordById($id);
            $record_next = $this->_model->getAllRowsByFields('`id`,`name`', '`status` = 2 AND `parent_id` =' .$record['parent_id'].' AND  `id`>'.$id);
            $record_pre = $this->_model->getAllRowsByFields('`id`,`name`', '`status` = 2 AND `parent_id` =' .$record['parent_id'].' AND  `id`<'.$id);
            $this->_assignParentImagePath($record['parent_id']);
            $this->_assignParentName($record['parent_id']);
            HResponse::setAttribute('record', $record);
            HResponse::setAttribute('record_next', $record_next[0]);
            HResponse::setAttribute('record_pre', $record_pre[0]);
            $this->_assignAboutUsList();
            $this->_render('newsview');
        } else {
            $parentId = HRequest::getParameter('parent_id') ?: 0;
            $this->_assignParentImagePath($parentId);
            $this->_assignParentName($parentId);
            $this->_assignModelList('`status` = 2 AND `parent_id` = ' . $parentId);
            $this->_assignNewsList();
            $this->_render('newslist');
        }
    }

    private function _assignNewsList()
    {
        $list = $this->_model->getAllRowsByFields(
            '`id`,`name`,`create_time`',
            '`status` = 2 AND `parent_id` = 737'
        );

        HResponse::setAttribute('news_list', $list);
    }

    private function _assignAboutUsList()
    {
        $list = $this->_model->getAllRowsByFields(
            '`id`,`image_path`',
            '`status` = 2 AND `parent_id` = 738'
        );
        HResponse::setAttribute('aboutus_list', $list);
    }

    private function _assignParentImagePath($parentId)
    {
        $record = $this->_articleCate->getRecordById($parentId);
        HResponse::setAttribute('bannerImage', HResponse::touri($record['image_path']));
    }

    private function _assignParentName($parentId)
    {
        $record = $this->_articleCate->getRecordById($parentId);
        HResponse::setAttribute('ParentName',$record['name']);
    }


}