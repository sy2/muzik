<?php 

/**
 * @version $Id$
 * @create 2013-8-6 10:20:09 By xjiujiu
 * @description HongJuZi Framework
 * @copyRight Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');
HClass::import('config.popo.noticepopo, app.cms.action.cmsaction, model.noticemodel');
class MessageAction extends CmsAction
{
    public function __construct() 
    {
        parent::__construct();
        $this->_popo    = new NoticePopo();
        $this->_model   = new NoticeModel($this->_popo);
    }

    public function add()
    {
        if (empty(I('name'))||empty(I('content'))||empty(I('email'))||empty(I('model'))) {
            throw new HRequestException('Please fill in the contents');
        }
        $email = HVerify::isEmail(I('email'));
        $data   = array(
            'shop_id'   => I('model'),
            'user_name' => I('name'),
            'email'     => $email,
            'content'   => I('content'),
            'create_time' => date('Y-m-d H:i:s'),
            'hash'      => md5(time())
        );
        $this->_model->add($data);
        if (HVerify::isAjaxByBool()) {
            HResponse::json(array('rs' => true, 'message' => 'Submit success'));
        } else {
            HResponse::info('Submit success');
        }
    }
}