<?php 

/**
 * @version $Id$
 * @create 2013-8-6 10:20:09 By xjiujiu
 * @description HongJuZi Framework
 * @copyRight Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');
HClass::import('config.popo.articlepopo, app.cms.action.cmsaction, model.articlemodel');
class CateAction extends CmsAction
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->_assignCurCateList();
        $this->_assignRecList();
        $this->_assignColorList();
        $this->_assignShapeList();
        $this->_assignRimList();
        $this->_assignMaterialList();
        $this->_getSelectList();
        $this->_render('catelist');
    }

}