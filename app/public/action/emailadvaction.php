<?php

/**
 * @version $Id$
 * @create 2013/10/13 12:51:32 By xjiujiu
 * @description HongJuZi Framework
 * @copyRight Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

HClass::import('app.base.action.baseaction');

/**
 * 公用控制父类
 *
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @package app.public.action
 * @since 1.0.0
 */
class EmailadvAction extends BaseAction
{
    /**
     * 访问统计
     */
    public function visit()
    {
        $id = HRequest::getParameter('id');//广告id
        if(is_numeric($id)){
            $article = HClass::quickLoadModel('article');
            $record = $article->getRecordById($id);
            if(!empty($record)){
                $data = array(
                    'id' => $id,
                    'total_visits' => $record['total_visits'] + 1
                );
                $article->edit($data);
            }
        }
        $image = imagecreate(10, 10);
        imagecolorallocate($image, 255, 255, 255);
        header('content-type:image/png');
        imagepng($image);
        imagedestroy($image);
        exit;
    }

}
