<?php

/**
 * @version			$Id$
 * @create 			2017-09-20 21:09:48 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.wxsettingpopo, app.admin.action.AdminAction, model.wxsettingmodel');

/**
 * 微信设置的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class WxsettingAction extends AdminAction
{

    /**
     * 构造函数 
     * 
     * 初始化类变量 
     * 
     * @access public
     */
    public function __construct() 
    {
        parent::__construct();
        $this->_popo        = new WxsettingPopo();
        $this->_model       = new WxsettingModel($this->_popo);
    }
    
    protected function _otherJobsAfterInfo() 
    {
        parent::_otherJobsAfterInfo();
        $this->_assignShopIdList();
        HResponse::setAttribute('encode_method_list', WxsettingPopo::$encodeMethodMap);
    }

}

?>
