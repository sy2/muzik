<?php

/**
 * @version			$Id$
 * @create 			2018-07-09 20:07:43 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.baomingpopo, app.admin.action.AdminAction, model.baomingmodel');
HClass::import('config.popo.goodspopo');
/**
 * 楼盘报名的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class BaomingAction extends AdminAction
{

    /**
     * 构造函数 
     * 
     * 初始化类变量 
     * 
     * @access public
     */
    public function __construct() 
    {
        parent::__construct();
        $this->_popo        = new BaomingPopo();
        $this->_model       = new BaomingModel($this->_popo);
    }
    
    public function _otherJobsAfterList()
    {
        parent::_otherJobsAfterList();
        $this->_formatDataList();
        HResponse::registerFormatMap('status', 'name', BaomingPopo::$statusMap);
    }

    public function _otherJobsAfterInfo()
    {
        parent::_otherJobsAfterInfo();
        $this->_assignStatusList();
    }

    private function _formatDataList()
    {
        $list = HResponse::getAttribute('list');
        foreach ($list as &$item) {
            $temp = explode(',', $item['pro']);
            $str  = '';
            foreach ($temp as $val) {
                $str .= !$str ? GoodsPopo::$productMap[$val]['name'] : ',' . GoodsPopo::$productMap[$val]['name'];
            }
            $item['pro'] = $str;

            $temp = explode(',', $item['hx']);
            $str  = '';
            foreach ($temp as $val) {
                $str .= !$str ? GoodsPopo::$huxingMap[$val]['name'] : ',' . GoodsPopo::$huxingMap[$val]['name'];
            }
            $item['hx'] = $str;

            $temp = explode(',', $item['gzd']);
            $str  = '';
            foreach ($temp as $val) {
                $str .= !$str ? GoodsPopo::$guanzhuMap[$val]['name'] : ',' . GoodsPopo::$guanzhuMap[$val]['name'];
            }
            $item['gzd'] = $str;

        }

        HResponse::setAttribute('list', $list);
    }

    private function _assignStatusList()
    {
        HResponse::setAttribute('status_list', BaomingPopo::$statusMap);

    }
}

?>
