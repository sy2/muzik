<?php

/**
 * @version			$Id$
 * @create 			2015-11-22 14:11:54 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.noticepopo, app.admin.action.AdminAction, model.noticemodel');

/**
 * 消息的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class NoticeAction extends AdminAction
{

    /**
     * 构造函数 
     * 
     * 初始化类变量 
     * 
     * @access public
     */
    public function __construct() 
    {
        parent::__construct();
        $this->_popo        = new NoticePopo();
        $this->_model       = new NoticeModel($this->_popo);
        $this->_popo->setFieldAttribute('create_time', 'is_show', true);
        $this->_popo->setFieldAttribute('author', 'is_show', true);
    }

    /**
     * 列表后驱方法
     * @return [type] [description]
     */
    protected function _otherJobsAfterList()
    {
        parent::_otherJobsAfterList();
        HResponse::registerFormatMap('status', 'name', NoticePopo::$statusMap);
        HResponse::registerFormatMap('type', 'name', NoticePopo::$typeMap);
        HResponse::setAttribute('parent_id_list', array());
    }


}

?>
