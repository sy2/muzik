<?php

/**
 * @version			$Id$
 * @create 			2017-10-31 15:10:57 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.accounttotalpopo, app.admin.action.AdminAction, model.accounttotalmodel');

/**
 * 账号总金额的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class AccounttotalAction extends AdminAction
{

    /**
     * 构造函数 
     * 
     * 初始化类变量 
     * 
     * @access public
     */
    public function __construct() 
    {
        parent::__construct();
        $this->_popo        = new AccounttotalPopo();
        $this->_model       = new AccounttotalModel($this->_popo);
    }

    /**
     * 加载完列表的方法
     *
     * @return [type] [description]
     */
    protected function _otherJobsAfterList()
    {
        $shopList   = $this->_getShopList();
        parent::_otherJobsAfterList();
        HResponse::registerFormatMap('shop_id', 'name', $shopList);
    }

    /**
     * 加载完详情的方法
     * @return [type] [description]
     */
    protected function _otherJobsAfterInfo()
    {
        parent::_otherJobsAfterInfo();
        $this->_assignShopIdList();
    }

}

?>
