<?php 

/**
 * @version $Id$
 * @create 2013/10/12 15:35:33 By xjiujiu
 * @description HongJuZi Framework
 * @copyRight Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('app.base.action.AdministratorAction');
HClass::import('vendor.excel.PHPExcel', false);

/**
 * Excel数据导入工具
 * 
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @package app.admin.action
 * @since 1.0.0
 */
class FiledataAction extends AdministratorAction
{

    /**
     * @var private $_excel Excel导入实例
     */
    private $_excel;

    /**
     * @var private static $_filterFields 过滤字段
     */
    private static $_filterFields = array('id', 'parent_id', 'author', 'create_time', 'hash');

    /**
     * 构造函数
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function __construct()
    {
        $this->_excel   = null;
    }

    /**
     * 生成对应的模块导入Excel模板 
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function index()
    {
        //$this->_assignAreaList();

        $this->_render('filedata/import');
    }

    /**
     * 加载地区列表
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     */
    private function _assignAreaList()
    {
        $area   = HClass::quickLoadModel('area');

        HResponse::setAttribute('areaList', $area->getAllRowsByFields('id, name'));
    }

    /**
     * 导入Excel数据
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function doimport()
    {
        HVerify::isEmpty(HRequest::getParameter('type'), '数据类型');
        $filePath   = $this->_uploadExcelFile();
        $startRow   = $this->_initModel();
        $data       = $this->_extractExcelData($filePath, $startRow);
        $total      = $this->_importExcelData($data);

        HResponse::succeed(
            'Excel文件导入成功，共导入（' . $total . '条数据）！'
            . '（<em>注意：如果没有数据添加，请确认是否有重复导入！</em>）',
            HResponse::url('filedata', 'id=' . HRequest::getParameter('type'))
        );
    }

    /**
     * 初始化模块文件
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     */
    private function _initModel()
    {
        $startRow   = 2;
        switch(HRequest::getParameter('type')) {
            case 1: $model = 'liantianqi'; $startRow = 4; break;
            case 2: $model = 'lianwuran'; break;
            case 3: $model = 'zongtianqi'; break;
            case 4: $model = 'zongwuran'; $startRow = 3; break;
            default: throw new HVerifyException('数据类型不支持，请确认！'); break;
        }
        $this->_model   = HClass::quickLoadModel($model);
        $this->_popo    = $this->_model->getPopo();

        return $startRow;
    }

    /**
     * 导入Excel数据
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     * @param  Array $data 需要导入的数据
     */
    private function _importExcelData($data)
    {
        if(empty($data)) { 
            throw new HVerifyException('Excel文件里为空，请确认！'); 
        }
        $list       = array();
        $count      = count($data) - 1;
        $total      = 0;
        foreach($data as $key => $row) {
            $list[] = $row;
            unset($data[$key]);
            if($key % 50 === 0 || $key == $count) {
                try {
                    $list   = $this->_cleanData($list);
                    if(!$list) {
                        continue;
                    }
                    $total  += count($list);
                    $this->_model->add($list, array_keys($row));
                    $list   = array();
                    continue;
                } catch(HSqlParseException $ex) {
                    throw new HRequestException('服务器繁忙，请您稍后再试～导入失败！' . $ex->getMessage());
                }
            }
        }

        return $total;
    }

    /**
     * 清空數據
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     * @param $list 需要處理的數據
     * @return 處理完后的數據
     */
    private function _cleanData($list)
    {
        $repeatList = $this->_model->getAllRowsByFields(
            '`id`, `hash`', 
            HSqlHelper::whereOrByListMap('hash', 'hash', $list)
        );
        if(!$repeatList) {
            return $list;
        }
        $repeatMap  = HArray::turnItemValueAsKey($repeatList, 'hash');
        foreach($list as $key => $item) {
            if(isset($repeatMap[$item['hash']])) {
                unset($list[$key]);
            }
        }

        return $list;
    }
    
    /**
     * 提取Excel数据
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     * @param  String $filePath 
     * @return Array 数组
     */
    private function _extractExcelData($filePath, $startRow)
    {
        $data           = array(); 
        $reader         = PHPExcel_IOFactory::createReader('Excel5');
        $reader->setReadDataOnly(true); 
        $this->_excel   = $reader->load(ROOT_DIR . DS . $filePath); 
        $workSheet      = $this->_excel->getActiveSheet(); 
        $maxRow         = $workSheet->getHighestRow(); 
        $maxColumn      = $workSheet->getHighestColumn(); 
        $maxColIndex    = $maxColumnIndex = PHPExcel_Cell::columnIndexFromString($maxColumn); 
        $fields         = $this->_popo->getFields();
        for ($row = $startRow; $row <= $maxRow; $row++) {
            $col    = 0;
            $loc    = $row - $startRow;
            foreach($fields as $field => $cfg) {
                if(in_array($field, self::$_filterFields)) {
                    continue;
                }
                $val    = (string)$workSheet->getCellByColumnAndRow($col, $row)->getValue();
                if(true === $cfg['null'] && HVerify::isEmptyNotZero($val)) {
                    throw new HVerifyException(
                        '错误：' . $cfg['name'] . '的值必须填写！在' . ($row + 1) . '行，' . ($col + 1) . '列。导入终止！'
                    );
                }
                $data[$loc][$field]     = HString::encodeHtml($val);
                $col ++;
            }
            $data[$loc]['timestamp']    = $this->_getTimestamp($data[$loc]);
            $data[$loc]['hash']         = md5($data[$loc]['timestamp']);
            $data[$loc]['author']       = HSession::getAttribute('id', 'user');
        }
        unset($fieldCfg);

        return $data; 
    }

    /**
     * 得到校驗碼
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     * @param $data 數據
     * @return  String 
     */
    private function _getTimestamp($data)
    {
        if(isset($data['datetime'])) {
            return strtotime($data['datetime']);
        }
        if(isset($data['year'])) {
            return strtotime($data['year'] . '-' . $data['month'] . '-' . $data['day'] . ' '.  $data['time']);
        }

        return strtotime($data['date'] . ' ' .  $data['time']);
    }

    /**
     * 上传Excel文件
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     * @throws HRequestException 请求异常 | HVerifyException 验证异常
     * @return String 上传后的服务器地址
     */
    private function _uploadExcelFile()
    {;
        $uploadFile      = HRequest::getFiles('excel_file');
        HVerify::isEmpty($uploadFile['name'], '需要导入的Excel文件');
        if($uploadFile['error']) {
            throw new HVerifyException('上传出错，请确认是否磁盘空间不足，或上传的文件已经不存在！');
        }
        HClass::import('hongjuzi.net.huploader');
        $hUploader          = new HUploader(
            HObject::GC('RES_DIR') . '/filedata/', 
            5,  //5m
            array('.xlsx', '.xls'),
            ROOT_DIR . DS
        );
        $uploadedInfo       = $hUploader->uploader($uploadFile);
        if(isset($uploadedInfo['error'])) {
            if(!empty($uploadedInfo['error'])) {
                throw new HRequestException(($uploadedInfo['error']));
            }
        }

        return $uploadedInfo['path'];
    }

    /**
     * 初始化Excel属性
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     * @param  String $type 导出文件类型
     */
    private function _initExcelProps($type)
    {
        $title  = $this->_popo->modelZhName . 'Excel' . $type;
        $this->_props  = $this->_excel->getProperties();
        $this->_props->setCreator('红橘子科技有限公司');
        $this->_props->setLastModifiedBy('红橘子科技有限公司');
        $this->_props->setTitle($title);
        $this->_props->setSubject($title);  
        $this->_props->setDescription($title);  
        $this->_props->setKeywords($type);
        $this->_props->setCategory($this->_popo->modelZhName . '模块Excel' . $type);
    }

    /**
     * 初始化Excel表头
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     * @param  Array $data 需要导入的数据
     * @param  int $sheet 需要操作的表
     */
    private function _fillDataToExcelFile($data = null, $sheet = 0, $title = 'sheet1')
    {
        $this->_excel->setActiveSheetIndex($sheet);
        $objActSheet = $this->_excel->getActiveSheet();  
        //设置当前活动sheet的名称  
        $objActSheet->setTitle($title);
        //设置单元格内容
        $col    = 0;
        //设置Excel表头
        foreach($this->_popo->get('fields') as $field => $cfg) {
            $value  = isset($cfg['not_null']) && true === $cfg['not_null'] ? '*必须填写！' : '';
            $value  .= empty($cfg['comment']) ? '' : $cfg['comment'];
            $value  = empty($value) ? $cfg['name'] : $cfg['name'] . '（' . $value . '）';
            $objActSheet->setCellValueByColumnAndRow($col, '1', $value);
            $col ++;
        }
        if(empty($data)) {
            return;
        }
        //填充数据
        $row    = 2;
        foreach($data as $key => $item) {
            $col    = 0;
            foreach($this->_popo->get('fields') as $field => $cfg) {
                $objActSheet->setCellValueByColumnAndRow($col, $row, HString::decodeHtml($item[$field]));
                $col ++;
            }
            $row ++;
        }
    }

    /**
     * 生成对应的模块Excel模板
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     */
    private function _outputExcelFile($fileName)
    {
        HClass::import('vendor.excel.PHPExcel.Writer.Excel5');
        $writer     = new PHPExcel_Writer_Excel5($this->_excel);
        header('Content-Type: application/force-download');  
        header('Content-Type: application/octet-stream');  
        header('Content-Type: application/download');
        header('Content-Disposition:inline;filename=' . $fileName);
        header('Content-Transfer-Encoding: binary');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');  
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');  
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: no-cache');
        $writer->save('php://output');  
    }

}

?>
