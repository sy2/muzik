<?php

/**
 * @version			$Id$
 * @create 			2013-06-17 01:06:41 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.rimpopo, app.admin.action.adminAction, model.rimmodel');

/**
 * 信息分类的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class RimAction extends AdminAction
{

    public function __construct()
    {
        parent::__construct();
        $this->_popo        = new Rimpopo();
        $this->_model       = new RimModel($this->_popo);
    }

    protected function _otherJobsAfterList()
    {
        parent::_otherJobsAfterList();
        HResponse::registerFormatMap('status', 'name', RimPopo::$statusMap);
    }
}

?>
