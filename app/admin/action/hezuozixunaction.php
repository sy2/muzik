<?php

/**
 * @version			$Id$
 * @create 			2018-06-30 17:06:46 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.hezuozixunpopo, app.admin.action.AdminAction, model.hezuozixunmodel');

/**
 * 合作咨询的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class HezuozixunAction extends AdminAction
{

    /**
     * 构造函数 
     * 
     * 初始化类变量 
     * 
     * @access public
     */
    public function __construct() 
    {
        parent::__construct();
        $this->_popo        = new HezuozixunPopo();
        $this->_model       = new HezuozixunModel($this->_popo);
        $this->_listTpl     = 'hezuozixun/list';
    }
    
    public function _otherJobsAfterList()
    {
        parent::_otherJobsAfterList();
        $this->_getUserMap();
        HResponse::registerFormatMap('parent_id', 'name', HResponse::getAttribute('userMap'));
    }
    
    private function _getUserMap()
    {
        $list = HResponse::getAttribute('list');
        $user = HClass::quickLoadModel('user');
        $userList = $user->getAllRows(HSqlHelper::whereInByListMap('id', 'parent_id', $list));
        HResponse::setAttribute('userMap', HArray::turnItemValueAsKey($userList, 'id'));
    }
}

?>
