<?php

/**
 * @version			$Id$
 * @create 			2012-04-21 11:04:10 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入模块工具类
HClass::import('config.popo.ordergoodspopo, app.admin.action.AdminAction, model.ordergoodsmodel');

/**
 * 利润统计的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class DazheAction extends AdminAction
{
    //订单对象
    private $_order;

    /**
     * 构造函数 
     * 
     * 初始化类变量 
     * 
     * @access public
     */
    public function __construct() 
    {
        parent::__construct();
        $this->_popo            = new OrdergoodsPopo();
        $this->_model           = new OrdergoodsModel($this->_popo);
        $this->_model->setMustWhere('is_pay', '`status` = 2');
        $this->_setStartEndTimeValue();
    }

    /**
     * 首页
     */
    public function index()
    {
        $this->_popo->modelEnName = 'dazhe';
        $this->_popo->modelZhName = '打折统计报表';
        $startTime  = strtotime(HRequest::getParameter('start_time') . ' 00:00:00');
        $endTime    = strtotime(HRequest::getParameter('end_time') . ' 23:59:59');
        if($endTime - $startTime > 5356800) {
            throw new HVerifyException('统计时间长度不能超过62天！');
        }
        $where      = $this->_combineWhere();
        $this->_assignDaZheData($where);
        $this->_assignGoodsDaZheData($startTime, $endTime, $where);
        $this->_commAssign();
        $this->_render('reportcount/zhekou');
    }

    //加载产品打折数据 
    private function _assignGoodsDaZheData($where)
    {
        $startDate  = HRequest::getParameter('start_time');
        $endDate    = HRequest::getParameter('end_time');
        $where      .= ' AND `create_time` >= \'' . $startDate . ' 00:00:00\' AND `create_time` <= \'' . $endDate . ' 23:59:59\'';
        $list       = $this->_model->getAllRowsByFieldsAndGroup(
            'FORMAT(sum(`old_price`), 2) as old_sum, sum(`price`) as zhe_sum, sum(`number`) as number, AVG(`discount`) AS zhekou, `goods_id`, `name`',
            $where,
            'goods_id',
            'zhe_sum DESC'
        );
        if(!$list) {
            return;
        }
        HResponse::setAttribute('goodsList', $list);
    }

    //加载打折数据 
    private function _assignDaZheData($startTime, $endTime, $preWhere)
    {
        $xAxis          = array();
        $oldSumList     = array();
        $zheSumList     = array();
        $zheKouList     = array();
        $numberList     = array();
        $subSumList     = array();
        $orderList      = array();
        $zhuoHaoList    = array();
        $sum            = 0;
        $zhuoHao        = HClass::quickLoadModel('zhuohao');
        $order          = HClass::quickLoadModel('order');
        while($startTime < $endTime) {
            $xAxis[]        = date('y-m-d', $startTime);
            $where          = $preWhere . ' AND `create_time` >= \'' . date('Y-m-d H:i:s', $startTime) . '\''. ' AND `create_time` <= \'' . date('Y-m-d H:i:s', $startTime + 86400) . '\'';
            $sumData        = $order->getRecordByFields(
                'SUM(`amount`) as old_sum, SUM(`zhekou_amount`) as zhe_sum, AVG(`zhekou`) as zhekou, SUM(`number`) as number',
                '`status` IN(4, 10) AND ' . $where
            );
            $oList          = $order->getAllRowsByFields(
                '`id`, `name`, `amount`, `code`, `zhekou`, `zhekou_amount`, `total_number`, `create_time`, `pay_time`, `payment`',
                '`status` IN(4, 10) AND ' . $where
            );
            $orderList[]    = $oList;
            $zhuoHaoList[]  = HArray::turnItemValueAsKey(
                $zhuoHao->getAllRowsByFields(
                    '`id`, `name`', 
                    HSqlHelper::whereInByListMap('id', 'code', $oList)
                ),
                'id'
            );
            $numberList[]       = intval($sumData['number']);
            $zheKouList[]       = number_format(floatval($sumData['zhekou']), 2);
            $zheSumList[]       = floatval($sumData['old_sum'] - $sumData['zhe_sum']);
            $oldSumList[]       = floatval($sumData['old_sum']);
            $subSumList[]       = floatval($sumData['zhe_sum']);
            $startTime          += 86400;
            $sum                += $sumData['old_sum'];
        }

        HResponse::setAttribute('xAxis', $xAxis);
        HResponse::setAttribute('sum', $sum);
        HResponse::setAttribute('orderList', $orderList);
        HResponse::setAttribute('numberList', $numberList);
        HResponse::setAttribute('zheKouList', $zheKouList);
        HResponse::setAttribute('zheSumList', $zheSumList);
        HResponse::setAttribute('subSumList', $subSumList);
        HResponse::setAttribute('oldSumList', $oldSumList);
        HResponse::setAttribute('zhuoHaoList', $zhuoHaoList);
    }

    /**
     * 得到日期条件
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     * @return 日期条件
     */
    protected function _getDateWhere()
    {
        $where      = array('1 = 1');
        $startTime  = HRequest::getParameter('start_time');
        $startTime  = $startTime ? $startTime : date('Y-m-d', strtotime('-1 week'));
        $endTime    = HRequest::getParameter('end_time');
        $endTime    = $endTime ? $endTime : date('Y-m-d', time());
        array_push($where, '`create_time` >= \'' . $startTime . '\'');
        array_push($where, '`create_time` <= \'' . $endTime . '\'');
        HRequest::setParameter('start_time', $startTime);
        HRequest::setParameter('end_time', $endTime);

        return $where;
    }

    /**
     * 加载快捷时间
     * @return [type] [description]
     */
    private function _assignQuickTime()
    {
        $now        = date('Y-m-d', time());
        $yestMonth  = date('Y-m-d', strtotime('-1 month'));
        $yestDay    = date('Y-m-d', strtotime('-7 day'));
        $yestYear   = date('Y-m-d', strtotime('-1 year'));
        HResponse::setAttribute(
            'sevDayWhere', 
            'start_time=' . $yestDay . '&end_time=' . $now
        );
        HResponse::setAttribute(
            'oneMonthWhere',
            'start_time=' . $yestMonth . '&end_time=' . $now  
        );
        HResponse::setAttribute(
            'oneYearWhere',
            'start_time=' . $yestYear . '&end_time=' . $now
        );
    }

}

?>
