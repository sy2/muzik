<?php

/**
 * @version			$Id$
 * @create 			2016-02-25 16:02:53 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.emailqueuepopo, app.admin.action.AdminAction, model.emailqueuemodel');

/**
 * 邮件队列的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class EmailqueueAction extends AdminAction
{

    /**
     * 构造函数 
     * 
     * 初始化类变量 
     * 
     * @access public
     */
    public function __construct() 
    {
        parent::__construct();
        $this->_popo        = new EmailqueuePopo();
        $this->_model       = new EmailqueueModel($this->_popo);
    }

    public function editview()
    {
        HResponse::info('功能未开放');
    }

}

?>
