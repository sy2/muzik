<?php

/**
 * @version			$Id$
 * @create 			2012-4-8 8:30:17 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('app.admin.action.AdminAction');

/**
 * 管理主页的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class AppsAction extends AdminAction
{

    public function __construct()
    {
        parent::__construct();
        $this->_model   = HClass::quickLoadModel('ModelManager');
    }

    /**
     * 主页动作 
     * 
     * @desc
     * 
     * @access public
     */
    public function index()
    {
        $this->_assignModelManagerList('`parent_id` <= 0');
        $this->_commAssign();
        
        $this->_render('apps/apps');
    }

    //子功能
    public function subapps()
    {
        $id     = HVerify::isEmptyByVal(HRequest::getParameter('id'), '编号');
        $info   = $this->_model->getRecordById($id);
        $this->_assignModelManagerList('`parent_id` = ' . $id);
        HResponse::setAttribute('rootInfo', $info);
        $this->_commAssign();
        
        $this->_render('apps/sub-apps');
    }

    /**
     * 加载模块管理所有列表
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     */
    private function _assignModelManagerList($where)
    {
        $this->_linkedData->setRelItemModel('actor', 'modelmanager');
        $linkedList  = $this->_linkedData->getAllRowsByFields(  
            '`id`, `item_id`',
            '`rel_id` = ' . HSession::getAttribute('parent_id', 'user')
        );
        if(!$linkedList) {
            return;
        }
        HResponse::setAttribute('list', $this->_model->getAllRowsByFields(
            '`id`, `name`, `identifier`, `image_path`, `parent_id`, `description`, `is_quick`',
            HSqlHelper::whereInByListMap('id', 'item_id', $linkedList)
            . ' AND (' . $where  . ')'
        ));
        HResponse::setAttribute('id', HRequest::getParameter('id'));
    }

}

?>
