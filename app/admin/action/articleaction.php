<?php

/**
 * @version			$Id$
 * @create 			2013-06-18 10:06:22 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.articlepopo, app.admin.action.AdminAction, model.articlemodel');

/**
 * 文章的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class ArticleAction extends AdminAction
{

    /**
     * 构造函数 
     * 
     * @access public
     */
    public function __construct() 
    {
        parent::__construct();
        $this->_popo            = new ArticlePopo();
        $this->_popo->setFieldAttribute('description', 'is_show', false);
        $this->_popo->setFieldAttribute('author', 'is_show', false);
        $this->_model           = new ArticleModel($this->_popo);
    }

    /**
     * 搜索方法 
     * 
     * @access public
     */
    public function search()
    {
        $this->_search($this->_combineWhere());
        $this->_render('article/list');
    }

    /**
     * 搜索方法 
     * 
     * @access public
     */
    public function index()
    {
        $this->_search($this->_combineWhere());
        $this->_render('article/list');
    }

    /**
     * List 方法
     * 
     * {@inheritdoc}
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     */
    protected function _otherJobsAfterList()
    {
        parent::_otherJobsAfterList();
        HResponse::setAttribute('parent_id_list', '');
        HResponse::registerFormatMap('status', 'name', ArticlePopo::$statusMap);
    }

    /**
     * 视图后驱
     * 
     * {@inheritdoc}
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     */
    protected function _otherJobsAfterAddView()
    {
        parent::_otherJobsAfterAddView();
        HResponse::setAttribute('status_list', ArticlePopo::$statusMap);
    }

    /**
     * 编辑后驱
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     */
    protected function _otherJobsAfterEditView($record)
    {
        parent::_otherJobsAfterEditView($record);
        HResponse::setAttribute('record', $record);
        HResponse::setAttribute('status_list', ArticlePopo::$statusMap);
    }

    /**
     * 详细页后驱
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     */
    protected function _otherJobsAfterEdit($id)
    {
        parent::_otherJobsAfterEdit($id);
    }

    /**
     * 得到上层条件
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     * @param $id 编号
     * @return String 
     */
    protected function _getParentWhere($id)
    {
        $cat    = $this->_category->getRecordById($id);
        if(!$cat) {
            throw new HVerifyException('分类已经不存在，请确认！');
        }
        return '`parent_path` LIKE \'' . $cat['parent_path'] . '%\'';
    }

    /**
     * 显示发布文章
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @return String 
     */
    public function publish()
    {
        $this->_search('`status` = 2');
        $this->_render('list');
    }

    /**
     * 显示未发布文章
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access notpublic
     * @return String 
     */
    public function notpublish()
    {
        $this->_search('`status` = 1');
        $this->_render('list');
    }

    protected function _otherJobsBeforeEdit(){
        HRequest::setParameter('shop_id', 0);
        HRequest::setParameter('create_time', date('Y-m-d H:i:s'));
    }
}

?>
