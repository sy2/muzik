<?php

/**
 * @version			$Id$
 * @create 			2018-07-10 09:07:42 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.userinfopopo, app.admin.action.AdminAction, model.userinfomodel');

/**
 * 用户完善信息的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class UserinfoAction extends AdminAction
{

    /**
     * 构造函数 
     * 
     * 初始化类变量 
     * 
     * @access public
     */
    private $_user;
    private $_userinfo;
    public function __construct() 
    {
        parent::__construct();
        $this->_popo        = new UserinfoPopo();
        $this->_model       = new UserinfoModel($this->_popo);
        $this->_user        = HClass::quickLoadModel('user');
        $this->_userinfo    = HClass::quickLoadModel('userinfo');
    }

    protected function _otherJobsAfterList()
    {
        parent::_otherJobsAfterList();
        $where   = HSqlHelper::whereInByListMap('id', 'parent_id', HResponse::getAttribute('list'));
        $userMap = HArray::turnItemValueAsKey( $this->_user->getAllRowsByFields('id,true_name', $where ), 'id');
        HResponse::registerFormatMap('parent_id', 'true_name', $userMap);
        HResponse::registerFormatMap('type', 'name', UserinfoPopo::$typeMap);
        HResponse::registerFormatMap('khh', 'name', UserinfoPopo::$yinghangMap);
    }

    protected function _otherJobsAfterInfo()
    {
        parent::_otherJobsAfterInfo();
        $this->_assignParentInfoList();
    }

    private function _assignParentInfoList()
    {
        $userInfoList = $this->_userinfo->getAllRowsByFields('parent_id AS `id`,true_name as name');
        HResponse::setAttribute('parent_id_list', HArray::turnItemValueAsKey($userInfoList, 'id'));
        HResponse::setAttribute('type_list', UserinfoPopo::$typeMap);
        HResponse::setAttribute('khh_list', UserinfoPopo::$yinghangMap);
    }

}

?>
