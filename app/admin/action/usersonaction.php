<?php

/**
 * @version			$Id$
 * @create 			2018-07-11 00:07:22 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.usersonpopo, app.admin.action.AdminAction, model.usersonmodel');

/**
 * 用户子用户的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class UsersonAction extends AdminAction
{

    /**
     * 构造函数 
     * 
     * 初始化类变量 
     * 
     * @access public
     */
    private $_user;
    private $_actor;

    public function __construct() 
    {
        parent::__construct();
        $this->_popo     = new UsersonPopo();
        $this->_model    = new UsersonModel($this->_popo);
        $this->_user     = HClass::quickLoadModel('user');
        $this->_actor    = HClass::quickLoadModel('actor');
    }
    
    protected function _otherJobsAfterList()
    {
        parent::_otherJobsAfterList();
        $this->_assignParentSonName();
        $this->_popo->modelZhName = '邀请人';
    }

    private function _assignParentSonName()
    {
        $list           = HResponse::getAttribute('list');
        $parentNameList = $this->_user->getAllRowsByFields('`id`,`name`', HSqlHelper::whereInByListMap('id', 'parent_id', $list));
        $sonNameList    = $this->_user->getAllRowsByFields('`id`,`name`', HSqlHelper::whereInByListMap('id', 'son_id', $list));
        $usesonList     = $this->_model->getAllRowsByFields('`parent_id`', '1 = 1');
        HResponse::setAttribute('parent_id_list', $this->_user->getAllRowsByFields('`id`,`name`', HSqlHelper::whereInByListMap('id', 'parent_id', $usesonList)));
        HResponse::registerFormatMap('parent_id', 'name', HArray::turnItemValueAsKey($parentNameList, 'id'));
        HResponse::registerFormatMap('son_id', 'name', HArray::turnItemValueAsKey($sonNameList, 'id'));
    }

    protected function _otherJobsAfterInfo()
    {
        parent::_otherJobsAfterInfo();
        $this->_assignUserInfoList();
    }

    protected function _otherJobsBeforeEdit()
    {
        parent::_otherJobsBeforeEdit();
        $this->_checkParentAndSon();
    }

    private function _checkParentAndSon()
    {
        $info  = $this->_model->getRecordByFields('id','`parent_id` = ' . HRequest::getParameter('parent_id') . ' AND `son_id` = ' . HRequest::getParameter('son_id'));
        if( $info ) {
            throw new HRequestException('已经邀请注册过了');
        }

        $info  = $this->_model->getRecordByFields('id','`parent_id` = ' . HRequest::getParameter('son_id') . ' AND `son_id` = ' . HRequest::getParameter('parent_id'));
        if( $info ) {
            throw new HRequestException('邀请不可逆');
        }
    }

    private function _assignUserInfoList()
    {
        $actor    = $this->_actor->getRecordByIdentifier('member');
        $userList = $this->_user->getAllRowsByFields('id,name', '`parent_id` = ' . $actor['id']);
        HResponse::setAttribute('parent_id_list', HArray::turnItemValueAsKey($userList, 'id'));
        HResponse::setAttribute('son_id_list', HArray::turnItemValueAsKey($userList, 'id'));
    }

    protected function _combineWhere()
    {
        return $this->_getUserSearchWhere();
    }

    private function _getUserSearchWhere()
    {
        $where = '1 = 1';
        if( HRequest::getParameter('keywords') != '关键字...' ) {
            $userList = $this->_user->getAllRowsByFields('id', '`name` LIKE \'%' . HRequest::getParameter('keywords') . '%\'');
            $where .= ' AND ' .  HSqlHelper::whereInByListMap('parent_id', 'id', $userList) . ' OR ' .  HSqlHelper::whereInByListMap('son_id', 'id', $userList);
        }

        if( HRequest::getParameter('type') ) {
            $where .= ' AND `parent_id` = ' . HRequest::getParameter('type');
        }

        return $where;
    }

}

?>
