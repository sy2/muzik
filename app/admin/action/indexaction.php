<?php

/**
 * @version			$Id$
 * @create 			2012-4-8 8:30:17 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('app.admin.action.AdminAction');

/**
 * 管理主页的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class IndexAction extends AdminAction
{

    /**
     * @var 商品对象
     */
    private $_goods;

    /**
     * 构造函数
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function __construct()
    {
        parent::__construct();
        $this->_goods       = HClass::quickLoadModel('goods');
    }

    /**
     * 主页动作 
     * 
     * @desc
     * 
     * @access public
     */
    public function index()
    {
        $curActorInfo  = $this->_getActorInfo();
        $this->_commAssign();
        HResponse::setAttribute('curActorInfo', $curActorInfo);
        $this->_render('index');
    }

    //得到角色信息
    private function _getActorInfo()
    {
        $actor  = HClass::quickLoadModel('actor');

        return $actor->getRecordById(HSession::getAttribute('parent_id', 'user'));
    }

    /**
     * 设置Sidebar 为Mini风格
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function setsidebarmini()
    {
        $flag   = HRequest::getParameter('flag');
        HSession::setAttribute('isMiniStyle', 2 != $flag ? false : true, 'root');
        HResponse::json(array('rs' => true));
    }


    private function _assignNewUsersYesterdayCount()
    {
        $where      = 'TO_DAYS(NOW()) - TO_DAYS(`create_time`) = 1';
        $user       = HClass::quickLoadModel('user');
        $count      = $user->getTotalRecords($where);

        HResponse::setAttribute('newUserYesterday', $count);
    }



    /**
     * 加载已上架商品总数
     */
    private function _assignGoodsCount()
    {
        $goods      = HClass::quickLoadModel('goods');
        $where      = '`status` = 2';
        $count      = $goods->getTotalRecords($where);

        HResponse::setAttribute('goodsCount', $count);
    }
}

?>
