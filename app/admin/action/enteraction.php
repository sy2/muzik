<?php

/**
 * @version			$Id$
 * @create 			2012-4-8 8:48:15 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 * HongJuZi Framework
 */
defined('_HEXEC') or die('Restricted access!');

HClass::import('app.admin.action.AdminAction');

/**
 * 管理主页的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class EnterAction extends AdminAction
{

    /**
     * 前驱方法
     * 
     * {@inheritdoc}
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     */
    public function beforeAction()
    {
        $this->_switchLang();
        HTranslate::loadDictByApp('admin', HSession::getAttribute('id', 'lang'));
    }

    public function tpost()
    {
        $url    = 'http://localhost:2767/pt/app/WeChatPay';
        parse_str('Body=&OutTradeNo=bill20182521924938550842&TotalFee=10&TimeSpan=1517837724958&Sign=B2B8D73276A0C8ECCE23746044280644', $params);
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>';
        var_dump(HRequest::post($url, $params));
        exit;
    }

    public function tbill()
    {
        $url    = 'http://localhost:2767/pt/app/ReleaseBillOrderNew';
        parse_str('billOrderUser=6FAQKCVD&BillOrderType=1&billOrderContent=今飞凯达&orderAddress=503大学生创业园B1栋4层&orderAddress2=黄家山路东50米&mapZuoBiao=27.529862,109.943252&mapZuoBiao2=27.529885,109.943266&OrderAddressInfo=测试&OrderAddressInfo2=和你的某方面&goodPrice=0&phone=18274591193&phone2=5649466&timespan=1517895501461&sign=B6083EB85DCB061E68E4DB8E15C333DA', $params);
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>';
        var_dump(HRequest::post($url, $params));
        exit;
    }

    /**
     * 进入初始动作 
     * 
     * @access public
     */
    public function index()
    {
        $this->_assignBingBg();
        $this->_render('login');
    }

    private function _assignBingBg()
    {
        $json   = HRequest::getContents('http://www.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1&nc=' . time() . '&pid=hp&FORM=Z9FD1&mkt=zh-CN');
        $data   = json_decode($json, true);
        HResponse::setAttribute('bingBg', 'http://cn.bing.com/' . $data['images'][0]['url']);
    }

}

?>
