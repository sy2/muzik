<?php

/**
 * @version			$Id$
 * @create 			2013-06-17 01:06:41 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.colorpopo, app.admin.action.adminAction, model.colormodel');

/**
 * 信息分类的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class ColorAction extends AdminAction
{

    public function __construct()
    {
        parent::__construct();
        $this->_popo        = new Colorpopo();
        $this->_model       = new ColorModel($this->_popo);
    }

    protected function _otherJobsAfterList()
    {
        parent::_otherJobsAfterList();
        HResponse::registerFormatMap('status', 'name', ColorPopo::$statusMap);
    }

    protected function _otherJobsBeforeEdit()
    {
        parent::_otherJobsBeforeEdit();
        if (!I('image_path')) {
            HRequest::setParameter('image_path', 0);
        }
    }
}

?>
