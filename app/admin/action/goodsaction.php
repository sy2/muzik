<?php

/**
 * @version			$Id$
 * @create 			2016-05-23 11:05:04 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.goodspopo, app.admin.action.AdminAction, model.goodsmodel');

/**
 * 商品管理的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class GoodsAction extends AdminAction
{
    //商品分类
    private $_goodsCategory;

    /**
     * 总数组
     * @var
     */
    private $_totalArr;

    /**
     * 总数组下标计数
     * @var
     */
    private $_totalArrIndex;

    /**
     * 输入的数组长度
     * @var
     */
    private $_totalCount;

    /**
     * 临时拼凑数组
     * @var
     */
    private $_tempArr;

    /**
     * 构造函数 
     * 
     * 初始化类变量 
     * 
     * @access public
     */
    private $_user;
    public function __construct()
    {
        parent::__construct();
        $this->_popo        = new GoodsPopo();
        $this->_model       = new GoodsModel($this->_popo);
        $this->_goodsCategory   = HClass::quickLoadModel('goodscategory');
        $this->_popo->setFieldAttribute('is_new', 'is_show', false);
        $this->_user = HClass::quickLoadModel('user');
    }

    /**
     * 编辑前的动作
     */
    protected function _otherJobsAfterInfo()
    {
        parent::_otherJobsAfterInfo();
        $record       = HResponse::getAttribute('record');
        $skuGoodsList = json_decode(HString::decodeHtml($record['extend']), true);
        HResponse::setAttribute('author', $this->_user->getRecordById($record['author'])['name']);
        HResponse::setAttribute('skuGoodsList', $skuGoodsList);
        HResponse::setAttribute('status_list', GoodsPopo::$statusMap);
        HResponse::setAttribute('position_list', HResponse::getAttribute('cityareaMap'));
        $record['color_id'] = json_decode(HString::decodeHtml($record['extend']), true);
        HResponse::setAttribute('record', $record);
        $this->_assignColorList();
        $this->_assignRimList();
        $this->_assignShapeList();
        $this->_assignMaterialList();
    }

    /**
     * 编辑前的动作
     */
    protected function _otherJobsBeforeEdit()
    {
        parent::_otherJobsBeforeEdit();
        $name    = HRequest::getParameter('name');
        $this->_setSkuParameter();
        HRequest::setParameter('first_word', $name);
    }

    protected static function _verifyNumber()
    {

    }

    private function _setSkuParameter()
    {
        $skuName     = HRequest::getParameter('color_id');
        $skuImagePath= HRequest::getParameter('sku_image_path');
        $brand       = I('brand');
        $sort        = I('sort');
        $sku         = array();
        $colorMap    = $this->_getColorMap();
        foreach ($skuName as $key => $item) {
            $sku[$key] = array(
                'id'         => $skuName[$key],
                'name'       => $colorMap[$item]['name'],
                'image_path' => $skuImagePath[$key],
                'brand'      => $brand[$key],
                'sort'       => $sort[$key],
                );
        }
        HRequest::setParameter('max_price', '1');
		HRequest::setParameter('color_id', ',' . implode(',', $skuName) . ',');
        array_multisort(array_column($sku,'sort'), SORT_ASC,$sku);
        HRequest::setParameter('extend', json_encode($sku, JSON_UNESCAPED_UNICODE));
    }

    /**
     * 得到分类映射
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     */
    private function _getColorMap()
    {
        $color  = HClass::quickLoadModel('color');
        $list   = $color->getAllRowsByFields(
            '`id`,`name`'
        );
        return HArray::turnItemValueAsKey($list, 'id');
    }

    /**
     * 执行编辑或是添加后驱
     * 
     * {@inheritdoc}
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     */
    protected function _otherJobsAfterEdit($goodsId)
    {
        parent::_otherJobsAfterEdit($goodsId);
    }

    /**
     * 加载完列表的方法
     *
     * @return [type] [description]
     */
    protected function _otherJobsAfterList() 
    {
        parent::_otherJobsAfterList();
        $this->_assignbrandMap();
        HResponse::registerFormatMap('status', 'name', GoodsPopo::$statusMap);
        HResponse::registerFormatMap('is_show', 'name', GoodsPopo::$isShowMap);
    }

    private function _assignbrandMap()
    {
        $list = HResponse::getAttribute('list');
        $list = $this->_category->getAllRowsByFields('`id`,`name`',
            HSqlHelper::whereInByListMap('id', 'brand_id', $list));
        HResponse::registerFormatMap('brand_id', 'name', HArray::turnItemValueAsKey($list, 'id'));
    }
    /**
     * 删除信息内部使用
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     * @param  Array $ids 需要删除的ID
     * @throws HRequestException 请求异常 
     */
    protected function _delete($recordIds)
    {
        $this->_model->deleteByWhere(HSqlHelper::whereIn('id', $recordIds));
    }

    /**
     * 主页动作
     *
     * @access public
     */
    public function index()
    {
        parent::_search();
        $this->_render('goods/list');
    }

    /**
     * 搜索方法
     *
     * @access public
     */
    public function search()
    {
        $where      = parent::_combineWhere();
        $status     = HRequest::getParameter('status');
        if($status){
            $where  = !$where ? '`status` = ' . $status : $where . ' AND `status` = ' . $status;
        }
        parent::_search($where);

        $this->_render('goods/list');
    }

    private function _assignColorList()
    {
        $color = HClass::quickLoadModel('color');
        $list = $color->getAllRowsByFields('`id`,`name`,`content`', '`status` = 2');
        HResponse::setAttribute('color_id_list', $list);
    }

    private function _assignRimList()
    {
        $color = HClass::quickLoadModel('rim');
        $list = $color->getAllRowsByFields('`id`,`name`,`content`', '`status` = 2');
        HResponse::setAttribute('rim_list', $list);
    }

    private function _assignShapeList()
    {
        $color = HClass::quickLoadModel('shape');
        $list = $color->getAllRowsByFields('`id`,`name`,`content`', '`status` = 2');
        HResponse::setAttribute('shape_list', $list);
    }

    private function _assignMaterialList()
    {
        $color = HClass::quickLoadModel('material');
        $list = $color->getAllRowsByFields('`id`,`name`,`content`', '`status` = 2');
        HResponse::setAttribute('material_list', $list);
    }
}

?>
