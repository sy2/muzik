<?php

/**
 * @version			$Id$
 * @create 			2017-09-29 21:09:29 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.articlecatpopo, app.admin.action.AdminAction, model.articlecatmodel');

/**
 * 文章分类的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class ArticlecatAction extends AdminAction
{

    /**
     * 构造函数 
     * 
     * 初始化类变量 
     * 
     * @access public
     */
    public function __construct() 
    {
        parent::__construct();
        $this->_popo        = new ArticlecatPopo();
        $this->_model       = new ArticlecatModel($this->_popo);
    }

    /**
     * 组合搜索条件
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     * @return String 组合成的搜索条件
     */
    protected function _combineWhere()
    {
        $where          = array('1 = 1');
        if(1 < intval(HRequest::getParameter('type'))) {
            array_push($where, '`parent_path` LIKE \'%:' . HRequest::getParameter('type') . ':%\'');
        }
        $keywords       = HRequest::getParameter('keywords');
        if(!$keywords || '关键字...' === $keywords) {
            return implode(' AND ', $where);
        }
        $keywordsWhere  = $this->_getSearchWhere($keywords);
        if($keywordsWhere) {
            array_push($where, $keywordsWhere);
        }

        if(!$where) {
            return null;
        }

        return implode(' AND ', $where);
    }


    /**
     * 编辑前的动作
     */
    protected function _otherJobsBeforeEdit()
    {
        parent::_otherJobsBeforeEdit();
        HRequest::setParameter('shop_id', 1);
        $this->_checkIdentifier();
    }

    /**
     * 检测标识是否已经使用过
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     * @param $id 编号
     */
    private function _checkIdentifier($id = '0')
    {
        if(!HRequest::getParameter('identifier')) {
            return;
        }
    }

    /**
     * 编辑提示动作 
     * 
     * @access public
     */
    public function edit()
    {
        $record         = $this->_model->getRecordById(HRequest::getParameter('id'));
        if(HVerify::isEmpty($record)) {
            throw new HVerifyException('没有找到这个分类！');
        }
        $this->_checkIdentifier($record['id']);
        $this->_edit();
        $this->_updateParentPath($record['id'], HRequest::getParameter('parent_id'), $record['parent_id']);
        
        HResponse::json(array(
            'rs' => true, 
            'message' => $this->_popo->modelZhName . '分类更新成功！', 
            'nextUrl' => $this->_getReferenceUrl(1))
        );
    }

    /**
     * 更新分类的层级信息
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     * @param $id 记录编号
     * @param  $pid 上级分类
     * @return 当前的分类层级
     */
    private function _updateParentPath($id, $pid, $oldPid)
    {
        $data      = array(
            'parent_path' => $this->_getParentPath($id, $pid)
        );
        $record     = $this->_model->getRecordById($id);
        $this->_model->editByWhere($data, '`id` = ' . $id);
        $this->_updateSubCategoryParentPath($record, $data['parent_path']);
        //更换父类编号时才需要更新层级
        if($record['parent_id'] != $oldPid) {
            $this->_updateArticleParentPath($record, $data['parent_path']);
        }

        return $data['parent_path'];
    }

    /**
     * 更新子分类层级
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     * @param $record 分类记录
     * @param  $path 当前新层级
     */
    private function _updateSubCategoryParentPath($record, $path)
    {
        $list   = $this->_category->getSubCategoryByParentPath($record['parent_path'], $record['id'], false);
        if(empty($list)) {
            return;
        }
        foreach($list as $item) {
            $data   = array(
                'parent_path' => str_replace($record['parent_path'], $path, $item['parent_path'])
            );
            $this->_model->editByWhere($data, '`id` = ' . $item['id']);
        }
    }

    /**
     * 更新文章层级
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     * @param  $record 记录
     * @param  $path 层级
     */
    private function _updateArticleParentPath($record, $path)
    {
        $id         = 0;
        $limit      = 100;
        $article    = HClass::quickLoadModel('article');
        $article->getPopo()->setFieldAttribute('id', 'is_order', 'ASC');
        if(!$record['parent_path']) {
            return ;
        }
        $oldPathWhere   = ' `parent_path` LIKE \'' . $record['parent_path'] . '%\'';
        do {
            $list       = $article->getSomeRowsByFields(
                $limit,
                '`id`, `parent_path`',
                'id > ' . $id . ' AND ' . $oldPathWhere
            );
            if(!$list) {
                break;
            }
            foreach($list as $item) {
                $id     = $item['id'];
                $data   = array(
                    'parent_path' => str_replace($record['parent_path'], $path, $item['parent_path'])
                );
                $article->editByWhere($data, '`id` = ' . $item['id']);
            }
        } while(true);
    }

    /**
     * 得到当前记录的parent_PATH值 
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     * @param  int $id 当前记录的ID
     * @param  int $parentId 当前的父层
     * @return 当前的所属层级 
     */
    protected function _getParentPath($id, $parentId)
    {
        if(empty($parentId) || -1 == $parentId) {
            return ':' . $id . ':';
        }
        $parent     = $this->_model->getRecordById($parentId);
        if(null == $parent) {
            return ':' . $id . ':';
        }

        return $parent['parent_path'] . $id . ':';
    }

    /**
     * 查找子分类 
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @throws HRequestException 请求异常 | HVerifyException 验证异常 
     */
    public function asubcategory()
    {
        $modelName = HRequest::getParameter('model_name');
        $modelName == '' ? $typeModel = $this->_model:$typeModel = HClass::quickLoadModel($modelName);
        HVerify::isAjax();
        HVerify::isNumber(HRequest::getParameter('pid'));
        HResponse::json(array(
            'list' => $typeModel->getAllRows(
                '`parent_id` = ' . HRequest::getParameter('pid')
            )
        ));
    }

    /**
     * 异步加载分享
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function aload()
    {
        HVerify::isAjax();
        HVerify::isEmpty(HRequest::getParameter('id'), '分类编号');

        echo HArray::makeZtreeJsonByListMap(
            $this->_model->getAllRows('`parent_id` = ' . HRequest::getParameter('id')),
            null,
            true
        );
    }

    /**
     * 异步新建分类
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function anew()
    {
        HVerify::isAjax();
        HVerify::isEmpty(HRequest::getParameter('name'), '名称');
        $data   = array(
            'name' => urldecode(HRequest::getParameter('name')),
            'parent_id' => intval(HRequest::getParameter('pid')),
            'author' => HSession::getAttribute('id', 'user')
        );
        $id     = $this->_model->add($data);
        if(1 > $id) {
            throw new HRequestException('添加新分类失败！');
        }
        $this->_updateParentPath($id, $data['parent_id']);
        $list   = $this->_model->getSubCategoryByIdentifier('article-cat');

        HResponse::json(array(
            'rs' => true,
            'data' => $this->_getCategoryTree($list),
            'node' => array(
                'id' => $id,
                'name' => $data['name'],
                'pId' => $data['parent_id'],
                'checked' => true
            )
        ));
    }

    /**
     * 得到分类的树形字符串
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     * @param $list 数据集合
     * @return 格式化事的字符串
     */
    private function _getCategoryTree($list)
    {
        HClass::import('hongjuzi.utils.HTree');
        $hTree  = new HTree(
            $list, 
            'id', 'parent_id',
            'name', 'id',
            '<option value="{id}">' . '{name}' . '</option>'
        );
        
        return $hTree->getTree();
    }

    /**
     * 异步获取商品属性列表
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     */
    public function alist()
    {
        HVerify::isAjax();
        HClass::import('hongjuzi.utils.HTree');
        $identifier = HVerify::isEmptyByVal(HRequest::getParameter('identifier'), '父类');
        $record     = $this->_model->getRecordByIdentifier($identifier);
        if(!$record){
            throw new HRequestException('分类' . $identifier . '不存在，请先添加');
        }
        $list       = $this->_model->getSubCategoryByIdentifier($identifier, true);
        $hTree      = new HTree(
            $list,
            'id',
            'parent_id',
            'name',
            'id',
            '<option value="{id}">' .
            '{name}' .
            '</option>'
        );
        $content    = $hTree->getTree();

        HResponse::json(array('rs' => true, 'data' => $content));
    }

}

?>
