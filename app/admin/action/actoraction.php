<?php

/**
 * @version			$Id$
 * @create 			2012-04-21 11:04:10 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入模块工具类
HClass::import('config.popo.actorpopo, app.admin.action.AdminAction, model.actormodel');

/**
 * 用户角色的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class ActorAction extends AdminAction
{

    /**
     * 构造函数 
     * 
     * 初始化类变量 
     * 
     * @access public
     */
    public function __construct() 
    {
        parent::__construct();
        $this->_popo        = new ActorPopo();
        $this->_model       = new ActorModel($this->_popo);
    }

    protected function _otherJobsAfterInfo()
    {
        parent::_otherJobsAfterInfo();
        $this->_assignRightsList();
        $this->_assignLeftSideBarMenuList();
        $this->_assignModelManagerList();
    }

    private function _assignModelManagerList()
    {
        $modelManager   = HClass::quickLoadModel('modelmanager');
        $list           = $modelManager->getAllRows('1 = 1');
        $record         = HResponse::getAttribute('record');
        HResponse::setAttribute(
            'modelManagerNodes', 
            HString::formatToZTreeNodes($list, $record ? explode(',', $record['modelmanager_ids']) : null)
        );
    }

    private function _assignLeftSideBarMenuList()
    {
        $catAdmin   = $this->_category->getRecordByIdentifier('admin');
        if(!$catAdmin) {
            return;
        }
        $navmenu    = HClass::quickLoadModel('navmenu');
        $list       = $navmenu->getAllRows('`position_id` = ' . $catAdmin['id']);
        $record     = HResponse::getAttribute('record');
        foreach($list as &$item) {
            $item['name'] .= '(' . $item['description'] . ')';
        }

        HResponse::setAttribute(
            'leftSidebarMenuNodes', 
            HString::formatToZTreeNodes($list, $record ? explode(',', $record['leftmenu_ids']) : null)
        );
    }

    /**
     * {@inheritDoc}
     */
    protected function _assignRightsList()
    {
        $rights     = HClass::quickLoadModel('rights');
        $list       = $rights->getAllRows('1 = 1');
        $record     = HResponse::getAttribute('record');
        HResponse::setAttribute(
            'rightsNodes',
            HString::formatToZTreeNodes($list, $record ? explode(',', $record['rights_ids']) : null)
        );
    }

    /**
     * 后驱
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access protected
     */
    protected function _otherJobsAfterEdit($id)
    {
        parent::_otherJobsAfterEdit($id);
        $this->_linkedData     = HClass::quickLoadModel('linkeddata');
        $this->_addRightsLinkedData($id);
        $this->_addLeftMenuLinkedData($id);
        $this->_addModelManagerLinkedData($id);
    }

    /**
     * 加入权限关联数据
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     */
    private function _addRightsLinkedData($id)
    {
        $ids      = array_filter(explode(',', HRequest::getParameter('rights_ids')));
        if(!$ids) {
            return;
        }
        $list           = array();
        $this->_linkedData->setRelItemModel('actor', 'rights');
        $this->_linkedData->deleteByWhere('`rel_id` = ' . $id);
        foreach($ids as $itemId) {
            $list[]     = array(
                'item_id' => $itemId,
                'rel_id' => $id,
                'author' => HSession::getAttribute('id', 'user')
            );
        }
        $this->_linkedData->addMore('`item_id`, `rel_id`, `author`', $list);
    }

    /**
     * 加入右菜单关联数据
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     */
    private function _addLeftMenuLinkedData($id)
    {
        $ids      = array_filter(explode(',', HRequest::getParameter('leftmenu_ids')));
        if(!$ids) {
            return;
        }
        $list           = array();
        $this->_linkedData->setRelItemModel('actor', 'leftmenu');
        $this->_linkedData->deleteByWhere('`rel_id` = ' . $id);
        foreach($ids as $itemId) {
            $list[]     = array(
                'item_id' => $itemId,
                'rel_id' => $id,
                'author' => HSession::getAttribute('id', 'user')
            );
        }
        $this->_linkedData->addMore('`item_id`, `rel_id`, `author`', $list);
    }

    /**
     * 加入模块关联数据
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access private
     */
    private function _addModelManagerLinkedData($id)
    {
        $ids      = array_filter(explode(',', HRequest::getParameter('modelmanager_ids')));
        if(!$ids) {
            return;
        }
        $list           = array();
        $this->_linkedData->setRelItemModel('actor', 'modelmanager');
        $this->_linkedData->deleteByWhere('`rel_id` = ' . $id);
        foreach($ids as $itemId) {
            $list[]     = array(
                'item_id' => $itemId,
                'rel_id' => $id,
                'author' => HSession::getAttribute('id', 'user')
            );
        }
        $this->_linkedData->addMore('`item_id`, `rel_id`, `author`', $list);
    }

    /**
     * 得到权限资源条件
     *  
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @return 权限资源ID的条件部分
     */
    private function _getItemIdWhere()
    {
    	if(0 < intval(HRequest::getParameter('p_id'))) {
    		return '`item_id` = ' . HRequest::getParameter('item_id');
    	}
    	$model 		= HClass::quickLoadModel(HRequest::getParameter('item_model'));
    	
        return '`item_id` = ' . HRequest::getParameter('item_id') 
            . ' OR ' . HSqlHelper::whereInByListMap(
            'item_id', 'id',
            $model->getAllRows('`parent_id` = ' . HRequest::getParameter('item_id'))
        );
    }

}

?>
