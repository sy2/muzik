<?php

/**
 * @version			$Id$
 * @create 			2015-04-21 01:04:46 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('config.popo.advpopo, app.admin.action.AdminAction, model.advmodel');

/**
 * 大图展示的动作类 
 * 
 * 主要处理后台管理主页的相关请求动作 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		app.admin.action
 * @since 			1.0.0
 */
class AdvAction extends AdminAction
{

    /**
     * 构造函数 
     * 
     * 初始化类变量 
     * 
     * @access public
     */
    public function __construct() 
    {
        parent::__construct();
        $this->_catIdentifier   = 'cat-adv-xxyy';
        $this->_popo        = new AdvPopo();
        $this->_model       = new AdvModel($this->_popo);
    }

    /**
     * 做一些其他的任务 
     * 
     * @access protected
     */
    protected function _otherJobsAfterInfo()
    {
        $showList   = AdvPopo::$isShowMap;

        HResponse::setAttribute('is_show_list', $showList);
    }

    /**
     * 编辑前的动作
     */
    protected function _otherJobsBeforeEdit()
    {
        parent::_otherJobsBeforeEdit();
        HRequest::setParameter('shop_id', 1);
        HRequest::setParameter('start_time', strtotime(HRequest::getParameter('start_time')));
        HRequest::setParameter('end_time', strtotime(HRequest::getParameter('end_time')));
    }

    /**
     * 加载完列表的方法
     *
     * @return [type] [description]
     */
    protected function _otherJobsAfterList() 
    {
        parent::_otherJobsAfterList();
        HResponse::registerFormatMap('is_show', 'name', AdvPopo::$isShowMap);
        HResponse::$formatDateTimeMap['start_time']     = 'Y-m-d H:i:s';
        HResponse::$formatDateTimeMap['end_time']       = 'Y-m-d H:i:s';
    }
}

?>
