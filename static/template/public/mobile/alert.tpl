<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
    <head>
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
      <title>系统助手提示您</title>
        <meta name="keywords" content="<?php echo $siteCfg['seo_keywords'];?>">
        <meta name="description" content="<?php echo $siteCfg['seo_desc'];?>">
        <meta name="author" content="<?php echo $siteCfg['ceo'];?>, 红橘子科技">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>bootstrap/v3/css/bs-row.min.css" />
        <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>font/glyphicon/css/glyphicon.min.css" />
        <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>ratchet/v2/css/ratchet.min.css" />
        <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>ratchet/v2/css/ratchet-theme-ios.min.css" />
        <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>css/hjz-style.css" />
        <link rel="stylesheet" href="<?php echo HResponse::uri('tpl'); ?>/public/mobile/css/style-green.css" />
        <script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>zepto/zepto.min.js"></script>
        <script type="text/javascript">
            var siteUrl     = '<?php echo HResponse::url(); ?>';
            var queryUrl    = siteUrl + 'index.php/';
            var cdnUrl      = '<?php echo HResponse::uri('cdn'); ?>';
            var siteUri     = '<?php echo HResponse::uri(); ?>';
            var modelZhName = '<?php echo $modelZhName; ?>';
            var modelEnName = '<?php echo $modelEnName; ?>';
        </script>
    </head>
    <body class="page-alert page-grey-alert">
        <header class="bar bar-nav top-bar-nav">
            <div class="row">
                <div class="col-xs-3">
                    <a href="<?php echo is_int($url) ? 'javascript:history.go(' . $url . ')': $url; ?>" class="fs-18 back">
                        <i class="glyphicon glyphicon-menu-left"></i>
                    </a>
                </div>
                <div class="col-xs-6 pl-0">
                    <h1 class="title">系统助手提示您</h1>
                </div>
                <div class="col-xs-3"></div>
            </div>
        </header>
        <div class="content">
            <div class="content-box mt-20">
                <div class="content-padded">
                    <p class="text-center glyphicon-box">
                    <?php 
                        $typeMap     = array(
                            'status200' => array(
                                'text-green',
                                'glyphicon-ok-sign'
                            ),
                            'status201' => array(
                                'text-blue',
                                'glyphicon-info-sign'
                            ),
                            'status400' => array(
                                'text-orange',
                                'glyphicon-exclamation-sign'
                            ),
                            'status500' => array(
                                'text-red',
                                'glyphicon-remove-circle'
                            ),
                        );
                    ?>
                        <i class="glyphicon <?php echo $typeMap['status' . $status][0] . ' ' .  $typeMap['status' . $status][1]; ?>"></i>
                    </p>
                    <p class="text-center">
                        <?php echo $message; ?>
                    </p>
                    <p class="fs-12 text-grey text-center">
                       （请您稍等，<b id='close-time'><?php echo $time; ?></b>秒后页面将跳转...） 
                    </p>
                </div>
            </div>
            <div class="content-padded bg-grey">
                <div class="mt-20">
                    <?php if(is_int($url)) { ?>
                    <a href="javascript:history.go(<?php echo $url; ?>);" class="btn btn-green btn-block">立即返回</a>
                    <?php } else { ?>
                    <a href="<?php echo $url; ?>" class="btn btn-green btn-block">立即返回</a>
                    <?php }?>
                </div>
            </div>
            <script type="text/javascript">
                $(function() {
                    setTimeout(function() {
                    <?php if(is_int($url)) { ?>
                        history.go(<?php echo $url; ?>);
                    <?php } else { ?>
                        window.location.href    = '<?php echo $url; ?>';
                    <?php }?>
                    }, <?php echo $time * 1000; ?>);
                    setInterval(function() {
                        var time    = parseInt($('#close-time').html()) - 1;
                        time        = time < 1 ? 0 : time;
                        $('#close-time').html(time);
                    }, 800);
                });
            </script> 
        </div>
    </body>
</html>
