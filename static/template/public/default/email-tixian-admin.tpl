<html><meta charset="utf-8" />
<table style="border:3px solid #25cb83;width:534px" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td><table style="font-size:14px" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <td>
          <img>
            <table cellspacing="0" cellpadding="0" border="0">
              <tbody><tr>
                <td style="padding:20px 16px;color:#333"><table cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                      <td style="height:28px;padding-left:14px"><h1>Hi, 提现管理员!</h1></td>
                    </tr>
                    <tr>
                      <td style="height:28px;padding-left:14px"><?php echo HSession::getAttribute('name', 'user');?>正在申请 <b style="color:#f3750f"><a  style="color:#25cb83;" href="<?php echo $url;?>" target="_blank">蔬享乐园</a></b>提现</td>
                    </tr>
                    <tr>
                      <td style="color:#666;height:28px;padding-left:14px">
                        <h2>提现信息：</h2>
                        <ul style="border-left: 4px solid #e0e0e0; padding-left: 15px; font-size: 14px;">
                          <li><strong>姓名</strong>：<?php echo $record['name'];?></li>
                          <li><strong>金额</strong>：￥ <?php echo $record['money'];?></li>
                          <li><strong>方式</strong>：<?php echo TixianPopo::$typeMap[$record['type']]['name'];?></li>
                        </ul>
                        点击以下链接即可了解详情：<br/>
                        <a href="<?php echo $adminUrl;?>" style="color:#25cb83;"><strong><?php echo $adminUrl;?></strong></a>，请尽快处理哦～
                      </td>
                    </tr>
                    <tr>
                      <td style="text-align: right; color:#999;font-size:12px;display:block;border-top:1px dotted #9f9f9f;padding-top:18px;padding-left:14px">我是系统自动发送的邮件，请不要直接回复哦。 <a  style="color:#25cb83;" href="http://hhhvd.dingding168.com">蔬享乐园</a>&nbsp;账务管理团队</td>
                    </tr>
                  </tbody></table></td>
              </tr>
            </tbody></table></td>
        </tr>
      </tbody></table></td>
  </tr>
</tbody></table>
</html>
