﻿<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <?php 
                    $copyRecord     = HResponse::getAttribute('copyRecord'); 
                    $record         = HResponse::getAttribute('record'); 
                ?>   
                <?php require_once(HResponse::path() . '/common/cur-location.tpl'); ?>
                    <div class="row-fluid">
                    <!-- PAGE CONTENT BEGINS HERE -->
                        <form action="<?php echo HResponse::url($modelEnName . '/' . HResponse::getAttribute('nextAction') ); ?>" method="post" enctype="multipart/form-data" id="info-form">
                            <div class="row-fluid">

                                <div class="span9 content-box">
                                <!-- PAGE CONTENT BEGINS HERE -->

                                    <?php $field = 'id'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                    <?php $record = !$record ? $copyRecord : $record; ?>
                                    <h3 class="title">基础设置</h3>
<?php $field = 'name'; require(HResponse::path() . '/fields/text.tpl'); ?>
<?php $field = 'wx_name'; require(HResponse::path() . '/fields/text.tpl'); ?>
<?php $field = 'app_id'; require(HResponse::path() . '/fields/text.tpl'); ?>
<?php $field = 'app_secret'; require(HResponse::path() . '/fields/text.tpl'); ?>
<?php $field = 'url'; require(HResponse::path() . '/fields/text.tpl'); ?>
<?php $field = 'token'; require(HResponse::path() . '/fields/text.tpl'); ?>
<?php $field = 'encoding_aeskey'; require(HResponse::path() . '/fields/text.tpl'); ?>
<?php $field = 'encode_method'; require(HResponse::path() . '/fields/select.tpl'); ?>
<?php $field = 'src_id'; require(HResponse::path() . '/fields/text.tpl'); ?>
<?php $field = 'image_path'; require(HResponse::path() . '/fields/image.tpl'); ?>
<?php $field = 'welcome'; require(HResponse::path() . '/fields/textarea.tpl'); ?>
<?php $field = 'func_list'; require(HResponse::path() . '/fields/textarea.tpl'); ?>
<h3 class="title">威富通支付设置</h3>
<?php $field = 'wft_mchid'; require(HResponse::path() . '/fields/text.tpl'); ?>
<?php $field = 'wft_key'; require(HResponse::path() . '/fields/text.tpl'); ?>
<h3 class="title">微信支付设置</h3>
<?php $field = 'mchid'; require(HResponse::path() . '/fields/text.tpl'); ?>
<?php $field = 'pay_key'; require(HResponse::path() . '/fields/text.tpl'); ?>
<div class="row-fulid">
    <div class="span4">
<?php $field = 'sslcert_path'; require(HResponse::path() . '/fields/file.tpl'); ?>
    </div>
    <div class="span4">
<?php $field = 'sslkey_path'; require(HResponse::path() . '/fields/file.tpl'); ?>
    </div>
    <div class="span4">
<?php $field = 'sslca_path'; require(HResponse::path() . '/fields/file.tpl'); ?>
    </div>
</div>

                                    
                                    
                                <!-- PAGE CONTENT ENDS HERE -->

                                </div>
                                <div class="span3">
                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4>发布</h4>
                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
                                    <?php $field = 'shop_id'; require(HResponse::path() . '/fields/select.tpl'); ?>
                                                <?php $field = 'create_time'; require(HResponse::path() . '/fields/datetime.tpl'); ?>

                                                <?php require_once(HResponse::path() . '/common/info-buttons.tpl'); 
 ?>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <?php 
                                        if($modelCfg && '2' == $modelCfg['has_multi_lang']) { 
                                            require_once(HResponse::path() . '/common/lang-widget.tpl'); 
                                            echo '<hr/>';
                                        }
                                    ?>  

                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4>维护</h4>
                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <?php require_once(HResponse::path() . '/fields/author.tpl'); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                             </div><!--/row-->

                          </div>
                         </form>
                    <!-- PAGE CONTENT ENDS HERE -->
                     </div><!--/row-->
                </div><!--/#page-content-->
			</div><!-- #main-content -->
		</div><!--/.fluid-container#main-container-->
        <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
        <script type="text/javascript"> var fromId = '<?php echo HRequest::getParameter('fid');?>';</script>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>/js/info.js"></script>
	</body>
</html>
