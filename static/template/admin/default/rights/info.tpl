﻿<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                    <?php 
 $record         = HResponse::getAttribute('record'); ?>    
                    <?php require_once(HResponse::path() . '/common/cur-location.tpl'); ?>
						<div class="row-fluid">
                        <!-- PAGE CONTENT BEGINS HERE -->
                            <form  action="<?php echo HResponse::url($modelEnName . '/' . HResponse::getAttribute('nextAction') ); ?>" method="post" enctype="multipart/form-data" id="info-form">
                                <div class="span9 content-box">
                                  <div class="tabbable tabs-shadow tabs-space">
                                    <ul class="nav nav-tabs" id="myTab">
                                      <li class="active"><a data-toggle="tab" href="#base-box"><i class="pink icon-leaf bigger-110"></i> 基本信息</a></li>
                                    </ul>
                                      <div class="tab-content">
                                        <div id="base-box" class="tab-pane in active">
                                          <input type="hidden" name="id" value="<?php echo $record['id'];?>" />
                                          <?php $field = 'name'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                          <?php $field = 'identifier'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                          <?php $field = 'parent_id'; require(HResponse::path() . '/fields/tree.tpl'); ?>
                                          <?php $field = 'app'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                          <?php $field = 'description'; require(HResponse::path() . '/fields/textarea.tpl'); ?>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                                <div class="span3">
                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4>发布</h4>
                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
                                              <?php $field = 'create_time'; require(HResponse::path() . '/fields/datetime.tpl'); ?>
                                              <?php require_once(HResponse::path() . '/fields/author.tpl'); ?>
                                                <?php require_once(HResponse::path() . '/common/info-buttons.tpl'); 
 ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                             </form>
                        <!-- PAGE CONTENT ENDS HERE -->
						 </div><!--/row-->
					</div><!--/#page-content-->
                <?php require_once(HResponse::path() . '/common/setting-bar.tpl'); ?>  
			</div><!-- #main-content -->
		</div><!--/.fluid-container#main-container-->
        <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>js/info.js?v=<?php echo $v;?>"></script>
	</body>
</html>
