<?php require_once(HResponse::path('admin') . '/common/header.tpl'); ?>
    </head>
    <body class="login-layout" style="background-image: url('<?php echo HResponse::getAttribute('bingBg');?>');">
        <div class="container-fluid" id="main-container">
            <div id="main-content">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="login-container">
                            <div class="row-fluid">
                                <div class="center">
                                    <h1>
                                        <img src="<?php echo HResponse::touri($siteCfg['image_path']); ?>" style="height: 36px;"/>
                                        <span class="white" title="<?php echo $siteCfg['name']; ?>"><?php echo HString::cutString($siteCfg['name'], 8, '...'); ?></span>
                                    </h1>
                                </div>
                            </div>
                            <div class="space-6"></div>
                            <div class="row-fluid">
                                <div class="position-relative">
                                    <div id="login-box" class="visible widget-box no-border">
                                        <div class="widget-body">
                                         <div class="widget-main">
                                            <h4 class="header lighter bigger"><i class="icon-coffee green"></i>请输入您的的登陆信息</h4>
                                            <div class="space-6"></div>
                                            <form action="<?php echo HResponse::url('auser/login', '', 'oauth'); ?>" id="login-form" method="post">
                                                 <fieldset>
                                                    <label>
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" class="span12" placeholder="注册用户名或邮箱地址" name="name" id="in-email" value=""/>
                                                            <i class="icon-user"></i>
                                                        </span>
                                                    </label>
                                                    <label>
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="password" class="span12" placeholder="密码" value="" id="password" name="password"/>
                                                            <i class="icon-lock"></i>
                                                        </span>
                                                    </label>
                                                    <label>
                                                        <img src="<?php echo HResponse::url('vcode', '', 'public'); ?>" class="pull-right" title="看不清楚?请点击更换." id="vcode-img"/>
                                                        <input type="text" class="span9" placeholder="请输入验证码..." id="vcode" name="vcode" data-def="请输入验证码..."/>
                                                    </label>
                                                    <div class="space"></div>
                                                    <button id="login-btn" class="btn btn-block btn-primary"><i class="icon-key"></i> 进入系统</button>
                                                    <input type="hidden" value="<?php echo HResponse::url('', '', 'admin'); ?>" name="next_url" />
                                                  </fieldset>
                                            </form>
                                         </div><!--/widget-main-->
                                         <div class="toolbar clearfix">
                                            <div>
                                                <a href="<?php echo HResponse::url(); ?>" class="user-signup-link"><i class="icon-arrow-left"></i> 返回前台? </a>
                                            </div>
                                            <div>
                                                <a href="javascript:void(0);" id="forgot-box-btn" class="forgot-password-link"> 忘记密码? <i class="icon-arrow-right"></i></a>
                                            </div>
                                         </div>
                                        </div><!--/widget-body-->
                                    </div><!--/login-box-->
                                    
                                    <div id="forgot-box" class="widget-box no-border" style="display:none;">
                                        <div class="widget-body">
                                         <div class="widget-main">
                                            <h4 class="header red lighter bigger"><i class="icon-key"></i> 找回密码</h4>
                                            <div class="space-6"></div>
                                            <p>请输入您的账号邮箱（或联系管理人员修改即可哦）</p>
                                            <form action="<?php echo HResponse::url('enter/send'); ?>" id="forgot-form">
                                                 <fieldset>
                                                    <label>
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="email" class="span12" placeholder="邮箱地址" id="email" name="email"/>
                                                            <i class="icon-envelope"></i>
                                                        </span>
                                                    </label>
                                                    <div class="row-fluid">
                                                        <button id="forgot-btn" class="span5 offset7 btn btn-small btn-danger"><i class="icon-lightbulb"></i>发送邮件!</button>
                                                    </div>
                                                  </fieldset>
                                            </form>
                                         </div><!--/widget-main-->
                                        
                                         <div class="toolbar center">
                                            <a href="#" id="login-box-btn" class="back-to-login-link">返回登陆 <i class="icon-arrow-right"></i></a>
                                         </div>
                                        </div><!--/widget-body-->
                                    </div><!--/forgot-box-->
                                </div><!--/position-relative-->
                            </div>
                        </div>
                    </div><!--/span-->
                </div><!--/row-->
            </div>
        </div><!--/.fluid-container-->
        <div class="footer-box text-center">
            &copy;2012 - <?php echo date('Y');?>
            <a href="http://www.hongjuzi.com.cn/" target="_blank">
                技术支持-温州乔宇科技
            </a>
        </div>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>/js/hjz-extend.js"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>/js/login.js"></script>
    </body>
</html>