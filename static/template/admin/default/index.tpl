<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
	</head>
	<body>
        <?php
            $curActorInfo   = HResponse::getAttribute('curActorInfo');
        ?>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-dashboard"></i> <a href="<?php echo HResponse::url('index', '', 'admin'); ?>">后台概览</a>
                        </li>
                    </ul><!--.breadcrumb-->
                    <div id="nav-search">
                        <span id="time-info">正在加载时钟...</span>
                    </div><!--#nav-search-->

                </div><!--#breadcrumbs-->
                <div id="page-content" class="clearfix">
                    <div class="row-fluid text-center" style="margin-top: 200px;">
                        <h1>欢迎使用Kirka后台管理系统</h1>
                    </div>
                </div><!--/#page-content-->
			</div><!-- #main-content -->
		</div><!--/.fluid-container#main-container-->
        <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
    </body>
</html>
