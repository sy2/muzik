<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
	</head>
	<body style="font-size:12px;margin: 50px 0;">
		<div style="width:200px; margin: 0 auto; border:1px solid #999; text-align:center; padding: 20px 10px;">
            <div style="line-height:30px;">
                <p style="font-size:20px; font-weight:bold; letter-spacing:2px;">怀化好味道餐饮</p>
                <p style="font-size:12px;">电话:0745-2207598</p>
                <p style="font-size:18px; border-bottom:1px solid #000; padding-bottom:10px;">客人存根</p>
            </div>
            <div>
                <p style="margin-bottom:0;">
                    <span style="text-align:left; display:inline-block; width:90px;">台号：51</span>
                    <span style="text-align:left; display:inline-block; width:100px;">单号：00110</span>
                </p>
                <p style="margin-bottom:0;">
                    <span style="text-align:left; display:inline-block; width:90px;">人数：2</span>
                    <span style="text-align:left; display:inline-block; width:100px;">印单员工：张小朵</span>
                </p>
                <p style="margin-bottom:0;">
                    <span style="text-align:left; display:inline-block; width:90px;">时间：14:19:12</span>
                    <span style="text-align:left; display:inline-block; width:100px;">开台员工：向日葵</span>
                </p>
            </div>
            <div style="margin-top:10px; border-bottom: 1px dashed #000;    ">
                <p style="border-bottom: 1px dashed #000; padding-bottom:5px;">
                    <span style="text-align:left; display:inline-block; width:100px;">菜品名称</span>
                    <span style="text-align:center; display:inline-block; width:45px;">数量</span>
                    <span style="text-align:center; display:inline-block; width:45px;">小计</span>
                </p>
                <p style="margin-bottom:5px;">
                    <span style="text-align:left; display:inline-block; width:100px;">蒜香鸡排</span>
                    <span style="text-align:center; display:inline-block; width:45px;">1</span>
                    <span style="text-align:center; display:inline-block; width:45px;">12</span>
                </p>
                <p style="margin-bottom:5px;">
                    <span style="text-align:left; display:inline-block; width:100px;">罗勒松子酱面</span>
                    <span style="text-align:center; display:inline-block; width:45px;">2</span>
                    <span style="text-align:center; display:inline-block; width:45px;">50</span>
                </p>
                <p style="margin-bottom:5px;">
                    <span style="text-align:left; display:inline-block; width:100px;">菌菇面</span>
                    <span style="text-align:center; display:inline-block; width:45px;">4</span>
                    <span style="text-align:center; display:inline-block; width:45px;">20</span>
                </p>
            </div>
            <div style="margin-top:10px;">
                <div style="float: left;">
                    <p style="margin-top:30px;">折扣代码：</p>
                </div>
                <div style="float: right; text-align: left; margin-right:11px;">
                    <p>
                        <span>消费合计：</span>
                        <span>310</span>
                    </p>
                    <p>
                        <span>折扣金额：</span>
                        <span>0</span>
                    </p>
                    <p>
                        <span>应收现金：</span>
                        <span style="font-weight:bold; font-size:14px;">310</span>
                    </p>
                </div>
            </div>
            <div style="clear:both;"></div>
            <div style="margin-top: 30px; font-size: 14px;">
                <p style="margin-bottom:0;">多谢惠顾</p>
                <p style="margin-bottom:0;">欢迎再次光临</p>
            </div>
        </div>
        <script type="text/javascript">
            $(function() {
                setTimeout(function() {
                    window.print();
                    $('#btn-print').show();
                }, 1000);
                $('#btn-print').click(function() { 
                    $(this).hide();
                    setTimeout(function() {
                        window.print();
                        $('#btn-print').show();
                    }, 500);
                });
            });
        </script>
	</body>
</html>
