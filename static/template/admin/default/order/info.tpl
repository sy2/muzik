﻿<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <?php 
                    $record         = HResponse::getAttribute('record');
                    $shopInfo       = HResponse::getAttribute('shopInfo');
                    $rate           = floatval($shopInfo['rate']);
                    $userInfo       = HResponse::getAttribute('userInfo');
                    $shopMap        = HResponse::getAttribute('shopMap');
                    $skuMap         = HResponse::getAttribute('skuMap');
                    $zhuoHaoMap     = HResponse::getAttribute('zhuoHaoMap');
                ?>   
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i> <a href="<?php echo HResponse::url('', '', 'admin'); ?>">后台桌面</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li><a href="<?php echo HResponse::url($modelEnName); ?>"><?php echo $modelZhName; ?></a> <span class="divider"><i class="icon-angle-right"></i></span></li>
                        <li class="active"><?php echo $modelZhName; ?><?php HTranslate::_('内容'); ?></li>
                    </ul><!--.breadcrumb-->
                    <div id="nav-search">
                        <span id="time-info">正在加载时钟...</span>
                    </div><!-- #nav-search -->
                </div><!-- #breadcrumbs -->
                <div id="page-content" class="clearfix">
                    <div class="page-header position-relative">
                        <h1>
                            <?php
                                echo '订单详情 - ';
                                echo isset($record['name']) ? $record['name'] . ' 【支付过期时间：' . date('Y-m-d H:i:s', $record['end_time']) . '】': 'ID：' . $record['id'];
                                $preRecord    = HResponse::getAttribute('preRecord');
                                $nextRecord   = HResponse::getAttribute('nextRecord');
                            ?>
                        </h1>
                    </div><!--/page-header-->             
                    <div class="row-fluid">
                    <!-- PAGE CONTENT BEGINS HERE -->
                        <div class="span9 content-box">
                            <?php $field = 'id'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                            <!-- PAGE CONTENT BEGINS HERE -->
                            <div class="row-fluid">
                                <table width="100%" cellpadding="3" cellspacing="1" class="table table-hover table-bordered">
                                      <tbody>
                                      <tr>
                                        <td colspan="4" class="bg-grey">
                                          <div class="text-center">
                                           <?php if(!$preRecord) { ?>
                                            <a href="javascript:void(0);" class="disabled btn-next-order btn btn-mini btn-grey">
                                                <i class="icon icon-arrow-left"></i> 上一个订单</a>
                                            <?php } else { ?>
                                            <a href="<?php echo HResponse::url('order/editview', 'id=' . $preRecord['id']);?>" class="btn-next-order btn btn-mini btn-info">
                                                <i class="icon icon-arrow-left"></i> 上一个订单</a>
                                            <?php } ?>
                                              <a data-id="<?php echo $record['id']; ?>" class="btn-print btn btn-mini btn-success" href="javascript:;">
                                                  <i class="icon icon-print"></i> 打印客户单
                                              </a>
                                              <a data-id="<?php echo $record['id']; ?>" class="btn-print-jie-zhang btn btn-mini btn-danger" href="javascript:;">
                                                  <i class="icon icon-print"></i> 打印结账清单
                                              </a>
                                              <a data-id="<?php echo $record['id']; ?>" class="btn-print-dui-zhang btn btn-mini btn-inverse" href="javascript:;">
                                                  <i class="icon icon-print"></i> 打印对账清单
                                              </a>
                                           <?php if(!$nextRecord) { ?>
                                            <a href="javascript:void(0);" class="disabled btn-pre-order btn btn-mini btn-grey mr-5">
                                                <i class="icon icon-arrow-right"></i> 下一个订单</a>
                                           <?php } else { ?>
                                            <a href="<?php echo HResponse::url('order/editview', 'id=' . $nextRecord['id']);?>" class="btn-pre-order btn btn-mini btn-info mr-5">
                                                <i class="icon icon-arrow-right"></i> 下一个订单</a>
                                           <?php } ?>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <th colspan="4" class=" text-left">
                                            <i class="icon icon-info-sign"></i> 基本信息
                                        </th>
                                      </tr>
                                      <tr>
                                        <td width="18%"><div align="right"><strong>订单ID：</strong></div></td>
                                        <td width="34%"><?php echo $record['id'] . '(编号：' . $record['name'] . ')';?></td>
                                        <td width="15%"><div align="right"><strong>订单状态：</strong></div></td>
                                        <td>
                                            <?php echo OrderPopo::$statusMap[$record['status']]['name'];?>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><div align="right"><strong>下单人：</strong></div></td>
                                        <td><?php echo $userInfo['name'];?> </td>
                                        <td><div align="right"><strong>下单时间：</strong></div></td>
                                        <td><?php echo $record['create_time'];?></td>
                                      </tr>
                                      <tr>
                                        <td><div align="right"><strong>支付方式：</strong></div></td>
                                        <td>
                                          <?php 
                                            $orderPaymentList   = HResponse::getAttribute('orderPaymentList');
                                            foreach($orderPaymentList as $key => $item) {
                                          ?>
                                          <?php echo $key != 0 ? '，' . OrderpaymentPopo::$typeMap[$item['type']]['name'] : OrderpaymentPopo::$typeMap[$item['type']]['name']; ?>
                                          <?php } ?>
                                        </td>
                                        <td><div align="right"><strong>付款时间：</strong></div></td>
                                        <td><?php echo !$record['pay_time'] ? '未付款' : date('Y-m-d H:i:s', $record['pay_time']);?></td>
                                      </tr>
                                      <tr>
                                        <td><div align="right"><strong>第三方订单编号：</strong></div></td>
                                        <td><?php echo $record['trade_no'];?></td>
                                        <td><div align="right"><strong>订单来源：</strong></div></td>
                                        <td><?php echo OrderPopo::$oFromMap[$record['o_from']]['name'];?></td>
                                      </tr>
                                      <tr>
                                        <td><div align="right"><strong>是否客户确认：</strong></div></td>
                                        <td><?php echo  1 == $record['user_sure'] ? '否' : '是';?></td>
                                        <td><div align="right"><strong>是否评价：</strong></div></td>
                                        <td><?php echo 1 == $record['is_comment'] ? '否' : '是';?></td>
                                      </tr>
                                      <tr>
                                        <td><div align="right"><strong>合并支付：</strong></div></td>
                                        <td><?php echo 1 == $record['is_merge'] ? '否' : '是';?></td>
                                        <td><div align="right"><strong>完成时间：</strong></div></td>
                                        <td><?php echo !$record['done_time'] ? '无' : date('Y-m-d H:i:s', $record['done_time']);?></td>
                                      </tr>
                                      <tr>
                                        <td><div align="right"><strong>客户给商家的留言：</strong></div></td>
                                        <td colspan="3"><?php echo $record['comment'];?></td>
                                      </tr>
                                      <?php 
                                        $address  = json_decode($record['address'], true);
                                        if($address) {  
                                      ?>
                                      <tr>
                                        <th colspan="4" class=" text-left">
                                            <i class="icon icon-user"></i> 收货人信息
                                            <?php if(1 == $record['status']){ ?>
                                            <a href="###" id="edit-order-address" class="pull-right special">编辑</a>
                                            <?php } ?>
                                        </th>
                                        </tr>
                                      <tr>
                                        <td><div align="right"><strong>收货人：</strong></div></td>
                                        <td><?php echo $address['name'];?></td>
                                        <td><div align="right"><strong>手机：</strong></div></td>
                                        <td><?php echo $address['phone'];?></td>
                                      </tr>
                                      <tr>
                                        <td><div align="right"><strong>地址：</strong></div></td>
                                        <td><?php echo $address['detail'];?></td>
                                        <td><div align="right"><strong>邮编：</strong></div></td>
                                        <td><?php echo $address['code'];?></td>
                                      </tr>
                                      <?php } ?>
                                      <?php $fapiao   = HResponse::getAttribute('fapiao');?>
                                      <?php if($fapiao) { ?>
                                      <tr>
                                        <th colspan="4" class=" text-left">
                                            <i class="icon icon-file"></i>
                                            发票信息
                                      <?php if(1 == $record['status'] || 2==$record['status']) { ?>
                                            <a href="###" id="edit-order-fapiao" class="pull-right special">编辑</a>
                                      <?php } ?>
                                        </th>
                                      </tr>
                                      <tr>
                                        <td><div align="right"><strong>发票类型：</strong></div></td>
                                        <td><?php echo !$fapiao ? '无' : (1 == $fapiao['type'] ? '个人' : '公司');?></td>
                                        <td><div align="right"><strong>发票抬头：</strong></div></td>
                                        <td><?php echo $fapiao['name'];?></td>
                                      </tr>
                                      <tr>
                                        <td><div align="right"><strong>发票内容：</strong></div></td>
                                        <td colspan="3"><?php echo $fapiao['content'];?></td>
                                      </tr>
                                      <?php } ?>
                                    </tbody>
                                </table>
                                <?php $wuliu     = HResponse::getAttribute('wuliu'); ?>
                                <?php if($wuliu) { ?>
                                <hr class="clearfix">
                                <table class="table table-hover table-bordered" width="100%" cellpadding="3" cellspacing="1">
                                  <tbody><tr>
                                    <th colspan="4" class="bg-grey text-left">
                                        <i class="icon icon-truck"></i>
                                        物流信息
                                      <?php if($record['status'] == 10 && ($record['process'] == 1 || $record['process'] == 4)) { ?>
                                        <a href="###" id="edit-order-wuliu" class="pull-right special">编辑</a>
                                      <?php } ?>
                                    </th>
                                  </tr>
                                  <tr>
                                    <td width="18%"><div align="right"><strong>物流公司：</strong></div></td>
                                    <td width="34%"><?php echo $wuliu['name'];?></td>
                                    <td width="15%"><div align="right"><strong>物流单号：</strong></div></td>
                                    <td><?php echo $wuliu['code'];?></td>
                                  </tr>
                                  <tr>
                                    <td><div align="right"><strong>物流进度：</strong></div></td>
                                    <td colspan="3"><?php echo $wuliu['content'];?></td>
                                  </tr>
                                </tbody></table>
                                <?php } ?>
                                <hr class="clearfix">
                                <table class="table table-hover table-bordered" width="100%" cellpadding="3" cellspacing="1">
                                  <tbody>
                                    <tr>
                                        <th colspan="7" scope="col" class="bg-grey text-left">
                                            <i class="icon icon-list"></i>
                                            <?php echo $zhuoHaoMap[$record['code']]['name']; ?>菜品信息
                                      <?php if(1 == $record['status']) { ?>
                                            <a href="###" id="edit-order-goods" class="pull-right special">编辑</a>
                                      <?php } ?>
                                        </th>
                                    </tr>
                                  <tr>
                                    <td scope="col"><div align="center"><strong>序号</strong></div></td>
                                    <td scope="col"><div align="center"><strong>菜品名称</strong></div></td>
                                    <td scope="col" class="hide"><div align="center"><strong>规格</strong></div></td>
                                    <td scope="col"><div align="center"><strong>售价</strong></div></td>
                                    <td scope="col"><div align="center"><strong>折扣</strong></div></td>
                                    <td scope="col"><div align="center"><strong>数量</strong></div></td>
                                    <td scope="col" class="hide"><div align="center"><strong>供货商</strong></div></td>
                                    <td scope="col"><div align="center"><strong>小计</strong></div></td>
                                  </tr>
                                    <?php 
                                        $goodsList  = HResponse::getAttribute('goodsList');
                                        $sumGoods   = 0;
                                        foreach($goodsList as $key => $item) {
                                            $shop       = $shopMap[$item['shop_id']];
                                            $sku        = $skuMap[$item['group_id']];
                                            $total      = $item['number'] * $item['price']; 
                                            $sumGoods   += $total;
                                    ?>
                                    <tr>
                                        <td class="text-center"><?php echo $key + 1;?></td>
                                        <td class="text-center">
                                            <a target="_blank" href="<?php echo HResponse::url('goods', 'id=' . $item['goods_id'], HObject::GC('DEF_APP'));?>">
                                                <?php echo $item['name'] ;?>
                                            </a>
                                        </td>
                                        <td class="text-center hide"><?php echo $sku['name']; ?></td>
                                        <td class="text-center"><?php echo $item['price']; ?></td>
                                        <td class="text-center"><?php echo $item['discount']; ?></td>
                                        <td class="text-center"><?php echo $item['number']; ?></td>
                                        <td class="text-center hide"><?php echo $shop['name']; ?></td>
                                        <td><div align="center">￥<span class="item-price"><?php echo $item['number'] * $item['price'];?></span>元</div></td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <th colspan="7" scope="col" class="text-left">
                                            消费金额小计：￥<?php echo $record['src_amount'];?>，
                                            应付金额小计：￥<?php echo $record['amount'];?>
                                        </th>
                                    </tr>
                                  </tbody>
                                </table>
                                <?php 
                                  if($record['is_merge'] == 2) { 
                                    $subOGList    = HResponse::getAttribute('subOrderGoodsList');
                                    $subOrderMap  = HResponse::getAttribute('subOrderMap');
                                    foreach($subOrderMap as $info) {
                                      $record['amount'] += $info['amount'];
                                      $record['src_amount'] += $info['src_amount'];
                                      $record['pay_money'] += $info['pay_money'];
                                      $record['zhekou_amount'] += $info['zhekou_amount'];
                                ?>
                                <hr class="clearfix">
                                <table class="table table-hover table-bordered" width="100%" cellpadding="3" cellspacing="1">
                                  <tbody>
                                    <tr>
                                        <th colspan="7" scope="col" class="bg-grey text-left">
                                            <i class="icon icon-list"></i>
                                            <?php echo $zhuoHaoMap[$info['code']]['name']; ?>菜品信息
                                        </th>
                                    </tr>
                                  <tr>
                                    <td scope="col"><div align="center"><strong>序号</strong></div></td>
                                    <td scope="col"><div align="center"><strong>菜品名称</strong></div></td>
                                    <td scope="col" class="hide"><div align="center"><strong>规格</strong></div></td>
                                    <td scope="col"><div align="center"><strong>售格</strong></div></td>
                                    <td scope="col"><div align="center"><strong>折扣</strong></div></td>
                                    <td scope="col"><div align="center"><strong>数量</strong></div></td>
                                    <td scope="col" class="hide"><div align="center"><strong>供货商</strong></div></td>
                                    <td scope="col"><div align="center"><strong>小计</strong></div></td>
                                  </tr>
                                    <?php 
                                        foreach($subOGList as $key => $item) {
                                            if($item['parent_id'] != $info['id']) {
                                              continue;
                                            }
                                            $total      = $item['number'] * $item['price']; 
                                            $sumGoods   += $total;
                                    ?>
                                    <tr>
                                        <td class="text-center"><?php echo $key + 1;?></td>
                                        <td class="text-center">
                                            <a target="_blank" href="<?php echo HResponse::url('goods', 'id=' . $item['goods_id'], HObject::GC('DEF_APP'));?>">
                                                <?php echo $item['name'] ;?>
                                            </a>
                                        </td>
                                        <td class="text-center hide"><?php echo $sku['name']; ?></td>
                                        <td class="text-center"><?php echo $item['price']; ?></td>
                                        <td class="text-center"><?php echo $item['discount']; ?></td>
                                        <td class="text-center"><?php echo $item['number']; ?></td>
                                        <td class="text-center hide"><?php echo $shop['name']; ?></td>
                                        <td><div align="center">￥<span class="item-price"><?php echo $item['number'] * $item['price'];?></span>元</div></td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <th colspan="7" scope="col" class="text-left">
                                            消费金额小计：￥<?php echo $info['src_amount'];?>，
                                            应付金额小计：￥<?php echo $info['amount'];?>
                                        </th>
                                    </tr>
                                  </tbody>
                                </table>
                                <?php } ?>
                                <?php } ?>
                                <?php 
                                    $zenpingList    = HResponse::getAttribute('zenpingList');
                                    if($zenpingList) { 
                                ?>
                                <hr class="clearfix">
                                <table class="table table-hover table-bordered" width="100%" cellpadding="3" cellspacing="1">
                                  <tbody><tr>
                                    <th colspan="5" class="bg-grey text-left">
                                        <i class="icon icon-tag"></i>
                                        赠品信息
                                      <?php if(1 == $record['status']) { ?>
                                      <a href="###" id="btn-add-order-zengpin" class="pull-right special">添加</a>
                                      <?php } ?>
                                    </th>
                                  </tr>
                                  <tr>
                                    <td scope="col" width="10%"><div align="center"><strong>序号</strong></div></td>
                                    <td scope="col" width="10%"><div align="center" ><strong>原价</strong></div></td>
                                    <td scope="col" width="25%"><div align="center"><strong>赠送内容</strong></div></td>
                                    <td scope="col" width="20%"><div align="center"><strong>图片</strong></div></td>
                                    <td scope="col" width="10%"><div align="center" ><strong>操作</strong></div></td>
                                  </tr>
                                  <?php 
                                    foreach($zenpingList as $key => $item) { 
                                  ?>
                                  <tr>
                                    <td scope="col"><div align="center"><strong><?php echo $key + 1; ?></strong></div></td>
                                    <td scope="col"><div align="center" ><strong><?php echo $item['price'];?></strong></div></td>
                                    <td scope="col" ><div align="center"><strong><?php echo $item['content'];?></strong></div></td>
                                    <td scope="col"><div align="center">
                                        <?php if($item['image_path']) { ?>
                                        <a href="<?php echo HResponse::touri($item['image_path']);?>" class="lightbox" target="_blank">
                                            <img src="<?php echo HResponse::touri($item['image_path']);?>" alt="" class="pic">
                                        </a>
                                        <?php } ?>
                                    </div></td>
                                    <td scope="col"><div align="center" >
                                      <?php if(1 == $record['status']) { ?>
                                      <a class="btn-del" href="<?php echo HResponse::url('orderzenping/delete', 'id=' . $item['id']);?>"><strong>删除</strong></a>
                                      <?php } else { ?>
                                      不能操作
                                      <?php } ?>
                                    </div></td>
                                  </tr>
                                  <?php } ?>
                                </tbody></table>
                                <?php } ?>
                                <?php 
                                  $youhuijuan     = HResponse::getAttribute('youhuijuan'); 
                                  if($youhuijuan) {
                                ?>
                                <hr class="clearfix">
                                <table class="table table-hover table-bordered" width="100%" cellpadding="3" cellspacing="1">
                                  <tbody><tr>
                                    <th colspan="5" class="bg-grey text-left">
                                        <i class="icon icon-bookmark"></i>优惠信息
                                      <?php if(1 == $record['status']) { ?>
                                        <a href="###" id="btn-edit-activity" class="pull-right special">编辑</a>
                                      <?php } ?>
                                    </th>
                                  </tr>
                                  <tr>
                                    <td scope="col" width="10%"><div align="center"><strong>序号</strong></div></td>
                                    <td scope="col" width="25%"><div align="center"><strong>满</strong></div></td>
                                    <td scope="col" width="20%"><div align="center"><strong>减</strong></div></td>
                                  </tr>
                                  <?php if($youhuijuan) { ?>
                                  <tr>
                                    <td scope="col" width="10%"><div align="center"><strong>1</strong></div></td>
                                    <td scope="col" width="25%"><div align="center"><strong><?php echo $youhuijuan['min_money'];?></strong></div></td>
                                    <td scope="col" width="20%"><div align="center"><strong><?php echo $youhuijuan['sub_money'];?></strong></div></td>
                                  </tr>
                                  <?php } ?>
                                </tbody></table>
                                <?php } ?>
                                <?php
                                  $guaDanList     = HResponse::getAttribute('guaDanList'); 
                                  if($guaDanList) {
                                 ?>
                                <hr class="clearfix">
                                <table class="table table-hover table-bordered" width="100%" cellpadding="3" cellspacing="1">
                                  <tbody><tr>
                                    <th colspan="5" class="bg-grey text-left">
                                        <i class="icon icon-bookmark"></i> 挂单信息
                                    </th>
                                  </tr>
                                  <tr>
                                    <td scope="col" width="10%"><div align="center"><strong>客户</strong></div></td>
                                    <td scope="col" width="25%"><div align="center"><strong>类别</strong></div></td>
                                    <td scope="col" width="20%"><div align="center"><strong>备注</strong></div></td>
                                  </tr>
                                  <?php foreach($guaDanList as $guaDanInfo) { ?>
                                  <tr>
                                    <td scope="col" width="10%"><div align="center">
                                    <a href="<?php echo HResponse::url('user/editview', 'id=' . $guaDanInfo['user_id']);?>"><strong><?php echo $guaDanInfo['user_name'];?></strong>
                                    </a></div>
                                    </td>
                                    <td scope="col" width="25%"><div align="center"><strong><?php echo OrderguadanPopo::$typeMap[$guaDanInfo['type']]['name'];?></strong></div></td>
                                    <td scope="col" width="20%"><div align="center"><strong><?php echo $guaDanInfo['comment'];?></strong></div></td>
                                  </tr>
                                  <?php } ?>
                                </tbody></table>
                                <?php } ?>
                                <hr class="clearfix">
                                <?php
                                    $redpackage     = HResponse::getAttribute('lingqian');
                                    $sumActivity    = floatval($youhuijuan['sub_money']);
                                    $sumRedpackage  = floatval($redpackage['money']);
                                    $sum            = $sumGoods + $record['yun_fee'];
                                    $chongZhiInfo   = HResponse::getAttribute('chongZhiInfo');
                                    $chongZhiInfo = 0;
                                    $lingQianInfo = 0;
                                    foreach($orderPaymentList as $key => $item){
                                        if($item['type'] == 12){
                                            $chongZhiInfo = $item['amount'];
                                        }
                                        if($item['type'] == 14){
                                            $lingQianInfo = $item['amount'];
                                        }
                                    }
                                ?>
                                <table class="table table-hover table-bordered" width="100%" cellpadding="3" cellspacing="1">
                                  <tbody><tr>
                                    <th class="bg-grey text-left">
                                        <i class="icon icon-yen"></i>
                                        费用信息
                                    </th>
                                  </tr>
                                  <tr>
                                    <td><div align="right" style="font-size: 18px;">菜品总金额
                                      + 配送费用：<strong>￥<?php echo $record['yun_fee']; ?>元</strong>
                                      = 订单消费金额：<strong>￥<?php echo $record['src_amount'];?>元</strong></div></td>
                                  </tr>
                                  <tr>
                                    <td><div align="fs-16" style="text-align: right!important; line-height:22px;">
                                      - 使用充值： <strong style="font-size: 18px;">￥<?php echo $chongZhiInfo;?>元</strong><br/>
                                      - 使用红包： <strong style="font-size: 18px;">￥<?php echo $lingQianInfo;?>元</strong><br/>
                                      - 使用优惠： <strong style="font-size: 18px;">￥<?php echo $sumActivity;?>元</strong>
                                    </div></td>
                                  </tr>
                                  <tr>
                                    <td class="text-right" style="font-size: 16px;line-height:22px; text-align: right!important;">
                                       = 应付款金额：<strong class="red">￥<?php echo $record['amount'];?>元</strong>
                                       <p style="margin-top: 10px;">
                                        打扣金额：<?php echo $record['zhekou_amount'];?>,
                                        打折比率：<?php echo $record['zhekou'];?>
                                       </p>
                                       <p style="margin-top: 10px;">
                                       实收金额：<?php echo $record['pay_amount'];?>,
                                       找零金额：<?php echo $record['zhaoling'];?>
                                       </p>
                                       <p style="margin-top: 10px;">
                                        开票金额：<?php echo $record['fapiao_amount'];?>
                                       </p>
                                     </td>
                                  </tr>
                                </tbody></table>
                                <hr class="clearfix">
                                <table class="table table-hover table-bordered" width="100%" cellpadding="3" cellspacing="1">
                                  <tbody><tr>
                                    <th colspan="4" class="bg-grey text-left">
                                        实付款方式明细信息
                                    </th>
                                  </tr><tr>
                                    <td class="text-center">
                                        <strong>支付状态</strong>
                                    </th>
                                    <td class="text-center">
                                        <strong>支付方式名称</strong>
                                    </th>
                                    <td class="text-center">
                                        <strong>支付金额</strong>
                                    </th>
                                    <td class="text-center">
                                        <strong>支付说明</strong>
                                    </th>
                                  </tr>
                                  <?php if($record['merge_order_id'] > 0) { ?>
                                  <tr>
                                    <td colspan="4">
                                      <div class="text-center">
                                        本订单通过合并支付，支付详情请查看主订单：
                                        <a href="<?php echo HResponse::url('order/editview', 'id=' . $record['merge_order_id']);?>">
                                        #<?php echo $record['merge_order_id'];?>
                                        </a>
                                      </div>
                                    </td>
                                    </tr>
                                  <?php } else { ?>
                                  <?php 
                                    $oczInfo  = HResponse::getAttribute('oczInfo');
                                    $olqInfo  = HResponse::getAttribute('olqInfo');
                                    if($oczInfo) {
                                  ?>
                                  <tr>
                                    <td> 
                                        <?php echo $oczInfo['status'] == 1 ? '待支付' : '成功';?>
                                    </td>
                                    <td>
                                          会员充值支付
                                    </td>
                                    <td>
                                          ￥<?php echo $oczInfo['money'];?>
                                    </td>
                                    <td>
                                          <?php echo $oczInfo['content'];?>
                                    </td>
                                  </tr>
                                  <?php } ?>
                                  <?php
                                    if($olqInfo) {
                                  ?>
                                  <tr>
                                    <td> 
                                        <?php echo $olqInfo['status'] == 1 ? '待支付' : '成功';?>
                                    </td>
                                    <td>
                                          会员红包支付
                                    </td>
                                    <td>
                                          ￥<?php echo $olqInfo['money'];?>
                                    </td>
                                    <td>
                                          <?php echo $olqInfo['content'];?>
                                    </td>
                                  </tr>
                                  <?php } ?>
                                  <?php 
                                    $orderPaymentList   = HResponse::getAttribute('orderPaymentList');
                                    $totalOrderPayment  = 0;
                                    foreach($orderPaymentList as $item) {
                                        $totalOrderPayment += $item['amount'];
                                  ?>
                                  <tr>
                                    <td>
                                          <?php echo OrderpaymentPopo::$statusMap[$item['status']]['name'];?>
                                    </td>
                                    <td>
                                          <?php echo OrderpaymentPopo::$typeMap[$item['type']]['name'];?>
                                    </td>
                                    <td>
                                          ￥<?php echo $item['amount'];?>
                                    </td>
                                    <td>
                                          <?php echo $item['content'];?>
                                    </td>
                                  </tr>
                                  <?php } ?>
                                  <tr>
                                    <td colspan="4" style="font-size: 22px;line-height:22px; text-align: right!important;">合计：<strong class="red">￥<?php echo floatval($totalOrderPayment);?></strong></td>
                                  </tr>
                                  <?php } ?>
                                </tbody></table>
                            </div>                                    
                            <!-- PAGE CONTENT ENDS HERE -->

                            </div>
                            <div class="span3">
                                <form action="<?php echo HResponse::url($modelEnName . '/' . HResponse::getAttribute('nextAction') ); ?>" method="post" enctype="multipart/form-data" id="info-form">
                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4>操作</h4>
                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
    <?php $field = 'id'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
    <?php $field = 'name'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
    <?php $field = 'address'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
    <?php $field = 'yun_fee'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
    <?php $field = 'parent_id'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
    <?php $field = 'fapiao_id'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
    <?php $field = 'shop_sure'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
    <?php $field = 'is_got_money'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
    <?php $field = 'src_type'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
    <?php $field = 'end_time'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
    <?php $field = 'create_time'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                <?php $field = 'status'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
    <?php require_once(HResponse::path() . '/fields/author.tpl'); ?>
                                                <hr/>
                                                <div class="control-group text-right btn-form-box" data-spy="affix" data-offset-top="160" >
                                                    <?php if(in_array($record['status'], array(1, 11, 12, 13)) && $record['process'] == 1){ ?>
                                                    <a href="<?php echo HResponse::url('order/trash', 'id=' . $record['id']);?>" class="btn btn-mini btn-info">取消订单</a>
                                                    <?php } ?>
                                                    <?php if(($record['status'] == 5 || $record['status'] == 9) && $record['process'] == 1){ ?>
                                                    <a href="<?php echo HResponse::url('order/delete', 'id=' . $record['id']);?>" class="btn btn-mini btn-info">删除订单</a>
                                                    <?php } ?>
                                                    <?php if($record['status'] == 10 && $record['process'] == 1){ ?>
                                                    <a href="javascript:void()" class="btn-edit-order-wuliu btn btn-mini btn-info" data-type="1">去发货</a>
                                                    <a href="<?php echo HResponse::url('orderpayback/paybackapply', 'id=' . $record['id']);?>" class="btn btn-mini btn-info">申请退款</a>
                                                    <?php } ?>
                                                    <?php if($record['status'] == 10 && $record['process'] == 3){ ?>
                                                    <a href="<?php echo HResponse::url('orderpayback/paybackapply', 'id=' . $record['id']);?>" class="btn btn-mini btn-info">申请退款</a>
                                                    <a href="<?php echo HResponse::url('order/noreceived', 'id=' . $record['id']);?>" class="btn btn-mini btn-info">未收货</a>
                                                    <?php } ?>
                                                    <?php if($record['status'] == 10 && $record['process'] == 4){ ?>
                                                    <a href="<?php echo HResponse::url('orderpayback/paybackapply', 'id=' . $record['id']);?>" class="btn btn-mini btn-info">申请退款</a>
                                                    <a href="javascript:void()" class="btn-edit-order-wuliu btn btn-mini btn-info" data-type="2">添加补发</a>
                                                    <?php } ?>
                                                    <?php if($record['status'] == 7 && ($record['process'] == 1 || $record['process'] == 3 || $record['process'] == 4)){ ?>
                                                    <a href="<?php echo HResponse::url('orderpayback/paybackdeal', 'id=' . $record['id']);?>" class="btn btn-mini btn-info">处理退款</a>
                                                    <?php } ?>
                                                    <?php if($record['status'] == 8 && ($record['process'] == 1 || $record['process'] == 3)){ ?>
                                                    <a href="<?php echo HResponse::url('orderpayback/paybackdeal', 'id=' . $record['id']);?>" class="btn btn-mini btn-info">查看退款</a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                         </div><!--/row-->
                      </div>
                    <!-- PAGE CONTENT ENDS HERE -->
                     </div><!--/row-->
                </div><!--/#page-content-->
			</div><!-- #main-content -->
		</div><!--/.fluid-container#main-container-->
        <!-- Modal -->
        <div class="modal modal-lg fade" id="order-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg " role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="order-modal-title"></h4>
                    </div>
                    <div class="modal-body" id="order-modal-content"> </div>
                    <div class="modal-footer">
                        <button id="btn-modal-submit" type="button" class="btn btn-primary btn-edit btn-sm">保存并刷新</button>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript"> 
            var oid = '<?php echo $record['id'];?>';
            var uid = '<?php echo $record['parent_id'];?>';
        </script>
        <?php require_once(HResponse::path() . '/order/parts.tpl'); ?>
        <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
        <script type="text/javascript"> var fromId = '<?php echo HRequest::getParameter('fid');?>';</script>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>js/info.js?v=<?php echo $v;?>"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>/js/order-detail.js"></script>
	</body>
</html>
