﻿<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
	</head>
	<body>
		<div class="container-fluid" id="main-container">
			<div class="clearfix">
                <?php 
                    $record         = HResponse::getAttribute('record');
                    $shopInfo       = HResponse::getAttribute('shopInfo');
                    $rate           = floatval($shopInfo['rate']);
                    $userInfo       = HResponse::getAttribute('userInfo');
                    $shopMap        = HResponse::getAttribute('shopMap');
                    $skuMap         = HResponse::getAttribute('skuMap');
                ?>   
                <div id="page-content" class="clearfix">
                    <div class="page-header position-relative">
                        <h1 class="text-center">
                            <a href="javascript:void(0);" style="display:none;" class="btn btn-mini  btn-info" id="btn-print">
                                <i class="icon icon-print"></i> 开始打印
                            </a>
                            <small class="fs-14 pull-right">打印时间：<?php echo date('Y-m-d H:i:s');?></small>
                            <span class="pull-left">
                            <?php
                                echo '订单详情 - ' .$record['name'];
                            ?>
                            </span>
                        </h1>
                        <div class="clearfix"></div>
                    </div><!--/page-header-->             
                    <div class="row-fluid">
                    <!-- PAGE CONTENT BEGINS HERE -->
                        <div class="span12 content-box">
                            <!-- PAGE CONTENT BEGINS HERE -->
                            <div class="row-fluid">
                                <table width="100%" cellpadding="3" cellspacing="1" class="table table-hover table-bordered">
                                      <tbody>
                                      <tr>
                                        <th colspan="4" class=" text-left">
                                            <i class="icon icon-info-sign"></i> 基本信息
                                        </th>
                                      </tr>
                                      <tr>
                                        <td width="18%"><div align="right"><strong>订单号：</strong></div></td>
                                        <td width="34%"><?php echo $record['name'];?></td>
                                        <td width="15%"><div align="right"><strong>订单状态：</strong></div></td>
                                        <td>
                                            <?php echo OrderPopo::$statusMap[$record['status']]['name'];?>，
                                            <?php echo OrderPopo::$processMap[$record['process']]['name'];?>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><div align="right"><strong>购货人：</strong></div></td>
                                        <td><?php echo $userInfo['name'];?> </td>
                                        <td><div align="right"><strong>下单时间：</strong></div></td>
                                        <td><?php echo $record['create_time'];?></td>
                                      </tr>
                                      <tr>
                                        <td><div align="right"><strong>支付方式：</strong></div></td>
                                        <td><?php echo OrderPopo::$paymentMap[$record['payment']]['name'];?></td>
                                        <td><div align="right"><strong>付款时间：</strong></div></td>
                                        <td><?php echo !$record['pay_time'] ? '未付款' : date('Y-m-d H:i:s', $record['pay_time']);?></td>
                                      </tr>
                                      <tr>
                                        <td><div align="right"><strong>第三方订单编号：</strong></div></td>
                                        <td><?php echo $record['trade_no'];?></td>
                                        <td><div align="right"><strong>订单来源：</strong></div></td>
                                        <td><?php echo OrderPopo::$oFromMap[$record['o_from']]['name'];?></td>
                                      </tr>
                                      <tr>
                                        <td><div align="right"><strong>是否客户确认：</strong></div></td>
                                        <td><?php echo  1 == $record['user_sure'] ? '否' : '是';?></td>
                                        <td><div align="right"><strong>是否评价：</strong></div></td>
                                        <td><?php echo 1 == $record['is_comment'] ? '否' : '是';?></td>
                                      </tr>
                                      <tr>
                                        <td><div align="right"><strong>是否合并：</strong></div></td>
                                        <td><?php echo 1 == $record['is_merge'] ? '否' : '是';?></td>
                                        <td><div align="right"><strong>完成时间：</strong></div></td>
                                        <td><?php echo !$record['done_time'] ? '无' : date('Y-m-d H:i:s', $record['done_time']);?></td>
                                      </tr>
                                      <tr>
                                        <td><div align="right"><strong>客户给商家的留言：</strong></div></td>
                                        <td colspan="3"><?php echo $record['comment'];?></td>
                                      </tr>
                                      <tr>
                                        <th colspan="4">
                                            <i class="icon icon-user"></i> 收货人信息
                                            <?php $address  = json_decode($record['address'], true); ?>
                                        </th>
                                        </tr>
                                      <tr>
                                        <td><div align="right"><strong>收货人：</strong></div></td>
                                        <td><?php echo $address['name'];?></td>
                                        <td><div align="right"><strong>手机：</strong></div></td>
                                        <td><?php echo $address['phone'];?></td>
                                      </tr>
                                      <tr>
                                        <td><div align="right"><strong>地址：</strong></div></td>
                                        <td><?php echo $address['detail'];?></td>
                                        <td><div align="right"><strong>邮编：</strong></div></td>
                                        <td><?php echo $address['code'];?></td>
                                      </tr>
                                      <tr>
                                        <th colspan="4" class=" text-left">
                                            <?php $fapiao   = HResponse::getAttribute('fapiao');?>
                                            <i class="icon icon-file"></i>
                                            发票信息
                                        </th>
                                      </tr>
                                      <tr>
                                        <td><div align="right"><strong>发票类型：</strong></div></td>
                                        <td><?php echo !$fapiao ? '无' : (1 == $fapiao['type'] ? '个人' : '公司');?></td>
                                        <td><div align="right"><strong>发票抬头：</strong></div></td>
                                        <td><?php echo $fapiao['name'];?></td>
                                      </tr>
                                      <tr>
                                        <td><div align="right"><strong>发票内容：</strong></div></td>
                                        <td colspan="3"><?php echo $fapiao['content'];?></td>
                                      </tr>
                                    </tbody>
                                </table>
                                <hr class="clearfix">
                                <table class="table table-hover table-bordered" width="100%" cellpadding="3" cellspacing="1">
                                  <tbody><tr>
                                    <th colspan="6" class="bg-grey text-left">
                                        <?php $wuliu     = HResponse::getAttribute('wuliu'); ?>
                                        <i class="icon icon-truck"></i>
                                        物流信息
                                    </th>
                                  </tr>
                                  <tr>
                                    <td width="18%"><div align="right"><strong>物流公司：</strong></div></td>
                                    <td width="14%"><?php echo $wuliu['name'];?></td>
                                    <td width="15%"><div align="right"><strong>物流单号：</strong></div></td>
                                    <td width="15%"><?php echo $wuliu['code'];?></td>
                                    <td width="15%"><div align="right"><strong>物流进度：</strong></div></td>
                                    <td ><?php echo $wuliu['content'];?></td>
                                  </tr>
                                </tbody></table>
                                <hr class="clearfix">
                                <table class="table table-hover table-bordered" width="100%" cellpadding="3" cellspacing="1">
                                  <tbody>
                                    <tr>
                                        <th colspan="7" scope="col" class="bg-grey text-left">
                                            <i class="icon icon-list"></i>
                                            菜品信息
                                            
                                        </th>
                                    </tr>
                                  <tr>
                                    <td scope="col"><div align="center"><strong>序号</strong></div></td>
                                    <td scope="col"><div align="center"><strong>菜品名称</strong></div></td>
                                    <td scope="col"><div align="center"><strong>规格</strong></div></td>
                                    <td scope="col"><div align="center"><strong>价格</strong></div></td>
                                    <td scope="col"><div align="center"><strong>数量</strong></div></td>
                                    <td scope="col"><div align="center"><strong>供货商</strong></div></td>
                                    <td scope="col"><div align="center"><strong>小计</strong></div></td>
                                  </tr>
                                    <?php 
                                        $goodsList  = HResponse::getAttribute('goodsList');
                                        $sumGoods   = 0;
                                        foreach($goodsList as $key => $item) {
                                            $shop       = $shopMap[$item['shop_id']];
                                            $sku        = $skuMap[$item['group_id']];
                                            $total      = $item['number'] * $item['price']; 
                                            $sumGoods   += $total;
                                    ?>
                                    <tr>
                                        <td class="text-center"><?php echo $key + 1;?></td>
                                        <td class="text-center">
                                            <?php echo $item['name'] ;?>
                                        </td>
                                        <td class="text-center"><?php echo $sku['name']; ?></td>
                                        <td class="text-center"><?php echo $item['price']; ?></td>
                                        <td class="text-center"><?php echo $item['number']; ?></td>
                                        <td class="text-center"><?php echo $shop['name']; ?></td>
                                        <td><div align="right">￥<span class="item-price"><?php echo $item['number'] * $item['price'];?></span>元</div></td>
                                    </tr>
                                    <?php } ?>
                                  </tbody>
                                </table>
                                <?php 
                                    $zenpingList    = HResponse::getAttribute('zenpingList');
                                    if($zenpingList) { 
                                ?>
                                <hr class="clearfix">
                                <table class="table table-hover table-bordered" width="100%" cellpadding="3" cellspacing="1">
                                  <tbody><tr>
                                    <th colspan="7" class="bg-grey text-left">
                                        <i class="icon icon-tag"></i>
                                        赠品信息
                                    </th>
                                  </tr>
                                  <tr>
                                      <td scope="col" width="10%"><div align="center"><strong>序号</strong></div></td>
                                      <td scope="col" width="10%"><div align="center" ><strong>原价</strong></div></td>
                                      <td scope="col" width="25%"><div align="center"><strong>赠送内容</strong></div></td>
                                      <td scope="col" width="20%"><div align="center"><strong>图片</strong></div></td>
                                  </tr>
                                  <?php 
                                    foreach($zenpingList as $key => $item) { 
                                  ?>
                                  <tr>
                                      <td scope="col"><div align="center"><strong><?php echo $key + 1; ?></strong></div></td>
                                      <td scope="col"><div align="center" ><strong><?php echo $item['price'];?></strong></div></td>
                                      <td scope="col" ><div align="center"><strong><?php echo $item['content'];?></strong></div></td>
                                      <td scope="col"><div align="center">
                                              <?php if($item['image_path']) { ?>
                                              <a href="<?php echo HResponse::touri($item['image_path']);?>" class="lightbox" target="_blank">
                                                  <img src="<?php echo HResponse::touri($item['image_path']);?>" alt="" class="pic">
                                              </a>
                                              <?php } ?>
                                          </div></td>
                                  </tr>
                                  <?php } ?>
                                </tbody></table>
                                <?php } ?>
                                <?php
                                    $youhuijuan     = HResponse::getAttribute('youhuijuan');
                                    if($youhuijuan) {
                                ?>
                                <hr class="clearfix">
                                <table class="table table-hover table-bordered" width="100%" cellpadding="3" cellspacing="1">
                                    <tbody><tr>
                                        <th colspan="5" class="bg-grey text-left">
                                            <i class="icon icon-bookmark"></i>优惠信息
                                        </th>
                                    </tr>
                                    <tr>
                                        <td scope="col" width="10%"><div align="center"><strong>序号</strong></div></td>
                                        <td scope="col" width="25%"><div align="center"><strong>满</strong></div></td>
                                        <td scope="col" width="20%"><div align="center"><strong>减</strong></div></td>
                                    </tr>
                                    <?php if($youhuijuan) { ?>
                                    <tr>
                                        <td scope="col" width="10%"><div align="center"><strong>1</strong></div></td>
                                        <td scope="col" width="25%"><div align="center"><strong><?php echo $youhuijuan['min_money'];?></strong></div></td>
                                        <td scope="col" width="20%"><div align="center"><strong><?php echo $youhuijuan['sub_money'];?></strong></div></td>
                                    </tr>
                                    <?php } ?>
                                    </tbody></table>
                                  <?php } ?>
                                <hr class="clearfix">
                                <?php
                                    $redpackage     = HResponse::getAttribute('lingqian');
                                    $sumActivity    = floatval($youhuijuan['sub_money']);
                                    $sumRedpackage  = floatval($redpackage['money']);
                                    $sum            = $sumGoods + $record['yun_fee'];
                                ?>
                                <table class="table table-hover table-bordered" width="100%" cellpadding="3" cellspacing="1">
                                  <tbody><tr>
                                    <th class="bg-grey text-left">
                                        <i class="icon icon-yen"></i>
                                        费用信息
                                    </th>
                                  </tr>
                                  <tr>
                                    <td><div align="right">菜品总金额：<strong>￥<?php echo $sumGoods; ?>元</strong>
                                      + 配送费用：<strong>￥<?php echo $record['yun_fee']; ?>元</strong>
                                      = 订单总金额：<strong>￥<?php echo $sum;?>元</strong>
                                      </div></td>
                                  </tr>
                                  <tr>
                                    <td><div align="right">
                                      - 使用优惠： <strong>￥<?php echo $sumActivity;?>元</strong>
                                      - 使用红包： <strong>￥<?php echo $sumRedpackage;?>元</strong>
                                    </div></td>
                                  </tr><tr>
                                    <td class="text-right">
                                     = 应付款金额：<strong>￥<?php echo $record['amount'];?>元</strong>
                                     <?php 
                                      $sum   = $sum - $sumActivity - $sumRedpackage; 
                                      $sum   = 0 > $sum ? 0 : $sum;
                                     ?></td>
                                  </tr>
                                </tbody></table>
                            </div>                                    
                            <!-- PAGE CONTENT ENDS HERE -->

                            </div>
                         </div><!--/row-->
                      </div>
                    <!-- PAGE CONTENT ENDS HERE -->
                     </div><!--/row-->
                </div><!--/#page-content-->
			</div><!-- #main-content -->
		</div><!--/.fluid-container#main-container-->
        <?php $isNoMessage = true; require_once(HResponse::path() . '/common/footer.tpl'); ?>
        <script type="text/javascript">
            $(function() {
                setTimeout(function() {
                    window.print();
                    $('#btn-print').show();
                }, 1000);
                $('#btn-print').click(function() { 
                    $(this).hide();
                    setTimeout(function() {
                        window.print();
                        $('#btn-print').show();
                    }, 500);
                });
            });
        </script>
	</body>
</html>
