<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
    <style type="text/css">
      th.field-id, td.field-id { display: table-cell; }
      #search-form input,
      #search-form input.date-picker { width: 150px; }
      #search-form select { width: 180px; }
      .dataTables_wrapper .row-fluid:last-child { border-top: none; border-bottom: none; }
    </style>
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
          <?php require_once(HResponse::path() . '/order/cur-location.tpl'); ?>
              <div class="row-fluid">
                  <!-- PAGE CONTENT BEGINS HERE -->
                  <div id="table_report_wrapper" class="dataTables_wrapper" role="grid">
                      <?php require_once(HResponse::path() . '/order/list-tool-bar-order.tpl'); ?>
                      <?php require_once(HResponse::path() . '/order/data-grid.tpl'); ?>
                  <!-- PAGE CONTENT ENDS HERE -->
                 </div><!--/row-->
                 </div><!--/row-->
                </div><!--/#page-content-->
             </div><!-- #main-content -->
         </div><!--/.fluid-container#main-container-->
         <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
		<script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>/jquery/plugins/jquery.tablesorter/jquery.tablesorter.min.js"></script> 
		<script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>js/list.js?v=<?php echo $v;?>"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>/js/order-list.js"></script>
	</body>
</html>
