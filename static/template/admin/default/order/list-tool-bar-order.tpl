                                <div class="row-fluid">
                                    <form id="search-form" action="<?php echo HResponse::url('' . $modelEnName . '/search'); ?>" method="get">
                                        <div class="span2">
                                            <label class="pl-5">每页显示:
                                                <select size="1" style="width: 100px;" class="auto-select input-small" name="perpage" id="perpage" aria-controls="table_report" data-cur="<?php echo HRequest::getParameter('perpage'); ?>">
                                                    <option value="10">10</option>
                                                    <option value="25">25</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                </select>
                                            </label>
                                            <label class="pl-5">订单类别:
                                                <select size="1" style="width: 100px;" class="auto-select input-small" name="type" id="type" data-cur="<?php echo HRequest::getParameter('type'); ?>">
                                                    <option value="">全部</option>
                                                    <?php foreach(OrderPopo::$typeMap as $item) { ?>
                                                    <option value="<?php echo $item['id'];?>"><?php echo $item['name'];?></option>
                                                    <?php } ?>
                                                </select>
                                            </label>
                                        </div>
                                        <div class="span10 txt-right f-right pr-15">
                                            <div class="row-fluid">
                                                <div class="span11">
                                                    <div class="where-box">
                                                        <label>ID:
                                                            <input type="text" class="input-large search-query" name="keywords" id="keywords" placeholder="<?php echo !HRequest::getParameter('keywords') ? '订单ID..' : HRequest::getParameter('keywords'); ?>">
                                                        </label>
                                                        <label>用户:
                                                            <input type="text" class="input-medium search-query" name="uname" id="uname" placeholder="用户名关键字..."  value="<?php echo HRequest::getParameter("uname"); ?>">
                                                        </label>
                                                        <label>桌号:
                                                            <input type="text" class="input-medium search-query" name="zhuohao" id="zhuohao" placeholder="输入桌号..."  value="<?php echo HRequest::getParameter('zhuohao'); ?>">
                                                        </label>
                                                        <label>支付:
                                                            <select name="payment" id="payment" class="auto-select input-medium search-query" data-cur="<?php echo HRequest::getParameter('payment'); ?>">
                                                                <option value="">全部</option>
                                                                <?php foreach(OrderPopo::$paymentMap as $item) { ?>
                                                                <option value="<?php echo $item['id'];?>"><?php echo $item['name']; ?></option>
                                                                <?php }?>
                                                            </select>
                                                        </label>
                                                    </div>
                                                    <div class="where-box">
                                                        <label class="lookup-field-box">商家: 
                                                            <input type="text" class="input-medium search-query" name="shopname" id="shopname" placeholder="输入商家名称..."  value="<?php echo HRequest::getParameter("shopname"); ?>">
                                                        </label>
                                                        <label>状态:
                                                            <select name="status" id="status" class="auto-select input-medium search-query" data-cur="<?php echo HRequest::getParameter('status'); ?>">
                                                                <option value="">全部</option>
                                                                <?php foreach(OrderPopo::$statusMap as $status) { ?>
                                                                <option value="<?php echo $status['id'];?>"><?php echo $status['name']; ?></option>
                                                                <?php }?>
                                                            </select>
                                                        </label>
                                                        <?php
                                                            $startTime  = HRequest::getParameter('start_time');
                                                            $endTime    = HRequest::getParameter('end_time');
                                                        ?>
                                                        <label>开始:</label>
                                                        <input class="date-picker search-query" name="start_time" type="text" value="<?php echo $startTime; ?>" data-date-format="yyyy-mm-dd hh:ii:ss" id="start_time">
                                                        <label>结束:</label>
                                                        <input  class="date-picker search-query" name="end_time" type="text" value="<?php echo $endTime; ?>" data-date-format="yyyy-mm-dd hh:ii:ss" id="end_time">
                                                    </div>
                                                </div>
                                                <div class="span1">
                                                    <button type="submit" style="height: 70px;" class="btn btn-purple btn-block btn-lager">搜索<i class="icon-search icon-on-right"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
