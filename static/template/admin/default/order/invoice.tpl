﻿<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
	</head>
	<body>
		<div class="container-fluid" id="main-container">
			<div class="clearfix">
                <?php 
                    $record         = HResponse::getAttribute('record');
                    $shopInfo       = HResponse::getAttribute('shopInfo');
                    $rate           = floatval($shopInfo['rate']);
                    $orderWuliu     = HResponse::getAttribute('orderWuliu');
                    $skuMap         = HResponse::getAttribute('skuMap');
                ?>   
                <div id="page-content" class="clearfix">
                    <div class="page-header position-relative">
                        <h1 class="text-center">
                            <a href="javascript:void(0);" style="display:none;" class="btn btn-mini  btn-info" id="btn-print">
                                <i class="icon icon-print"></i> 开始打印
                            </a>
                            <small class="fs-14 pull-right">打印时间：<?php echo date('Y-m-d H:i:s');?></small>
                            <span class="pull-left">
                            <?php
                                echo '客人菜单：订单编号 - ' .$record['name'];
                            ?>
                            </span>
                        </h1>
                        <div class="clearfix"></div>
                    </div><!--/page-header-->             
                    <div class="row-fluid">
                    <!-- PAGE CONTENT BEGINS HERE -->
                        <div class="span12 content-box">
                            <!-- PAGE CONTENT BEGINS HERE -->
                            <div class="row-fluid">
                                <table class="table table-hover table-bordered" width="100%" cellpadding="3" cellspacing="1">
                                    <tbody>
                                    <tr>
                                        <th colspan="4">
                                            <i class="icon icon-user"></i> 收货人信息
                                            <?php $address  = json_decode($record['address'], true); ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td class="text-center">收货人：</td>
                                        <td class="text-center"><?php echo $address['name'];?></td>
                                        <td class="text-center">手机：</td>
                                        <td class="text-center"><?php echo $address['phone'];?></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">地址：</td>
                                        <td class="text-center"><?php echo $address['detail'];?></td>
                                        <td class="text-center">邮编：</td>
                                        <td class="text-center"><?php echo $address['code'];?></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">物流方式：</td>
                                        <td class="text-center"><?php echo $orderWuliu['name']; ?></td>
                                        <td colspan="2" class="text-center"></td>
                                    </tr>
                                    </tbody>
                                </table>

                                <table class="table table-hover table-bordered" width="100%" cellpadding="3" cellspacing="1">
                                  <tbody>
                                    <tr>
                                        <th colspan="6" scope="col" class="bg-grey text-left">
                                            <i class="icon icon-list"></i>
                                            菜单信息
                                            
                                        </th>
                                    </tr>
                                  <tr>
                                    <td scope="col"><div align="center"><strong>序号</strong></div></td>
                                    <td scope="col"><div align="center"><strong>名称</strong></div></td>
                                    <td scope="col"><div align="center"><strong>规格</strong></div></td>
                                    <td scope="col"><div align="center"><strong>价格</strong></div></td>
                                    <td scope="col"><div align="center"><strong>数量</strong></div></td>
                                    <td scope="col"><div align="center"><strong>小计</strong></div></td>
                                  </tr>
                                    <?php 
                                        $goodsList  = HResponse::getAttribute('goodsList');
                                        $sumGoods   = 0;
                                        foreach($goodsList as $key => $item) {
                                            $sku        = $skuMap[$item['group_id']];
                                            $total      = $item['number'] * $item['price']; 
                                            $sumGoods   += $total;
                                    ?>
                                    <tr>
                                        <td class="text-center"><?php echo $key + 1;?></td>
                                        <td class="text-center">
                                            <?php echo $item['name'] ;?>
                                        </td>
                                        <td class="text-center"><?php echo $sku['name']; ?></td>
                                        <td class="text-center"><?php echo $item['price']; ?></td>
                                        <td class="text-center"><?php echo $item['number']; ?></td>
                                        <td><div align="right">￥<span class="item-price"><?php echo $item['number'] * $item['price'];?></span>元</div></td>
                                    </tr>
                                    <?php } ?>
                                  </tbody>
                                </table>
                                <?php 
                                    $zenpingList    = HResponse::getAttribute('zenpingList');
                                    if($zenpingList) { 
                                ?>
                                <table class="table table-hover table-bordered" width="100%" cellpadding="3" cellspacing="1">
                                  <tbody><tr>
                                    <th colspan="3" class="bg-grey text-left">
                                        <i class="icon icon-tag"></i>
                                        赠品信息
                                    </th>
                                  </tr>
                                  <tr>
                                    <td scope="col" width="10%"><div align="center"><strong>序号</strong></div></td>
                                    <td scope="col" width="15%"><div align="center"><strong>内容</strong></div></td>
                                    <td scope="col" width="10%"><div align="center" ><strong>原价</strong></div></td>
                                  </tr>
                                  <?php 
                                    foreach($zenpingList as $key => $item) { 
                                  ?>
                                  <tr>
                                    <td scope="col" width="10%"><div align="center"><strong><?php echo $key + 1; ?></strong></div></td>
                                    <td scope="col" width="15%"><div align="center"><strong><?php echo $item['content'];?></strong></div></td>
                                    <td scope="col" width="10%"><div align="center" ><strong><?php echo $item['price'];?></strong></div></td>
                                  </tr>
                                  <?php } ?>
                                </tbody></table>
                                <?php } ?>
                                <?php
                                    $rate           = HResponse::getAttribute('rate');
                                ?>
                                <table class="table table-hover table-bordered" width="100%" cellpadding="3" cellspacing="1">
                                  <tbody><tr>
                                    <th class="bg-grey text-left">
                                        <i class="icon icon-yen"></i>
                                        费用信息
                                    </th>
                                  </tr>
                                  <tr>
                                    <td><div align="right">菜品总金额：<strong>￥<?php echo $sumGoods; ?>元</strong>
                                      + 配送费用：<strong>￥<?php echo $record['yun_fee']; ?>元</strong>
                                      = 订单总金额：<strong>￥<?php echo $sumGoods + $sumGoods * $rate + $record['yun_fee']; ?>元</strong>
                                      </div></td>
                                  </tr>
                                </tbody></table>
                            </div>                                    
                            <!-- PAGE CONTENT ENDS HERE -->

                            </div>
                         </div><!--/row-->
                      </div>
                    <!-- PAGE CONTENT ENDS HERE -->
                     </div><!--/row-->
                </div><!--/#page-content-->
			</div><!-- #main-content -->
		</div><!--/.fluid-container#main-container-->
        <script type="text/javascript">
            $(function() {
                setTimeout(function() {
                    window.print();
                    $('#btn-print').show();
                }, 1000);
                $('#btn-print').click(function() { 
                    $(this).hide();
                    setTimeout(function() {
                        window.print();
                        $('#btn-print').show();
                    }, 500);
                });
            });
        </script>
	</body>
</html>
