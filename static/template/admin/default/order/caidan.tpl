﻿<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
    </head>
    <body style="font-size:18px; font-family: '黑体';">
        <div class="position-relative">
            <h1 class="text-center">
                <a href="javascript:void(0);" style="display:none;" class="btn btn-mini  btn-info" id="btn-print">
                    <i class="icon icon-print"></i> 开始打印
                </a>
            </h1>
            <div class="clearfix"></div>
        </div>
        <?php
            $record     = HResponse::getAttribute('record');
            $kefu       = HResponse::getAttribute('kefu');
            $goodsList  = HResponse::getAttribute('goodsList');
        ?>
        <div style=" margin: 0 auto; text-align:center; padding: 20px 0;">
            <div style="line-height:34px;">
                <p style="font-size:24px; font-weight:bold; letter-spacing:2px;"><?php echo $siteCfg['name']; ?></p>
                <p>电话：<?php echo $kefu['content']; ?></p>
                <p style="font-size:20px; border-bottom:1px solid #000; padding-bottom:10px;">客人存根</p>
            </div>
            <div style="line-height:30px;width:260px; margin:0 auto; font-size:16px;">
                <p style="margin-bottom:0; width:260px;">
                    <span style="text-align:left; display:inline-block; width:46%;">台号：<?php echo $record['code']; ?></span>
                    <span style="text-align:left; display:inline-block; width:48%;">单号：<?php echo $record['id']; ?></span>
                </p>
                <p style="margin-bottom:0; width:260px;">
                    <span style="text-align:left; display:inline-block; width:46%;">人数：<?php echo $record['total_number']; ?></span>
                    <span style="text-align:left; display:inline-block; width:48%;">印单员工：<?php echo HSession::getAttribute('id', 'user'); ?></span>
                </p>
                <p style="margin-bottom:0; width:260px;">
                    <span style="text-align:left; display:inline-block; width:46%;">时间：<?php echo date('H:i:s', time()); ?></span>
                    <span style="text-align:left; display:inline-block; width:48%;">开台员工：<?php echo HSession::getAttribute('id', 'user'); ?></span>
                </p>
            </div>
            <div style="margin-top:10px; border-bottom: 1px dashed #000; width:230px; margin:0 auto;">
                <p style="border-bottom: 1px dashed #000; padding-bottom:5px; width:235px;">
                    <span style="text-align:left; display:inline-block; width:50%;">菜品名称</span>
                    <span style="text-align:center; display:inline-block; width:20%;">数量</span>
                    <span style="text-align:center; display:inline-block; width:20%;">小计</span>
                </p>
                <?php
                    $total  = 0;
                    foreach($goodsList as $item){
                    $price  = $item['number'] * $item['price'];
                    $total += $price;
                ?>
                <p style="margin-bottom:5px; width:235px;">
                    <span style="text-align:left; display:inline-block; width:50%; font-size:18px;"><?php echo HString::cutString($item['name'], 14); ?></span>
                    <span style="text-align:center; display:inline-block; width:20%;"><?php echo $item['number']; ?></span>
                    <span style="text-align:center; display:inline-block; width:20%;"><?php echo $price; ?></span>
                </p>
                <?php } ?>
            </div>
            <div style="margin-top:20px; width:240px; margin:0 auto;">
                <div style="float: left;">
                    <p style="margin-top:40px;">消费总计</p>
                </div>
                <div style="float: right; text-align: left; margin-right:11px; margin-top:10px;">
                    <p>
                        <span>消费金额：</span>
                        <span><?php echo $total; ?></span>
                    </p>
                    <p>
                        <span>折扣金额：</span>
                        <span>0</span>
                    </p>
                    <p>
                        <span>应收现金：</span>
                        <span style="font-size:18px;"><?php echo $record['amount']; ?></span>
                    </p>
                </div>
            </div>
            <div style="clear:both;"></div>
             <div style="width:200px; height:auto; margin: 0 auto; margin-top:30px;">
                <img src="<?php echo HResponse::uri() ?>images/code.jpg" alt="">
            </div>
            <div style="margin-top: 10px; font-size: 18px; line-height:25px;">
                <p style="margin-bottom:0;">多谢惠顾</p>
                <p style="margin-bottom:0;">欢迎再次光临</p>
            </div>
        </div>
        <script type="text/javascript">
            $(function() {
                setTimeout(function() {
                    window.print();
                    $('#btn-print').show();
                }, 1000);
                $('#btn-print').click(function() { 
                    $(this).hide();
                    setTimeout(function() {
                        window.print();
                        $('#btn-print').show();
                    }, 500);
                });
            });
        </script>
    </body>
</html>
