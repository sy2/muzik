﻿<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
        <link rel="stylesheet" href="<?php echo HResponse::uri();?>css/colorpicker.css">
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <?php 
                    $copyRecord     = HResponse::getAttribute('copyRecord'); 
                    $record         = HResponse::getAttribute('record'); 
                ?>   
                <?php require_once(HResponse::path() . '/common/cur-location.tpl'); ?>
                    <div class="row-fluid">
                    <!-- PAGE CONTENT BEGINS HERE -->
                        <form action="<?php echo HResponse::url($modelEnName . '/' . HResponse::getAttribute('nextAction') ); ?>" method="post" enctype="multipart/form-data" id="info-form">
                            <div class="row-fluid">

                                <div class="span9 content-box">
                                <!-- PAGE CONTENT BEGINS HERE -->

                                    <?php $field = 'id'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                    <?php $record = !$record ? $copyRecord : $record; ?>
                                    <?php $field = 'name'; require(HResponse::path() . '/fields/text.tpl'); ?>
<?php $field = 'parent_id'; require(HResponse::path() . '/fields/select.tpl'); ?>
<div id="tpl-part-box" class="tpl-part-box">
    <?php if(!$record['content']) { ?>
    <p class="text-center">请选择模板编号，此处会自动生成。</p>
    <?php 
        } else { 
            $content= HString::decodeHtml($record['content']);
            $data   = json_decode($content, true);
            foreach($data as $item) {
    ?>
            <div class="control-group">
                <div class="row-fluid">
                    <div class="span4">
                        <label class="control-label" for="<?php echo $item['key'];?>-val">
                            <?php echo $item['name']; ?>
                        </label>
                    </div>
                    <div class="span8">
                        <div class="title-color-box pull-right"  title="设置标题文字颜色">
                            <select class="title-color auto-select" class="hide" name="colors[]" data-cur="<?php echo $item['color']; ?>">
                                <option value="#333" selected="">#333</option>
                                <option value="#ac725e">#ac725e</option>
                                <option value="#d06b64">#d06b64</option>
                                <option value="#f83a22">#f83a22</option>
                                <option value="#fa573c">#fa573c</option>
                                <option value="#ff7537">#ff7537</option>
                                <option value="#ffad46">#ffad46</option>
                                <option value="#42d692">#42d692</option>
                                <option value="#16a765">#16a765</option>
                                <option value="#7bd148">#7bd148</option>
                                <option value="#b3dc6c">#b3dc6c</option>
                                <option value="#fbe983">#fbe983</option>
                                <option value="#fad165">#fad165</option>
                                <option value="#92e1c0">#92e1c0</option>
                                <option value="#9fe1e7">#9fe1e7</option>
                                <option value="#9fc6e7">#9fc6e7</option>
                                <option value="#4986e7">#4986e7</option>
                                <option value="#9a9cff">#9a9cff</option>
                                <option value="#b99aff">#b99aff</option>
                                <option value="#c2c2c2">#c2c2c2</option>
                                <option value="#cabdbf">#cabdbf</option>
                                <option value="#cca6ac">#cca6ac</option>
                                <option value="#f691b2">#f691b2</option>
                                <option value="#cd74e6">#cd74e6</option>
                                <option value="#a47ae2">#a47ae2</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="controls">
                  <input type="text" name="values[]" value="<?php echo $item['value']; ?>" id="<?php echo $item['key']; ?>-val" placeholder="请输入..." class="form-control span12">
                  <input type="hidden" name="keys[]" value="<?php echo $item['key']; ?>" id="<?php echo $item['key']; ?>"/>
                  <input type="hidden" name="names[]" value="<?php echo $item['key']; ?>" id="<?php echo $item['name']; ?>"/>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
</div>
                                <div class="control-group">
                                     <small class="help-info">
                                         自动变量标签：{user_name}（自动填写为接收人的用户昵称）等。
                                     </small>
                                    <div class="clearfix"></div>
                                </div>
<?php $field = 'content'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
<?php $field = 'url'; require(HResponse::path() . '/fields/text.tpl'); ?>
<?php $field = 'no_send'; require(HResponse::path() . '/fields/textarea.tpl'); ?>

                                    
                                    
                                <!-- PAGE CONTENT ENDS HERE -->

                                </div>
                                <div class="span3">
                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4>发布</h4>
                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <?php $field = 'status'; require(HResponse::path() . '/fields/select.tpl'); ?>
<?php $field = 'total_send'; require(HResponse::path() . '/fields/text-readonly.tpl'); ?>
<?php $field = 'create_time'; require(HResponse::path() . '/fields/datetime.tpl'); ?>

                                                <?php require_once(HResponse::path() . '/common/info-buttons.tpl'); 
 ?>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <?php 
                                        if($modelCfg && '2' == $modelCfg['has_multi_lang']) { 
                                            require_once(HResponse::path() . '/common/lang-widget.tpl'); 
                                            echo '<hr/>';
                                        }
                                    ?>  

                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4>维护</h4>
                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <?php require_once(HResponse::path() . '/fields/author.tpl'); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                             </div><!--/row-->

                          </div>
                         </form>
                    <!-- PAGE CONTENT ENDS HERE -->
                     </div><!--/row-->
                </div><!--/#page-content-->
			</div><!-- #main-content -->
		</div><!--/.fluid-container#main-container-->
        <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
        <script type="text/template" id="part-tpl">
            <div class="control-group">
                <div class="row-fluid">
                    <div class="span4">
                        <label class="control-label" for="{key}-val">
                            {label}
                        </label>
                    </div>
                    <div class="span8">
                        <div class="title-color-box pull-right"  title="设置标题文字颜色">
                            <select class="title-color" class="hide" name="colors[]">
                                <option value="#333" selected="">#333</option>
                                <option value="#ac725e">#ac725e</option>
                                <option value="#d06b64">#d06b64</option>
                                <option value="#f83a22">#f83a22</option>
                                <option value="#fa573c">#fa573c</option>
                                <option value="#ff7537">#ff7537</option>
                                <option value="#ffad46">#ffad46</option>
                                <option value="#42d692">#42d692</option>
                                <option value="#16a765">#16a765</option>
                                <option value="#7bd148">#7bd148</option>
                                <option value="#b3dc6c">#b3dc6c</option>
                                <option value="#fbe983">#fbe983</option>
                                <option value="#fad165">#fad165</option>
                                <option value="#92e1c0">#92e1c0</option>
                                <option value="#9fe1e7">#9fe1e7</option>
                                <option value="#9fc6e7">#9fc6e7</option>
                                <option value="#4986e7">#4986e7</option>
                                <option value="#9a9cff">#9a9cff</option>
                                <option value="#b99aff">#b99aff</option>
                                <option value="#c2c2c2">#c2c2c2</option>
                                <option value="#cabdbf">#cabdbf</option>
                                <option value="#cca6ac">#cca6ac</option>
                                <option value="#f691b2">#f691b2</option>
                                <option value="#cd74e6">#cd74e6</option>
                                <option value="#a47ae2">#a47ae2</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="controls">
                  <input type="text" name="values[]" value="" id="{key}-val" placeholder="请输入..." class="form-control span12">
                  <input type="hidden" name="keys[]" value="{key}" id="{key}"/>
                  <input type="hidden" name="names[]" value="{label}" id="{key}-name"/>
                </div>
            </div>
        </script>
        <script type="text/javascript"> var fromId = '<?php echo HRequest::getParameter('fid');?>';</script>
        <script type="text/javascript" src="<?php echo HResponse::uri(); ?>js/bootstrap-colorpicker.min.js"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>js/info.js"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>js/wxtplsend-info.js"></script>
	</body>
</html>
