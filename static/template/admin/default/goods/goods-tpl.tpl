<?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
<script type="javascript/template" id="sku-item-tr-tpl">
<tr data-id="{id}" class="color-box-{id}">
    <td class="text-center">
        <strong>{name}</strong>
        <input type="hidden" name="color_id[]" class="sku-id" id="sku-{id}" value="{colorid}"/>
    </td>
    <td class="text-center">
        <input type="text" name="brand[]" class="sku-id" id="sku-brand-{id}" value=""/>
    </td>
    <td class="text-center">
        <div class="old-file-box">
            <a href="###" class="lightbox" id="sku-img-{id}-lightbox" target="_blank"></a>
        </div>
        <button type="button"
            data-field="image_path"
            data-type="image"
            data-target="sku-img-{id}"
			id="sku-img-{id}"
            class="btn btn-info btn-mini span12 btn-file btn-sku-img"
        ><i class="icon-file"></i></button>
        <input type="hidden" name="sku_image_path[]" id="sku-img-{id}input" value="" />
    </td>
    <td class="text-center">
        <input type="text" name="sort[]" value="" class="sort" id="sort-{id}" />
    </td>
    <td><span class="btn btn-danger delete-color-image" data-id="{id}">x</span></td>
</tr>
</script>