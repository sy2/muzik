<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <?php 
                    $copyRecord     = HResponse::getAttribute('copyRecord'); 
                    $record         = HResponse::getAttribute('record'); 
                ?>
                <?php require_once(HResponse::path() . '/common/cur-location.tpl'); ?>
                    <div class="row-fluid">
                    <!-- PAGE CONTENT BEGINS HERE -->
                        <form action="<?php echo HResponse::url($modelEnName . '/' . HResponse::getAttribute('nextAction') ); ?>" method="post" enctype="multipart/form-data" id="info-form" data-ajax="2">
                            <div class="row-fluid">
                                <div class="span9 content-box">
                                <!-- PAGE CONTENT BEGINS HERE -->
                                    <div class="tabbable">
                                        <ul class="nav nav-tabs" id="myTab">
                                            <li class="active">
                                                <a id="tab-step-baseinfo" data-toggle="tab" href="#tab-baseinfo">
                                                    <i class="green icon-info-sign bigger-110"></i>
                                                    基本信息
                                                </a>
                                            </li>
                                            <li>
                                                <a id="tab-step-attr" data-toggle="tab" href="#tab-attr">
                                                    颜色配图
                                                </a>
                                            </li>
                                            <li>
                                                <a id="tab-step-album" data-toggle="tab" href="#tab-album">
                                                    封面图
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="tab-baseinfo" class="tab-pane in active">
                                    <?php $field = 'id'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                    <?php $record = !$record ? $copyRecord : $record; ?>
                                    <?php $field = 'name'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                    <div class="row-fluid">
                                        <div class="span3">
                                    <?php $field = 'kuan'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                        </div>
                                        <div class="span3">
                                    <?php $field = 'zhai'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                        </div>
                                        <div class="span3">
                                    <?php $field = 'height'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                        </div>
                                        <div class="span3">
                                    <?php $field = 'length'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span4">
                                            <?php $field = 'shape'; require(HResponse::path() . '/fields/select.tpl'); ?>
                                        </div>
                                        <div class="span4">
                                            <?php $field = 'rim'; require(HResponse::path() . '/fields/select.tpl'); ?>
                                        </div>
                                        <div class="span4">
                                            <?php $field = 'material'; require(HResponse::path() . '/fields/select.tpl'); ?>
                                        </div>
                                    </div>
                                    <?php $field = 'first_word'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                    <?php $field = 'parent_id'; require(HResponse::path() . '/fields/tree.tpl'); ?>
                                    <?php $field = 'parent_path'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                    <?php $field = 'tui_rate'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                    <?php $field = 'content'; require(HResponse::path() . '/fields/editor.tpl'); ?>
                                    <?php $field = 'ext_1'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                    <div class="control-group">
                                        <div class="span6">
                                        </div>
                                        <div class="span6 txt-right">
                                            <a data-toggle="tab-step-attr" class="btn btn-success btn-tab-step" href="javascript:void(0);">下一步</a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    </div>
                                            <div id="tab-album" class="tab-pane">
<?php $field = 'image_path'; require(HResponse::path() . '/fields/image.tpl'); ?>
<?php $field = 'hash'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
<hr class="clearfix mt-5"  />
                                                <div class="control-group">
                                                    <div class="span6">                                                        
                                                        <a data-toggle="tab-step-attr" class="btn btn-warning btn-tab-step" href="javascript:void(0);">上一步</a>
                                                    </div>
                                                    <div class="span6 txt-right hide">
                                                        <a data-toggle="tab-step-other" class="btn btn-success btn-tab-step" href="javascript:void(0);">下一步</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div id="tab-attr" class="tab-pane">
                                                <div class="row-fluid">
                                                    <div class="span4">
                                                        <div class="controls">
                                                            <label>颜色：</label>
                                                            <select class="auto-select span12" id="color-box">
                                                                <?php foreach (HResponse::getAttribute('color_id_list') as $item){ ?>
                                                                <option value="<?php echo $item['id']; ?>"><?php echo $item['name']; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                            <script type="text/javascript"> selectList.push("#color-box"); </script>
                                                        </div>
                                                        <a href="javascript:void(0);" class="btn btn-sm btn-info fr" id="btn-generate-sku">
                                                            <i class="icon icon-wrench"></i>
                                                            添加颜色图组
                                                        </a>
                                                    </div>
                                                    <div class="span8">
                                                        <table class="table-sku-list table table-striped table-bordered table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th width="20%">颜色值</th>
                                                                <th width="20%">品牌</th>
                                                                <th width="40%">图片</th>
                                                                <th width="10%">排序</th>
                                                                <th width="10%">操作</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="sku-list">
                                                            
                                                         
                                                            <?php foreach($record['color_id'] as $item) { ?>
                            
                                                            <tr data-id="<?php echo $item['id'];?>" class="color-box-<?php echo $item['id'];?>">
                                                                <td class="text-center">
                                                                    <strong><?php echo $item['name']; ?></strong>
                                                                    <input type="hidden" name="color_id[]" value="<?php echo $item['id']; ?>" class="sku-id" id="sku-name-<?php echo $item['id'];?>" />
                                                                </td>
                                                                <td class="text-center">
                                                                    <input type="text" name="brand[]" value="<?php echo $item['brand']; ?>" class="sku-id" id="sku-brand-<?php echo $item['id'];?>" />
                                                                </td>
                                                                <td class="text-center">
                                                                    <div class="old-file-box">
                                                                        <a  target="_blank" href="<?php echo HResponse::touri($item['image_path']); ?>" class="lightbox" id="sku-img-<?php echo $item['id'];?>-lightbox">
                                                                        <?php if($item['image_path']) { ?>
                                                                        <img src="<?php echo HResponse::touri($item['image_path']); ?>" />
                                                                        <?php }?>
                                                                        </a>
                                                                    </div>
                                                                    <button type="button"
                                                                            data-field="image_path"
                                                                            data-type="image"
                                                                            data-target="sku-img-<?php echo $item['id'];?>"
																			id="sku-img-<?php echo $item['id'];?>"
                                                                            class="btn btn-info btn-mini span12 btn-file btn-sku-img"
                                                                    ><i class="icon-file"></i></button>
                                                                    <input type="hidden" name="sku_image_path[]"
                                                                           id="sku-img-<?php echo $item['id'];?>"
                                                                           value="<?php echo $item['image_path'];?>" />
                                                                    <script type="text/javascript">
                                                                        <?php $timestamp    = time() . rand(1000, 9999); ?>
                                                                        formData['sku-img-<?php echo $item['id'];?>']   = {
                                                                            'timestamp' : '<?php echo $timestamp; ?>',
                                                                            'token'     : '<?php echo md5($timestamp);?>',
                                                                            'model'     : 'skugoods',
                                                                            'field'     : 'image_path',
                                                                            'nolinked'  : 1
                                                                        };
                                                                    </script>
                                                                </td>
                                                                <td class="text-center">
                                                                    <input type="text" name="sort[]" value="<?php echo $item['sort']; ?>" class="sort" id="sort-<?php echo $item['id'];?>" />
                                                                </td>
                                                                <td><span class="btn btn-danger delete-color-image" data-id="<?php echo $item['id']; ?>">x</span></td>
                                                            </tr>
                                                            <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <div class="span6">
                                                        <a data-toggle="tab-step-baseinfo" class="btn btn-warning btn-tab-step" href="javascript:void(0);">上一步</a>
                                                    </div>
                                                    <div class="span6 txt-right">
                                                        <a data-toggle="tab-step-album" class="btn btn-success btn-tab-step" href="javascript:void(0);">下一步</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div id="tab-other" class="tab-pane">
<?php $field = 'score'; require(HResponse::path() . '/fields/text.tpl'); ?>
<?php $field = 'total_comments'; require(HResponse::path() . '/fields/text.tpl'); ?>
<?php $field = 'total_great'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
<?php $field = 'total_save'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                <div class="control-group">
                                                    <div class="span6">                                                        
                                                        <a data-toggle="tab-step-album" class="btn btn-warning btn-tab-step" href="javascript:void(0);">上一步</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <!-- PAGE CONTENT ENDS HERE -->
                                </div>
                                <div class="span3">
                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4>发布</h4>
                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <?php $field = 'is_show'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                <?php $field = 'is_new'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                <?php $field = 'status'; require(HResponse::path() . '/fields/checkbox.tpl'); ?>
                                                <?php $field = 'attr_data'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                <?php $field = 'total_great'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                <?php $field = 'sort_num'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                                <?php $field = 'total_orders'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                <?php $field = 'total_number'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                <?php require_once(HResponse::path() . '/common/info-buttons.tpl'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4>维护</h4>
                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <?php $field = 'total_visits'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                <?php $field = 'create_time'; require(HResponse::path() . '/fields/datetime.tpl'); ?>
<?php require_once(HResponse::path() . '/fields/author.tpl'); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                             </div><!--/row-->

                          </div>
                         </form>
                    <!-- PAGE CONTENT ENDS HERE -->
                     </div><!--/row-->
                </div><!--/#page-content-->
			</div><!-- #main-content -->
		</div><!--/.fluid-container#main-container-->
        <?php require_once(HResponse::path() . '/goods/goods-tpl.tpl'); ?>
        <script type="text/javascript"> var fromId = '<?php echo HRequest::getParameter('fid');?>';</script>
        <script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>/jquery/plugins/jquery.nestable.min.js"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>js/info.js?v=<?php echo $v;?>"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>js/goods-info.js?v=<?php echo $v;?>"></script>
    </body>
</html>
