                                <div class="row-fluid">
                                    <form id="search-form" action="<?php echo HResponse::url('' . $modelEnName . '/search'); ?>" method="get">
                                        <div class="span4">
                                            <div id="table_report_length" class="dataTables_length ml-18">
                                                <?php if(HResponse::getAttribute('parent_id_list')) { ?>
                                                <label>餐桌号
                                                    <select name="type" id="category" class="input-medium" data-cur="<?php echo HRequest::getParameter('type'); ?>">
                                                        <option value="">全部</option>
                                                        <?php 
                                                            HClass::import('hongjuzi.utils.HTree');
                                                            $hTree  = new HTree(
                                                                HResponse::getAttribute('parent_id_list'),
                                                                'id',
                                                                'parent_id',
                                                                'name',
                                                                'id',
                                                                '<option value="{id}">' .
                                                                '{name}' .
                                                                '</option>'
                                                            );
                                                            echo $hTree->getTree();
                                                        ?>
                                                    </select>
                                                </label>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="span8 txt-right">
                                            <div class="pr-15">
                                                <label>搜索餐桌号: 
                                                    <input type="text" class="input-medium search-query" name="keywords" id="keywords" data-def="<?php echo !HRequest::getParameter('keywords') ? '顾客餐桌号码...' : HRequest::getParameter('keywords'); ?>">
                                                    <button type="submit" class="btn btn-purple btn-small">搜索<i class="icon-search icon-on-right"></i></button>
                                                </label>
                                            </div>
                                        </div>
                                    </form>
                                </div>
