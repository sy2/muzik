                    <div id="breadcrumbs">
                        <?php 
                            $catInfo    = HSession::getAttribute('catInfo');
                            $catUrl     = $catInfo ? HResponse::url('category/type', 'id=' . $catInfo['id']) : HResponse::url('index', 'id=category-tab');
                        ?>
						<ul class="breadcrumb">
							<li>
                                <i class="icon-dashboard"></i> <a href="<?php echo HResponse::url('', '', 'admin'); ?>">后台桌面</a>
                                <span class="divider"><i class="icon-angle-right"></i></span>
                            </li>
                            <li>
                                <a href="<?php echo HResponse::url('apps'); ?>">更多</a>
                                <span class="divider"><i class="icon-angle-right"></i></span>
                            </li>
							<li><a href="<?php echo $catUrl; ?>"><?php echo $modelZhName; ?></a> <span class="divider"><i class="icon-angle-right"></i></span></li>
							<li class="active"><?php echo $modelZhName; ?><?php HTranslate::_('内容'); ?></li>
						</ul><!--.breadcrumb-->
						<div id="nav-search">
                            <span id="time-info">正在加载时钟...</span>
						</div><!-- #nav-search -->
					</div><!-- #breadcrumbs -->
                    <div id="page-content" class="clearfix">
						<div class="page-header position-relative">
							<h1>
                                <a class="btn btn-mini btn-info pull-right mt-10" href="<?php echo HResponse::url('gonggao/addview', $catId); ?>">
                                    <i class="icon icon-plus"></i>
                                    添加新<?php echo $popo->modelZhName;?>
                                </a>
                                <?php
                                    if(!empty($record)) {
                                        HTranslate::_('编辑');
                                        echo ' - ';
                                        echo isset($record['name']) ? $record['name'] : 'ID：' . $record['id'];
                                    } else {
                                        $title  = HResponse::getAttribute('title');
                                        if(!$title) {
                                            HTranslate::_($modelZhName);
                                        } else {
                                            echo $title;
                                        }
                                        echo $copyRecord ? '(' . HTranslate::__('基于') . 'ID：' . $copyRecord['id'] . ')' : '';
                                    }
                                    $catId  = !HRequest::getParameter('cat') ? '' : 'cat=' . HRequest::getParameter('cat');
                                ?>
                            </h1>
						</div><!--/page-header-->                   
