<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
    </head>
    <body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
        <div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
            <div id="main-content" class="clearfix">
                <?php require_once(HResponse::path() . '/common/report-count-cur-location.tpl'); ?>
                    <div class="row-fluid">
                        <!-- PAGE CONTENT BEGINS HERE -->
                        <div id="table_report_wrapper" class="dataTables_wrapper" role="grid">
                            <div class="row-fluid">
                                <div class="span3">
                                    <div class="search-box">
                                    <span>快捷时间：</span>
                                        <a class="btn btn-info btn-mini" href="<?php echo HResponse::getAttribute('sevDayUrl'); ?>">近七天</a>
                                        <a class="btn btn-success btn-mini" href="<?php echo HResponse::getAttribute('oneMonthUrl'); ?>">近一月</a>
                                        <a class="btn btn-warning btn-mini" href="<?php echo HResponse::getAttribute('oneYearUrl'); ?>">近一年</a>
                                    </div>
                                </div>
                                <div class="span9 text-right">
                                    <form class="form-inline search-box">
                                        <fieldset>
                                            <label class="lookup-field-box">商家: 
                                                <input type="text" class="input-medium search-query" name="shopname" id="shopname" placeholder="输入商家名称..."  value="<?php echo HRequest::getParameter("shopname"); ?>">
                                            </label>
                                            <?php
                                                $startTime  = HRequest::getParameter('start_time');
                                                $endTime    = HRequest::getParameter('end_time');
                                                $startTime  = isset($startTime) ? $startTime : date('Y-m-d', strtotime('-1 month'));
                                                $endTime    = isset($endTime) ? $endTime : date('Y-m-d', time());
                                            ?>
                                            <label>开始日期</label>
                                            <input class="datetime-picker" name="start_time" type="text" value="<?php echo $startTime; ?>" data-date-format="yyyy-mm-dd" id="start_time">
                                            <label>结束日期</label>
                                            <input  class="datetime-picker" name="end_time" type="text" value="<?php echo $endTime; ?>" data-date-format="yyyy-mm-dd" id="end_time">
                                            <button type="submit" class="btn btn-purple btn-small">查询</button>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                            <form action="<?php echo HResponse::url('' . $modelEnName . '/quick'); ?>" method="post" id="list-form">
                                <table id="data-grid-box" class="table table-striped table-bordered table-hover" >
                                    <thead>
                                    <tr>
                                        <th>排行</th>
                                        <th>供货商名称</th>
                                        <th>销售量</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                            if(HVerify::isEmpty(HResponse::getAttribute('list'))) {
                                                echo '<tr><td colspan="5" class="center">暂无相关记录</td></tr>';
                                    }
                                    $key = 0;
                                    foreach(HResponse::getAttribute('list') as  $record) {
                                        echo $key % 2 == 0 ? '<tr class="odd"' . '" id="' . $record['id'] .'">' : '<tr ' . '" id="' . $record['id'] .'">';

                                    ?>
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?php echo $record['name']; ?></td>
                                    <td><?php echo $record['total']; ?></td>
                                    <td>
                                        <a class="" title="查看详细统计" href="<?php echo HResponse::url('reportcount/goodssale', 'shop_id=' . $record['id']); ?>">查看详细统计</a>
                                    </td>
                                    </tr>
                                    <?php
                                            $key ++;
                                        } 
                                     ?>
                                    </tbody>
                                </table>
                            </form>
                        <!-- PAGE CONTENT ENDS HERE -->
                         </div><!--/row-->
                        </div><!--/#page-content-->
                    </div>
                </div><!-- #main-content -->
            </div><!--/.fluid-container#main-container-->
         <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
        <script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>/jquery/plugins/jquery.tablesorter/jquery.tablesorter.min.js"></script>       
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>js/list.js?v=<?php echo $v;?>"></script>
        <script type="text/javascript">
            dateList.push("#start_time");
            dateList.push("#end_time");
        </script>
    </body>
</html>
