<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
        <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>jquery/plugins/jquery.tablesorter/themes/blue/style.css">
        <?php $shopInfo     = HResponse::getAttribute('shopInfo');?>
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <?php require_once(HResponse::path() . '/common/report-count-cur-location.tpl'); ?>
                    <div class="row-fluid">
                        <div id="table_report_wrapper" class="dataTables_wrapper" role="grid">
                            <div class="row-fluid">
                                <div class="span4">
                                    <div class="search-box  pl-15">
                                        总折前销售金额:  ￥<strong class="red fs-22"><?php echo HResponse::getAttribute('sum'); ?></strong>元
                                    </div>
                                </div>
                                <div class="span8">
                                    <form class="form-inline text-right  pr-15">
                                        <fieldset>
                                            <label class="lookup-field-box">商家: 
                                                <input type="text" class="input-medium search-query" name="shopname" id="shopname" placeholder="输入商家名称..."  value="<?php echo HRequest::getParameter("shopname"); ?>">
                                            </label>
                                            <label>查询日期（限62天以内）：</label>
                                            <?php
                                                $startTime  = HResponse::getAttribute('start_time');
                                                $endTime    = HResponse::getAttribute('end_time');
                                            ?>
                                            <label>开始日期</label>
                                            <input class="datetime-picker" name="start_time" type="text" value="<?php echo $startTime; ?>" data-date-format="yyyy-mm-dd" id="start_time">
                                            <label>结束日期</label>
                                            <input  class="datetime-picker" name="end_time" type="text" value="<?php echo $endTime; ?>" data-date-format="yyyy-mm-dd" id="end_time">
                                            <button type="submit" class="btn btn-purple btn-small">查询</button>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="tabbable mt-10">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#zhekou-total-box" data-toggle="tab">折扣统计图表</a></li>
                                <li><a href="#zhekou-list-box" data-toggle="tab">折扣统计明细表</a></li>
                                <li><a href="#goods-zhekou-box" data-toggle="tab">单品折扣报表</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="zhekou-total-box">
                                    <div class="liren-chart-box" id="order-total"  style="width: 100%;height:600px;"></div>
                                </div>
                                <div class="tab-pane" id="zhekou-list-box" >
                                    <h4 class="page-header">折扣统计报表</h4>
                                    <?php 
                                        $xAxis          = HResponse::getAttribute('xAxis');
                                        $oldSumList     = HResponse::getAttribute('oldSumList');
                                        $orderList      = HResponse::getAttribute('orderList');
                                        $zheSumList     = HResponse::getAttribute('zheSumList');
                                        $subSumList     = HResponse::getAttribute('subSumList');
                                        $zheKouList     = HResponse::getAttribute('zheKouList');
                                        $numberList     = HResponse::getAttribute('numberList');
                                        $zhuoHaoList    = HResponse::getAttribute('zhuoHaoList');
                                        for($key = count($xAxis) - 1; $key >= 0; $key --) {
                                            $zhuoHaoMap = $zhuoHaoList[$key];
                                    ?>
                                    <h4 class="header smaller lighter blue">
                                        <?php echo $xAxis[$key];?> 折扣统计明细
                                    </h4>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="center">
                                                    日期
                                                </th>
                                                <th>折扣前销售金额</th>
                                                <th>折扣后销售金额</th>
                                                <th>差值销售金额</th>
                                                <th>平均折扣</th>
                                                <th>
                                                    成交数量
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?php echo $xAxis[$key];?></td>
                                                <td><strong>￥<?php echo $oldSumList[$key]; ?></strong></td>
                                                <td><strong>￥<?php echo $zheSumList[$key]; ?></strong></td>
                                                <td><strong class="text-red">￥<?php echo $subSumList[$key];?></strong></td>
                                                <td><?php echo number_format($zheKouList[$key], 2);?></td>
                                                <td><?php echo $numberList[$key];?></td>
                                            </tr>
                                            <tr>
                                                <td colspan="6">
                                                    <table class="table table-striped table-bordered table-hover tablesorter">
                                                        <thead>
                                                            <tr>
                                                                <th class="center">
                                                                    订单号
                                                                </th>
                                                                <th class="center">
                                                                    桌号
                                                                </th>
                                                                <th>折扣前金额</th>
                                                                <th>折扣后金额</th>
                                                                <th>差值金额</th>
                                                                <th>实付金额</th>
                                                                <th>折扣</th>
                                                                <th>
                                                                    人数
                                                                </th>
                                                                <th class="center">
                                                                    支付方式
                                                                </th>
                                                                <th class="center">
                                                                    时间
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach($orderList[$key] as $item) { ?>
                                                            <tr>
                                                                <td><a target="_blank" href="<?php echo HResponse::url('order/editview', 'id=' . $item['id']);?>"><?php echo $item['name'];?></a></td>
                                                                <td><?php echo $zhuoHaoMap[$item['code']]['name'];?></td>
                                                                <td><strong>￥<?php echo $item['amount'] + $item['zhekou_amount']; ?></strong></td>
                                                                <td><strong>￥<?php echo $item['amount']; ?></strong></td>
                                                                <td><strong class="text-red">￥<?php echo $item['zhekou_amount'];?></strong></td>
                                                                <td>￥<?php echo $item['pay_money'];?></td>
                                                                <td><?php echo $item['zhekou'];?></td>
                                                                <td><?php echo $item['total_number'];?></td>
                                                                <td>
                                                                    <?php 
                                                                        foreach(explode(',', $item['payment']) as $pId) {
                                                                            echo OrderPopo::$paymentMap[$pId]['name']; 
                                                                        } 
                                                                    ?>
                                                                </td>
                                                                <td><?php echo $item['create_time'];?></td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <hr>
                                    <?php } ?>
                                    <table class="table ">
                                        <tbody>
                                            <tr>
                                                <td>合计：</td>
                                                <td><strong>￥<?php echo array_sum($oldSumList); ?></strong></td>
                                                <td><strong>￥<?php echo array_sum($zheSumList); ?></strong></td>
                                                <td><strong class="text-red">￥<?php echo array_sum($subSumList);?></strong></td>
                                                <td><?php echo array_sum($zheKouList) / count($zheKouList) * 10;?></td>
                                                <td><?php echo array_sum($numberList);?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="goods-zhekou-box" >
                                    <h4 class="page-header">单品折扣统计报表</h4>
                                    <table id="sample-table-2" class="table table-striped table-bordered table-hover tablesorter">
                                        <thead>
                                            <tr>
                                                <th class="center">
                                                    时间
                                                </th>
                                                <th>菜品名称</th>
                                                <th>折扣前</th>
                                                <th>折扣后</th>
                                                <th>差值</th>
                                                <th>平均折扣</th>
                                                <th>
                                                    成交数量
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php 
                                                foreach(HResponse::getAttribute('goodsList') as $key => $item) { 
                                            ?>
                                            <tr>
                                                <td><?php echo $key + 1;?></td>
                                                <td>
                                                    <a href="<?php echo HResponse::url('goods/editview', 'id=' . $item['goods_id']);?>">
                                                        <?php echo $item['name'];?>
                                                    </a>
                                                </td>
                                                <td><strong>￥<?php echo $item['old_sum']; ?></strong></td>
                                                <td><strong>￥<?php echo $item['zhe_sum']; ?></strong></td>
                                                <td><strong class="text-red">￥<?php echo $item['old_sum'] - $item['zhe_sum'];?></strong></td>
                                                <td><?php echo number_format($item['zhekou'], 2) * 10;?></td>
                                                <td><?php echo $item['number'];?></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- #main-content -->
            </div><!--/.fluid-container#main-container-->
         <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
		<script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>jquery/plugins/jquery.tablesorter/jquery.tablesorter.min.js"></script>       
        <script type="text/javascript">
            dateList.push("#start_time");
            dateList.push("#end_time");
            HJZCommon.bindBtnLookUpShop('#shopname');
            var orderChart      = echarts.init(document.getElementById('order-total'));
            var option = {
                title: {
                    text: '<?php echo !$shopInfo ? '所有' : $shopInfo['name'];?>商家订单走势图'
                },
                tooltip : {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        label: {
                            backgroundColor: '#6a7985'
                        }
                    }
                },
                toolbox: {
                    feature: {
                        saveAsImage: {}
                    }
                },
                legend: {
                    data:['折扣前金额', '折扣后金额', '差额', '平均折扣', '总销量']
                },
                xAxis: {
                    data: <?php echo json_encode(HResponse::getAttribute('xAxis'));?>
                },
                yAxis: {},
                series : [
                    {
                        name:'折扣前金额',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} 元'}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('oldSumList')); ?>
                    },
                    {
                        name:'折扣后金额',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} 元'}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('zheSumList')); ?>
                    },
                    {
                        name:'差额',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} 元'}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('subSumList')); ?>
                    },
                    {
                        name:'平均折扣',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} 折'}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('zheKouList')); ?>
                    },
                    {
                        name:'总销量',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} 件'}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('numberList')); ?>
                    }
                ]
            };
            orderChart.setOption(option);
            //sort table
            var oTable1 = $('#sample-table-2').tablesorter();
        </script>
    </body>
</html>
