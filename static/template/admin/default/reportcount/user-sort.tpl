<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <?php require_once(HResponse::path() . '/common/report-count-cur-location.tpl'); ?>
                    <div class="row-fluid">
                        <!-- PAGE CONTENT BEGINS HERE -->
                        <div id="table_report_wrapper" class="dataTables_wrapper" role="grid">
                            <div class="row-fluid text-right">
                                <form class="form-inline span12  pr-15">
                                    <div class="search-box">
                                        <fieldset>
                                            <label class="lookup-field-box">商家: 
                                                <input type="text" class="input-medium search-query" name="shopname" id="shopname" placeholder="输入商家名称..."  value="<?php echo HRequest::getParameter("shopname"); ?>">
                                            </label>
                                            <?php
                                                $startTime  = HResponse::getAttribute('start_time');
                                                $endTime    = HResponse::getAttribute('end_time');
                                            ?>
                                            <label>查询日期（限62天以内）：</label>
                                            <label>开始日期</label>
                                            <input class="datetime-picker" name="start_time" type="text" value="<?php echo $startTime; ?>" data-date-format="yyyy-mm-dd" id="start_time">
                                            <label>结束日期</label>
                                            <input  class="datetime-picker" name="end_time" type="text" value="<?php echo $endTime; ?>" data-date-format="yyyy-mm-dd" id="end_time">
                                            <button type="submit" class="btn btn-purple btn-small">查询</button>
                                        </fieldset>
                                    </div>
                                </form>
                            </div>
                            <form action="<?php echo HResponse::url('' . $modelEnName . '/quick'); ?>" method="post" id="list-form">
                                <table id="data-grid-box" class="table table-striped table-bordered table-hover" >
                                    <thead>
                                    <tr>
                                        <th>排行</th>
                                        <th>会员名</th>
                                        <th>订单数(单位:个)</th>
                                        <th>购物金额</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                            if(HVerify::isEmpty(HResponse::getAttribute('list'))) {
                                                echo '<tr><td colspan="4" class="center">暂无相关记录</td></tr>';
                                    }
                                    foreach(HResponse::getAttribute('list') as $key => $record) {
                                        echo $key % 2 == 0 ? '<tr class="odd"' . '" id="' . $record['id'] .'">' : '<tr ' . '" id="' . $record['id'] .'">';

                                    ?>
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?php echo $record['name']; ?></td>
                                    <td><?php echo $record['total_order']; ?></td>
                                    <td><?php echo number_format($record['total_money'], 2); ?></td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                                <div class="row-fluid">
                                    <div class="span3">
                                        <div class="dataTables_info" id="table_report_info">
                                            共 <?php echo HResponse::getAttribute('totalRows');?> 条
                                            当前: <?php echo HResponse::getAttribute('curPage') . '/' . HResponse::getAttribute('totalPages')?></strong>页
                                        </div>
                                    </div>
                                    <div class="span6">
                                        <div class="dataTables_paginate paging_bootstrap pagination">
                                            <ul><?php echo HResponse::getAttribute('pageHtml');?></ul>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        <!-- PAGE CONTENT ENDS HERE -->
                         </div><!--/row-->
                        </div><!--/#page-content-->
                    </div>
                </div><!-- #main-content -->
            </div><!--/.fluid-container#main-container-->
         <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
		<script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>/jquery/plugins/jquery.tablesorter/jquery.tablesorter.min.js"></script>       
		<script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>js/list.js?v=<?php echo $v;?>"></script>
        <script type="text/javascript">
            dateList.push("#start_time");
            dateList.push("#end_time");
        </script>
    </body>
</html>
