<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
        <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>jquery/plugins/jquery.tablesorter/themes/blue/style.css">
        <?php $shopInfo     = HResponse::getAttribute('shopInfo');?>
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <?php require_once(HResponse::path() . '/common/report-count-cur-location.tpl'); ?>
                    <div class="row-fluid">
                        <div id="table_report_wrapper" class="dataTables_wrapper" role="grid">
                            <div class="row-fluid">
                                <div class="span4">
                                    <div class="search-box  pl-15">
                                        总毛利:  ￥<strong class="red fs-22"><?php echo number_format(array_sum(HResponse::getAttribute('allLirenList')), 2); ?></strong>元
                                    </div>
                                </div>
                                <div class="span8">
                                    <form class="form-inline text-right  pr-15">
                                        <fieldset>
                                            <label class="lookup-field-box">商家: 
                                                <input type="text" class="input-medium search-query" name="shopname" id="shopname" placeholder="输入商家名称..."  value="<?php echo HRequest::getParameter("shopname"); ?>">
                                            </label>
                                            <label>查询日期（限62天以内）：</label>
                                            <?php
                                                $startTime  = HResponse::getAttribute('start_time');
                                                $endTime    = HResponse::getAttribute('end_time');
                                            ?>
                                            <label>开始日期</label>
                                            <input class="datetime-picker" name="start_time" type="text" value="<?php echo $startTime; ?>" data-date-format="yyyy-mm-dd" id="start_time">
                                            <label>结束日期</label>
                                            <input  class="datetime-picker" name="end_time" type="text" value="<?php echo $endTime; ?>" data-date-format="yyyy-mm-dd" id="end_time">
                                            <button type="submit" class="btn btn-purple btn-small">查询</button>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="tabbable mt-10">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#order-total-box" data-toggle="tab">利润增长趋势图</a></li>
                                <li><a href="#liren-list-box" data-toggle="tab">利润增长趋势统计数据表</a></li>
                                <li><a href="#goods-order-box" data-toggle="tab">单品利润报表</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="order-total-box">
                                    <div class="liren-chart-box" id="order-total"  style="width: 100%;height:600px;"></div>
                                </div>
                                <div class="tab-pane" id="liren-list-box" >
                                    <h4 class="page-header">利润增长趋势统计数据表</h4>
                                    <table id="sample-table-2" class="table table-striped table-bordered table-hover tablesorter">
                                        <thead>
                                            <tr>
                                                <th class="center">
                                                    日期
                                                </th>
                                                <th>总销售额</th>
                                                <th>总毛利</th>
                                                <th>不含其它成本总毛利</th>
                                                <th>除去红包毛利</th>
                                                <th>
                                                    挂单总金额
                                                </th>
                                                <th>
                                                    充值消费
                                                </th>
                                                <th>成本开支</th>
                                                <th>其它成本</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php 
                                                $xAxis                  = HResponse::getAttribute('xAxis');
                                                $chongZhiList           = HResponse::getAttribute('chongZhiList');
                                                $guaDanList             = HResponse::getAttribute('guaDanList');
                                                $otherChengBenList      = HResponse::getAttribute('otherChengBenList');
                                                $noOtherChengBenliRenList= HResponse::getAttribute('noOtherChengBenliRenList');
                                                $inComeList             = HResponse::getAttribute('inComeList');
                                                $chengBenList           = HResponse::getAttribute('chengBenList');
                                                $allLirenList           = HResponse::getAttribute('allLirenList');
                                                $noRedPackageLiRenList  = HResponse::getAttribute('noRedPackageLiRenList');
                                                for($key = count($xAxis) - 1; $key >= 0; $key --) {
                                            ?>
                                            <tr>
                                                <td><?php echo $xAxis[$key];?></td>
                                                <td><strong>￥<?php echo $inComeList[$key]; ?></strong></td>
                                                <td><strong>￥<?php echo $allLirenList[$key]; ?></strong></td>
                                                <td>￥<?php echo $noOtherChengBenliRenList[$key];?></td>
                                                <td>￥<?php echo $noRedPackageLiRenList[$key];?></td>
                                                <td>￥<?php echo $guaDanList[$key];?></td>
                                                <td>￥<?php echo $chongZhiList[$key];?></td>
                                                <td>￥<?php echo $chengBenList[$key];?></td>
                                                <td>￥<?php echo $otherChengBenList[$key];?></td>
                                            </tr>
                                            <?php } ?>
                                            <tr>
                                                <td>合计</td>
                                                <td><strong>￥<?php echo array_sum($inComeList); ?></strong></td>
                                                <td><strong>￥<?php echo array_sum($allLirenList); ?></strong></td>
                                                <td>￥<?php echo array_sum($noOtherChengBenliRenList);?></td>
                                                <td>￥<?php echo array_sum($noRedPackageLiRenList);?></td>
                                                <td>￥<?php echo array_sum($guaDanList);?></td>
                                                <td>￥<?php echo array_sum($chongZhiList);?></td>
                                                <td>￥<?php echo array_sum($chengBenList);?></td>
                                                <td>￥<?php echo array_sum($otherChengBenList);?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="goods-order-box" >
                                    <h4 class="page-header">单品利润报表</h4>
                                    <table id="sample-table-2" class="table table-striped table-bordered table-hover tablesorter">
                                        <thead>
                                            <tr>
                                                <th class="center">
                                                    排名
                                                </th>
                                                <th>菜品名称</th>
                                                <th>利润</th>
                                                <th>总交易额</th>
                                                <th>成本</th>
                                                <th>
                                                    成交数量
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php 
                                                foreach(HResponse::getAttribute('goodsList') as $key => $item) { 
                                            ?>
                                            <tr>
                                                <td><?php echo $key + 1;?></td>
                                                <td>
                                                    <a href="<?php echo HResponse::url('goods/editview', 'id=' . $item['goods_id']);?>">
                                                        <?php echo $item['name'];?>
                                                    </a>
                                                </td>
                                                <td><strong class="text-red">￥<?php echo $item['income'] - $item['chengben'];?></strong></td>
                                                <td><strong>￥<?php echo $item['income']; ?></strong></td>
                                                <td><strong>￥<?php echo $item['chengben']; ?></strong></td>
                                                <td><?php echo $item['number'];?></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- #main-content -->
            </div><!--/.fluid-container#main-container-->
         <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
		<script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>jquery/plugins/jquery.tablesorter/jquery.tablesorter.min.js"></script>       
        <script type="text/javascript">
            dateList.push("#start_time");
            dateList.push("#end_time");
            HJZCommon.bindBtnLookUpShop('#shopname');
            var orderChart      = echarts.init(document.getElementById('order-total'));
            var option = {
                title: {
                    text: '<?php echo !$shopInfo ? '所有' : $shopInfo['name'];?>商家订单走势图'
                },
                tooltip : {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        label: {
                            backgroundColor: '#6a7985'
                        }
                    }
                },
                toolbox: {
                    feature: {
                        saveAsImage: {}
                    }
                },
                legend: {
                    data:['总销售额', '总毛利', '不含其它成本总毛利', '除去红包毛利', '挂单总金额', '充值消费', '成本开支', '其它成本']
                },
                xAxis: {
                    data: <?php echo json_encode(HResponse::getAttribute('xAxis'));?>
                },
                yAxis: {},
                series : [
                    {
                        name:'总销售额',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} 元'}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('inComeList')); ?>
                    },
                    {
                        name:'总毛利',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} 元'}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('allLirenList')); ?>
                    },
                    {
                        name:'不含其它成本总毛利',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} 元'}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('noOtherChengBenliRenList')); ?>
                    },
                    {
                        name:'除去红包毛利',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} 元'}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('noRedPackageLiRenList')); ?>
                    },
                    {
                        name:'挂单总金额',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} 元'}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('guaDanList')); ?>
                    },
                    {
                        name:'充值消费',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} 元'}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('chongZhiList')); ?>
                    },
                    {
                        name:'成本开支',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} 元'}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('chengBenList')); ?>
                    },
                    {
                        name:'其它成本',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} 元'}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('otherChengBenList')); ?>
                    }
                ]
            };
            orderChart.setOption(option);
            //sort table
            var oTable1 = $('#sample-table-2').tablesorter();
        </script>
    </body>
</html>
