<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
        <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>jquery/plugins/jquery.tablesorter/themes/blue/style.css">
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <?php require_once(HResponse::path() . '/common/report-count-cur-location.tpl'); ?>
                    <div class="row-fluid">
                        <div id="table_report_wrapper" class="dataTables_wrapper" role="grid">
                            <?php
                                $startTime  = HResponse::getAttribute('start_time');
                                $endTime    = HResponse::getAttribute('end_time');
                            ?>
                            <div class="row-fluid">
                                <div class="span4">
                                    <div class="search-box fs-18 pl-15">
                                        搜索操作区：
                                    </div>
                                </div>
                                <div class="span8">
                                    <form class="form-inline text-right  pr-15">
                                        <fieldset>
                                            <label>查询日期（限62天以内）：</label>
                                            <label>开始日期</label>
                                            <input class="input-small datetime-picker" name="start_time" type="text" value="<?php echo $startTime; ?>" data-date-format="yyyy-mm-dd" id="start_time">
                                            <label>结束日期</label>
                                            <input  class="input-small datetime-picker" name="end_time" type="text" value="<?php echo $endTime; ?>" data-date-format="yyyy-mm-dd" id="end_time">
                                            <button type="submit" class="btn btn-purple btn-small">查询</button>
                                            <a href="javascript:;" data-href="<?php echo HResponse::url('weektongji/export');?>" id="btn-export" class="btn btn-primary btn-small">导出Excel</a>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="tabbable mt-10">
                            <div class="tab-content">
                                <div class="tab-pane active" id="zhekou-list-box">
                                    <?php 
                                        $totalOrders    = HResponse::getAttribute('totalOrders');
                                        $avgPeople      = HResponse::getAttribute('avgPeople');
                                        $sumData        = HResponse::getAttribute('sumData');
                                        $payAmount      = HResponse::getAttribute('payAmount');
                                        $paymentList    = HResponse::getAttribute('paymentList');
                                        $useChongZhiSum = HResponse::getAttribute('useChongZhiSum');
                                        $useLingQianSum = HResponse::getAttribute('useLingQianSum');
                                        $catList        = HResponse::getAttribute('catList');
                                        $typeMap        = HResponse::getAttribute('typeMap');
                                    ?>
                                    <h2 class="header lighter text-center">
                                        营业汇总统计报表明细
                                        <br/>
                                        <small><?php echo $startTime; ?> —— <?php echo $endTime; ?></small>
                                    </h2>
                                    <h4>消费：</h4>
                                    <table class="table table-striped table-bordered table-hover tablesorter">
                                        <thead>
                                            <tr>
                                                <th class="center">
                                                    开单数
                                                </th>
                                                <th>消费金额</th>
                                                <th>打折金额</th>
                                                <th>实付金额</th>
                                                <th>找零金额</th>
                                                <th>净收金额</th>
                                                <th>应付金额</th>
                                                <th>人均消费</th>
                                                <th>
                                                    人数
                                                </th>
                                                <th class="center">
                                                    桌均消费
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?php echo $totalOrders;?></td>
                                                <td><strong>￥<?php echo $sumData['src_amount']; ?></strong></td>
                                                <td><strong class="text-red">￥<?php echo $sumData['zhe_sum'];?></strong></td>
                                                <td><strong>￥<?php echo $payAmount; ?></strong></td>
                                                <td><strong>￥<?php echo $sumData['zhaoling_sum']; ?></strong></td>
                                                <td><strong>￥<?php echo number_format($payAmount - $sumData['zhaoling_sum'], 2); ?></strong></td>
                                                <td>￥<?php echo $sumData['amount'];?></td>
                                                <td>￥<?php echo number_format($avgPeople, 2);?></td>
                                                <td><?php echo $sumData['people'];?></td>
                                                <td>￥<?php echo number_format($sumData['pay_sum'] / $totalOrders, 2);?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <hr>
                                    <div class="row-fluid">
                                        <div class="span7">
                                            <h4>付款方式：</h4>
                                            <table class="table table-striped table-bordered table-hover tablesorter">
                                                <thead>
                                                    <tr>
                                                        <th class="center">
                                                            付款方式
                                                        </th>
                                                        <th>付款金额</th>
                                                        <th>付款笔数</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $sum = 0;
                                                        $total = 0;
                                                        foreach($paymentList as $item) { 
                                                            $sum += $item['sum'];
                                                            $total += $item['total'];
                                                    ?>
                                                    <tr>
                                                        <td><?php echo OrderpaymentPopo::$typeMap[$item['type']]['name'];?></td>
                                                        <td><strong>￥<?php echo $item['sum']; ?></strong></td>
                                                        <td><strong><?php echo $item['total']; ?></strong></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php 
                                                        if($useChongZhiSum) { 
                                                            $sum += $useChongZhiSum['sum'];
                                                            $total += $useChongZhiSum['total'];
                                                    ?>
                                                    <tr>
                                                        <td>会员充值消费</td>
                                                        <td><strong>￥<?php echo $useChongZhiSum['sum']; ?></strong></td>
                                                        <td><strong><?php echo $useChongZhiSum['total']; ?></strong></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php 
                                                        if($useLingQianSum) { 
                                                            $sum += $useLingQianSum['sum'];
                                                            $total += $useLingQianSum['total'];
                                                    ?>
                                                    <tr>
                                                        <td>会员红包消费</td>
                                                        <td><strong>￥<?php echo $useLingQianSum['sum']; ?></strong></td>
                                                        <td><strong><?php echo $useLingQianSum['total']; ?></strong></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <td>合计：</td>
                                                        <td><strong>￥<?php echo number_format($sum, 2); ?></strong></td>
                                                        <td><strong><?php echo number_format($total, 2); ?></strong></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="span5">
                                            <h4>会员充值：</h4>
                                            <table class="table table-striped table-bordered table-hover tablesorter">
                                                <thead>
                                                    <tr>
                                                        <th class="center">
                                                            存款金额
                                                        </th>
                                                        <th>￥<?php echo HResponse::getAttribute('userChongZhiSum');?></th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    <hr>
                                    <h4>菜品：品类</h4>
                                    <table class="table table-striped table-bordered table-hover tablesorter">
                                        <thead>
                                            <tr>
                                                <th>订餐方式</th>
                                                <?php foreach($catList as $item) { ?>
                                                <th class="center">
                                                    <?php echo $item['name'];?>
                                                </th>
                                                <?php } ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($typeMap as $key => $item) { ?>
                                            <tr>
                                                <td><?php echo OrderPopo::$typeMap[$key]['name'];?></td>
                                                <?php foreach($item as $sum) { ?>
                                                <td>￥<?php echo $sum;?></td>
                                                <?php } ?>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- #main-content -->
            </div><!--/.fluid-container#main-container-->
        <script type="text/javascript">
            dateList.push("#start_time");
            dateList.push("#end_time");
        </script> 
         <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
		<script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>jquery/plugins/jquery.tablesorter/jquery.tablesorter.min.js"></script>       
        <script type="text/javascript">
            $('#btn-export').click(function() {
                try {
                    HHJsLib.isEmptyByDom('#start_time', '开始时间');
                    HHJsLib.isEmptyByDom('#end_time', '结束时间');
                    var href    = $(this).attr('data-href') + '?s=' + $('#start_time').val() + '&e=' + $('#end_time').val();
                    $(this).attr('href', href);
                    return true;
                } catch(e) {
                    return HHJsLib.warn(e);
                }
            });
            //sort table
            var oTable1 = $('#sample-table-2').tablesorter();
        </script>
    </body>
</html>
