<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
        <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>jquery/plugins/jquery.tablesorter/themes/blue/style.css">
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <?php require_once(HResponse::path() . '/common/report-count-cur-location.tpl'); ?>
                    <div class="row-fluid">
                        <!-- PAGE CONTENT BEGINS HERE -->
                        <div id="table_report_wrapper" class="dataTables_wrapper" role="grid">
                            <div class="row-fluid text-right">
                                <form class="form-inline search-box pr-15">
                                    <fieldset>
                                        <label class="lookup-field-box">商家: 
                                            <input type="text" class="input-medium search-query" name="shopname" id="shopname" placeholder="输入商家名称..."  value="<?php echo HRequest::getParameter("shopname"); ?>">
                                        </label>
                                        <?php
                                            $startTime  = HResponse::getAttribute('start_time');
                                            $endTime    = HResponse::getAttribute('end_time');
                                        ?>
                                        <label>开始日期</label>
                                        <input class="datetime-picker" name="start_time" type="text" value="<?php echo $startTime; ?>" data-date-format="yyyy-mm-dd" id="start_time">
                                        <label>结束日期</label>
                                        <input  class="datetime-picker" name="end_time" type="text" value="<?php echo $endTime; ?>" data-date-format="yyyy-mm-dd" id="end_time">
                                        <input type="hidden" name="shop_id" value="<?php echo HResponse::getAttribute('shop_id'); ?>">
                                        <button type="submit" class="btn btn-purple btn-small">查询</button>
                                    </fieldset>
                                </form>
                            </div>
                            <form action="<?php echo HResponse::url('' . $modelEnName . '/quick'); ?>" method="post" id="list-form">
                                <table id="data-grid-box" class="table table-striped table-bordered table-hover tablesorter" >
                                    <thead>
                                    <tr>
                                        <th>排行</th>
                                        <th>菜品名称</th>
                                        <th>销售量</th>
                                        <th>销售额</th>
                                        <th>均价</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                            if(HVerify::isEmpty(HResponse::getAttribute('list'))) {
                                                echo '<tr><td colspan="5" class="center">暂无相关记录，请确认已经输入需要查看的“商家名称”！</td></tr>';
                                    }
                                    foreach(HResponse::getAttribute('list') as $key => $record) {
                                        echo $key % 2 == 0 ? '<tr class="odd"' . '" id="' . $record['id'] .'">' : '<tr ' . '" id="' . $record['id'] .'">';

                                    ?>
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?php echo $record['name']; ?></td>
                                    <td><?php echo $record['number']; ?></td>
                                    <td>￥<strong class="fs-18"><?php echo $record['total_price']; ?></strong></td>
                                    <td>￥<strong class="fs-18"><?php echo $record['avg_price']; ?></strong></td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </form>
                        <!-- PAGE CONTENT ENDS HERE -->
                         </div><!--/row-->
                        </div><!--/#page-content-->
                    </div>
                </div><!-- #main-content -->
            </div><!--/.fluid-container#main-container-->
         <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
		<script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>/jquery/plugins/jquery.tablesorter/jquery.tablesorter.min.js"></script>       
		<script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>js/list.js?v=<?php echo $v;?>"></script>
        <script type="text/javascript">
            dateList.push("#start_time");
            dateList.push("#end_time");
        </script>
    </body>
</html>
