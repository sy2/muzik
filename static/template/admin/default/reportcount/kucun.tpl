<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
        <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>jquery/plugins/jquery.tablesorter/themes/blue/style.css">
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <?php require_once(HResponse::path() . '/common/report-count-cur-location.tpl'); ?>
                    <div class="row-fluid">
                        <div id="table_report_wrapper" class="dataTables_wrapper" role="grid">
                            <div class="row-fluid">
                                <div class="span4">
                                    <div class="search-box  pl-15">
                                        总出库量:  <strong class="red fs-22">
                                        <?php echo array_sum(HResponse::getAttribute('chuKuNumberList')); ?>
                                        </strong>，
                                        总净出库量:  <strong class="red fs-22">
                                        <?php echo array_sum(HResponse::getAttribute('chuKuZhongLiangList')); ?>
                                        </strong>
                                    </div>
                                </div>
                                <div class="span8">
                                    <form class="form-inline text-right  pr-15" action="<?php echo HResponse::url('kucun');?>">
                                        <fieldset>
                                            <label class="lookup-field-box">商家: 
                                                <input type="text" class="input-medium search-query" name="shopname" id="shopname" placeholder="输入商家名称..."  value="<?php echo HRequest::getParameter("shopname"); ?>">
                                            </label>
                                            <label>查询日期（限62天以内）：</label>
                                            <?php
                                                $startTime  = HResponse::getAttribute('start_time');
                                                $endTime    = HResponse::getAttribute('end_time');
                                            ?>
                                            <label>开始日期</label>
                                            <input class="datetime-picker" name="start_time" type="text" value="<?php echo $startTime; ?>" data-date-format="yyyy-mm-dd" id="start_time">
                                            <label>结束日期</label>
                                            <input  class="datetime-picker" name="end_time" type="text" value="<?php echo $endTime; ?>" data-date-format="yyyy-mm-dd" id="end_time">
                                            <button type="submit" class="btn btn-purple btn-small">查询</button>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="tabbable mt-10">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#order-total-box" data-toggle="tab">库存情况走势图</a></li>
                                <li><a href="#kucun-list-box" data-toggle="tab">库存情况走势报表</a></li>
                                <li><a href="#goods-order-box" data-toggle="tab">单品进出库报表</a></li>
                                <li><a href="#goods-sale-box" data-toggle="tab">实际交易单品用量报表</a></li>
                                <li><a href="#goods-number-box" data-toggle="tab">菜品库存报表</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="order-total-box">
                                    <div class="liren-chart-box" id="order-total"  style="width: 100%;height:600px;"></div>
                                </div>
                                <div class="tab-pane" id="kucun-list-box" >
                                    <h4 class="page-header">库存情况走势报表</h4>
                                    <table id="sample-table-2" class="table table-striped table-bordered table-hover tablesorter">
                                        <thead>
                                            <tr>
                                                <th class="center">
                                                    日期
                                                </th>
                                                <th>进库总量</th>
                                                <th>进库净量</th>
                                                <th>进库成本</th>
                                                <th>出库总量</th>
                                                <th>
                                                    出库净量
                                                </th>
                                                <th>
                                                    实际交易使用总量
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php 
                                                $xAxis                  = HResponse::getAttribute('xAxis');
                                                $chuKuZhongLiangList    = HResponse::getAttribute('chuKuZhongLiangList');
                                                $chuKuNumberList        = HResponse::getAttribute('chuKuNumberList');
                                                $jinKuNumberList        = HResponse::getAttribute('jinKuNumberList');
                                                $jinKuZhongLiangList    = HResponse::getAttribute('jinKuZhongLiangList');
                                                $jinKuChengBenList      = HResponse::getAttribute('jinKuChengBenList');
                                                $orderGoodsList         = HResponse::getAttribute('orderGoodsList');
                                                for($key = count($xAxis) - 1; $key >= 0; $key --) {
                                            ?>
                                            <tr>
                                                <td><?php echo $xAxis[$key];?></td>
                                                <td><strong><?php echo $chuKuNumberList[$key]; ?></strong></td>
                                                <td><strong><?php echo $chuKuZhongLiangList[$key]; ?></strong></td>
                                                <td><?php echo $jinKuChengBenList[$key];?></td>
                                                <td><?php echo $jinKuNumberList[$key];?></td>
                                                <td><?php echo $jinKuZhongLiangList[$key];?></td>
                                                <td><?php echo $orderGoodsList[$key];?></td>
                                            </tr>
                                            <?php } ?>
                                            <tr>
                                                <td>合计</td>
                                                <td><strong><?php echo array_sum($chuKuNumberList); ?></strong></td>
                                                <td><strong><?php echo array_sum($chuKuZhongLiangList); ?></strong></td>
                                                <td><?php echo array_sum($jinKuChengBenList);?></td>
                                                <td><?php echo array_sum($jinKuNumberList);?></td>
                                                <td><?php echo array_sum($jinKuZhongLiangList);?></td>
                                                <td><?php echo array_sum($orderGoodsList);?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="goods-order-box" >
                                    <h4 class="page-header">单品进出库报表</h4>
                                    <table id="sample-table-2" class="table table-striped table-bordered table-hover tablesorter">
                                        <thead>
                                            <tr>
                                                <th class="center">
                                                    排名
                                                </th>
                                                <th>菜品名称</th>
                                                <th>进库量</th>
                                                <th>进库净量</th>
                                                <th>
                                                    出库量
                                                </th>
                                                <th>
                                                    出库净量
                                                </th>
                                                <th>
                                                    出售单价
                                                </th>
                                                <th>总成本</th>
                                                <th>
                                                    销售额
                                                </th>
                                                <th>
                                                    毛利
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php 
                                                $goodsJinKuMap  = HResponse::getAttribute('goodsJinKuMap');
                                                $goodsChuKuMap  = HResponse::getAttribute('goodsChuKuMap');
                                                foreach(HResponse::getAttribute('caiGouGoodsList') as $key => $item) { 
                                                    $jin        = $goodsJinKuMap[$item['id']];
                                                    $chu        = $goodsChuKuMap[$item['id']];
                                                    $sum        = floatval($chu['zhongliang']) * $item['sale_price']; 
                                            ?>
                                            <tr>
                                                <td><?php echo $key + 1;?>
                                                </td>
                                                <td>
                                                    <a href="<?php echo HResponse::url('caigougoods/editview', 'id=' . $item['id']);?>">
                                                        <?php echo $item['name'];?>
                                                    </a>
                                                </td>
                                                <td><?php echo $jin['number'] . $item['unit'];?></td>
                                                <td><?php echo $jin['zhongliang'] . $item['unit'];?></td>
                                                <td><strong><?php echo $chu['number'] . $item['unit']; ?></strong></td>
                                                <td><?php echo $chu['zhongliang'] . $item['unit'];?></td>
                                                <td><?php echo $item['sale_price'];?></td>
                                                <td><strong class="text-red">￥<?php echo $jin['chengben']; ?></strong></td>
                                                <td><strong class="text-red">￥<?php echo $sum;?></strong></td>
                                                <td><strong class="text-red">￥<?php echo ($sum - floatval($jin['chengben']));?></strong></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="goods-sale-box" >
                                    <h4 class="page-header">实际交易菜品用量报表</h4>
                                    <table id="sample-table-3" class="table table-striped table-bordered table-hover tablesorter">
                                        <thead>
                                            <tr>
                                                <th class="center">
                                                    排名
                                                </th>
                                                <th>菜品名称</th>
                                                <th>销售数量</th>
                                                <th>
                                                    总用量
                                                </th>
                                                <th>
                                                    总销售额
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php 
                                                foreach(HResponse::getAttribute('goodsList') as $key => $item) { 
                                                    $sum        = floatval($item['zhongliang']) * $item['number']; 
                                            ?>
                                            <tr>
                                                <td><?php echo $key + 1;?>
                                                </td>
                                                <td>
                                                    <a href="<?php echo HResponse::url('goods/editview', 'id=' . $item['goods_id']);?>">
                                                        <?php echo $item['name'];?>
                                                    </a>
                                                </td>
                                                <td><?php echo $item['number'];?></td>
                                                <td><?php echo $item['zhongliang'];?></td>
                                                <td><strong>￥<?php echo $item['sale']; ?></strong></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="goods-number-box" >
                                    <h4 class="page-header">在售菜品库存报表</h4>
                                    <table id="sample-table-2" class="table table-striped table-bordered table-hover tablesorter">
                                        <thead>
                                            <tr>
                                                <th>序号</th>
                                                <th>名称</th>
                                                <th>库存数量</th>
                                                <th>
                                                    总销量
                                                </th>
                                                <th>
                                                    上下架情况
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php 
                                                foreach(HResponse::getAttribute('allGoodsList') as $key => $item) { 
                                            ?>
                                            <tr>
                                                <td><?php echo $key + 1;?> </td>
                                                <td>
                                                    <a href="<?php echo HResponse::url('goods/editview', 'id=' . $item['id']);?>">
                                                        <?php echo $item['name'];?>
                                                    </a>
                                                </td>
                                                <td><?php echo $item['total_number'];?></td>
                                                <td><?php echo $item['total_orders'];?></td>
                                                <td><strong><?php echo 2 == $item['status'] ? '上架' : '下架'; ?></strong></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- #main-content -->
            </div><!--/.fluid-container#main-container-->
         <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
		<script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>jquery/plugins/jquery.tablesorter/jquery.tablesorter.min.js"></script> 
        <script type="text/javascript">
            dateList.push("#start_time");
            dateList.push("#end_time");
            HJZCommon.bindBtnLookUpShop('#shopname');
            var orderChart      = echarts.init(document.getElementById('order-total'));
            var option = {
                title: {
                    text: '本店库存情况走势图'
                },
                tooltip : {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        label: {
                            backgroundColor: '#6a7985'
                        }
                    }
                },
                toolbox: {
                    feature: {
                        saveAsImage: {}
                    }
                },
                legend: {
                    data:['进库总量', '进库净量', '进库成本', '出库总量', '出库净量', '实际交易使用总量']
                },
                xAxis: {
                    data: <?php echo json_encode(HResponse::getAttribute('xAxis'));?>
                },
                yAxis: {},
                series : [
                    {
                        name:'进库总量',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} '}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('jinKuNumberList')); ?>
                    },
                    {
                        name:'进库净量',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} '}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('jinKuZhongLiangList')); ?>
                    },
                    {
                        name:'进库成本',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} '}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('jinKuChengBenList')); ?>
                    },
                    {
                        name:'出库总量',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} '}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('chuKuNumberList')); ?>
                    },
                    {
                        name:'出库净量',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} '}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('chuKuZhongLiangList')); ?>
                    },
                    {
                        name:'实际交易使用总量',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} '}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('orderGoodsList')); ?>
                    }
                ]
            };
            orderChart.setOption(option);
            //sort table
            var oTable1 = $('table.tablesorter').tablesorter();
        </script>
    </body>
</html>
