<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
        <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>jquery/plugins/jquery.tablesorter/themes/blue/style.css">
        <?php $shopInfo     = HResponse::getAttribute('shopInfo');?>
    </head>
    <body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
        <div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
            <div id="main-content" class="clearfix">
                <?php require_once(HResponse::path() . '/common/report-count-cur-location.tpl'); ?>
                    <div class="row-fluid">
                        <div id="table_report_wrapper" class="dataTables_wrapper" role="grid">
                            <div class="row-fluid">
                                <div class="span4">
                                    <div class="search-box  pl-15">
                                        总订单量: <strong class="red fs-22"><?php echo array_sum(HResponse::getAttribute('finishCountList')); ?></strong>单
                                    </div>
                                </div>
                                <div class="span8">
                                    <form class="form-inline text-right  pr-15">
                                        <fieldset>
                                            <label class="lookup-field-box">商家: 
                                                <input type="text" class="input-medium search-query" name="shopname" id="shopname" placeholder="输入商家名称..."  value="<?php echo HRequest::getParameter("shopname"); ?>">
                                            </label>
                                            <label>查询日期（限62天以内）：</label>
                                            <?php
                                                $startTime  = HResponse::getAttribute('start_time');
                                                $endTime    = HResponse::getAttribute('end_time');
                                            ?>
                                            <label>开始日期</label>
                                            <input class="datetime-picker" name="start_time" type="text" value="<?php echo $startTime; ?>" data-date-format="yyyy-mm-dd" id="start_time">
                                            <label>结束日期</label>
                                            <input  class="datetime-picker" name="end_time" type="text" value="<?php echo $endTime; ?>" data-date-format="yyyy-mm-dd" id="end_time">
                                            <button type="submit" class="btn btn-purple btn-small">查询</button>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="tabbable mt-10">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#order-total-box" data-toggle="tab">订单走势统计图</a></li>
                                <li><a href="#liren-list-box" data-toggle="tab">订单走势统计数据表</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="order-total-box">
                                    <div class="liren-chart-box" id="order-total"  style="width: 100%;height:600px;"></div>
                                </div>
                                <div class="tab-pane" id="liren-list-box" >
                                    <h4 class="page-header">订单走势统计数据表</h4>
                                    <table id="sample-table-2" class="table table-striped table-bordered table-hover tablesorter">
                                        <thead>
                                            <tr>
                                                <th class="center">
                                                    日期
                                                </th>
                                                <th>总完成订单数</th>
                                                <th>总销售额</th>
                                                <th>总销量</th>
                                                <th>取消订单数</th>
                                                <th>退款订单数</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php 
                                                $xAxis                 = HResponse::getAttribute('xAxis');
                                                $totalMoneyList        = HResponse::getAttribute('totalMoneyList');
                                                $totalNumberList       = HResponse::getAttribute('totalNumberList');
                                                $waitPayCountList      = HResponse::getAttribute('waitPayCountList');
                                                $finishCountList       = HResponse::getAttribute('finishCountList');
                                                $tuikuanCountList      = HResponse::getAttribute('tuikuanCountList');
                                                $cancelCountList       = HResponse::getAttribute('cancelCountList');
                                                for($key = count($xAxis) - 1; $key >= 0; $key --) {
                                            ?>
                                            <tr>
                                                <td><?php echo $xAxis[$key];?></td>
                                                <td><strong><?php echo $finishCountList[$key]; ?></strong></td>
                                                <td><strong>￥<?php echo $totalMoneyList[$key]; ?></strong></td>
                                                <td><?php echo $totalNumberList[$key];?></td>
                                                <td><?php echo $cancelCountList[$key];?></td>
                                                <td><?php echo $tuikuanCountList[$key];?></td>
                                            </tr>
                                            <?php } ?>
                                            <tr>
                                                <td>合计</td>
                                                <td><strong><?php echo array_sum($finishCountList); ?></strong></td>
                                                <td><strong>￥<?php echo array_sum($totalMoneyList); ?></strong></td>
                                                <td><?php echo array_sum($totalNumberList);?></td>
                                                <td><?php echo array_sum($cancelCountList);?></td>
                                                <td><?php echo array_sum($tuikuanCountList);?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- #main-content -->
            </div><!--/.fluid-container#main-container-->
         <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
        <script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>jquery/plugins/jquery.tablesorter/jquery.tablesorter.min.js"></script>       
        <script type="text/javascript">
            dateList.push("#start_time");
            dateList.push("#end_time");
            HJZCommon.bindBtnLookUpShop('#shopname');
            var orderChart      = echarts.init(document.getElementById('order-total'));
            var option = {
                title: {
                    text: '<?php echo !$shopInfo ? '所有' : $shopInfo['name'];?>商家订单走势图'
                },
                tooltip : {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        label: {
                            backgroundColor: '#6a7985'
                        }
                    }
                },
                toolbox: {
                    feature: {
                        saveAsImage: {}
                    }
                },
                legend: {
                    data:['总成交订单量', '总成交额', '总销量', '取消订单数量', '退款订单数量']
                },
                xAxis: {
                    data: <?php echo json_encode(HResponse::getAttribute('xAxis'));?>
                },
                yAxis: {},
                series : [
                    {
                        name:'总成交订单量',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} 笔'}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('finishCountList')); ?>
                    },
                    {
                        name:'总成交额',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} 元'}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('totalMoneyList')); ?>
                    },
                    {
                        name:'总销量',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} '}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('totalNumberList')); ?>
                    },
                    {
                        name:'取消订单数量',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} 笔'}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('cancelCountList')); ?>
                    },
                    {
                        name:'退款订单数量',
                        type:'line',
                        itemStyle: {
                            normal: {
                                label : {show: true, position: 'top', formatter:'{c} 笔'}
                            }
                        },
                        data: <?php echo json_encode(HResponse::getAttribute('tuikuanCountList')); ?>
                    }
                ]
            };
            orderChart.setOption(option);
            //sort table
            var oTable1 = $('#sample-table-2').tablesorter();
        </script>
    </body>
</html>
