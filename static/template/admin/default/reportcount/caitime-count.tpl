<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <?php require_once(HResponse::path() . '/common/report-count-cur-location.tpl'); ?>
                    <div id="table_report_wrapper" class="dataTables_wrapper" role="grid">
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="search-box pl-15">
                                    总订单数:  <strong class="red fs-22"><?php echo HResponse::getAttribute('totalRows'); ?></strong>笔
                                </div>
                            </div>
                            <div class="span8">
                                <form class="form-inline text-right pr-15">
                                    <fieldset>
                                        <label class="lookup-field-box">商家: 
                                            <input type="text" class="input-medium search-query" name="shopname" id="shopname" placeholder="输入商家名称..."  value="<?php echo HRequest::getParameter("shopname"); ?>">
                                        </label>
                                        <?php
                                            $startTime  = HResponse::getAttribute('start_time');
                                            $endTime    = HResponse::getAttribute('end_time');
                                        ?>
                                        <label>开始日期</label>
                                        <input class="datetime-picker" name="start_time" type="text" value="<?php echo $startTime; ?>" data-date-format="yyyy-mm-dd" id="start_time">
                                        <label>结束日期</label>
                                        <input  class="datetime-picker" name="end_time" type="text" value="<?php echo $endTime; ?>" data-date-format="yyyy-mm-dd" id="end_time">
                                        <button type="submit" class="btn btn-purple btn-small">查询</button>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="tabbable mt-10">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#order-total" data-toggle="tab">上菜用时概况</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="order-total"  style="width: 100%;height:600px;">

                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- #main-content -->
            </div><!--/.fluid-container#main-container-->
         <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
		<script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>/jquery/plugins/jquery.tablesorter/jquery.tablesorter.min.js"></script>       
		<script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>js/list.js?v=<?php echo $v;?>"></script>
        <script type="text/javascript">
            dateList.push("#start_time");
            dateList.push("#end_time");
            var orderChart      = echarts.init(document.getElementById('order-total'));
            var option = {
                title: {
                    text: '上菜用时分布',
                    subtext: '数据时间范围: （<?php echo HResponse::getAttribute('startDate') . '-' . HResponse::getAttribute('endDate'); ?>）'
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data:['上菜用时']
                },
                toolbox: {
                    show: true,
                    feature: {
                        dataZoom: {
                            yAxisIndex: 'none'
                        },
                        dataView: {readOnly: false},
                        magicType: {type: ['line', 'bar']},
                        restore: {},
                        saveAsImage: {}
                    }
                },
                xAxis:  {
                    type: 'category',
                    boundaryGap: false,
                    data: <?php echo HResponse::getAttribute('xAxis');?>
                },
                yAxis: {
                    type: 'value',
                    axisLabel: {
                        formatter: '{value} 分钟'
                    }
                },
                series: [
                    {
                        name:'上菜用时',
                        type:'line',
                        data: <?php echo HResponse::getAttribute('data');?>,
                        markPoint: {
                            data: [
                                {type: 'max', name: '最大值'},
                                {type: 'min', name: '最小值'}
                            ]
                        },
                        markLine: {
                            data: [
                                {type: 'average', name: '平均值'}
                            ]
                        }
                    }
                ]
            };

            orderChart.setOption(option);
        </script>
    </body>
</html>
