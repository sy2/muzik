<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
        <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>/jquery/plugins/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
        <link rel="stylesheet" href="<?php echo HResponse::uri('cdn');?>/jquery/plugins/ztree/css/metroStyle/metroStyle.css" />
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                    <?php $record         = HResponse::getAttribute('record');  ?>
                    <?php require_once(HResponse::path() . '/common/cur-location.tpl'); ?>

                    <form  action="<?php echo HResponse::url($modelEnName . '/' . HResponse::getAttribute('nextAction') ); ?>" method="post" enctype="multipart/form-data" id="info-form">
                        <div class="row-fluid">
                            <!-- PAGE CONTENT BEGINS HERE -->
                            <div class="span9  content-box">
                                <div class="tabbable tabs-shadow tabs-space">
                                    <ul class="nav nav-tabs" id="myTab">
                                        <li class="active"><a data-toggle="tab" href="#base-box"><i class="pink icon-leaf bigger-110"></i> 基本信息</a></li>
                                    </ul>
                                    <div class="tab-content">
                                      <div id="base-box" class="tab-pane in active">
                                        <input type="hidden" name="id" value="<?php echo $record['id'];?>" />
                                        <?php $field = 'name'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                        <?php $field = 'description'; require(HResponse::path() . '/fields/textarea.tpl'); ?>
                                        <hr>
                                        <h3>
                                            权限菜单配置
                                            <small class="help-info pull-right">（请按实际需要，为当前角色分配后台管理权限、菜单哦。）</small>
                                        </h3>
                                        <div class="rights-box">
                                            <div class="row-fluid">
                                                <div class="span4">
                                                    <div class="item-box">
                                                        <h4 class="header smaller lighter blue">
                                                            <label class="pull-right btn-check-all">
                                                                <input type="checkbox"
                                                                    class="ace pull-right ace-checkbox-2" 
                                                                    value="1" />
                                                                <span class="lbl"></span>
                                                                全选
                                                            </label>
                                                            左侧导航菜单分配（<?php echo count(HResponse::getAttribute('leftSidebarMenuNodes'));?>项）
                                                        </h4>
                                                        <div class="controls ztree-checkbox">
                                                            <ul id="left-menu-tree" class="ztree"
                                                                data-cur=""
                                                            ></ul>
                                                        </div>
                                                        <input type="hidden" name="leftmenu_ids" id="left-menu-ids" value="<?php echo $record['leftmenu_ids'];?>">
                                                    </div>
                                                </div>
                                                <div class="span4">
                                                    <div class="item-box">
                                                        <h4 class="header smaller lighter green">
                                                            <label class="pull-right btn-check-all">
                                                                <input type="checkbox"
                                                                    class="ace pull-right ace-checkbox-2" 
                                                                    value="2" />
                                                                <span class="lbl"></span>
                                                                全选
                                                            </label>
                                                            后台桌面菜单分配（<?php echo count(HResponse::getAttribute('modelManagerNodes'));?>项）
                                                        </h4>
                                                        <div class="controls ztree-checkbox">
                                                            <ul id="desktop-tree" class="ztree"
                                                                data-cur=""
                                                            ></ul>
                                                        </div>
                                                        <input type="hidden" name="modelmanager_ids" id="desktop-ids" value="<?php echo $record['modelmanager_ids'];?>">
                                                    </div>
                                                </div>
                                                <div class="span4">
                                                    <div class="item-box">
                                                        <h4 class="header smaller lighter purple">
                                                            <label class="pull-right btn-check-all">
                                                                <input type="checkbox"
                                                                    class="ace pull-right ace-checkbox-2" 
                                                                    value="0" />
                                                                <span class="lbl"></span>
                                                                全选
                                                            </label>
                                                            功能权限分配（<?php echo count(HResponse::getAttribute('rightsNodes'));?>项）
                                                        </h4>
                                                        <div class="controls ztree-checkbox">
                                                            <ul id="rights-tree" class="ztree"
                                                                data-cur=""
                                                            ></ul>
                                                        </div>
                                                        <input type="hidden" name="rights_ids" id="rights-tree-ids" value="<?php echo $record['rights_ids'];?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                            </div>
                            <div class="span3">
                                <div class="widget-box">
                                    <div class="widget-header">
                                        <h4>发布</h4>
                                        <div class="widget-toolbar">
                                            <a href="#" data-action="collapse">
                                                <i class="icon-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <?php $field = 'is_statusbar'; require(HResponse::path() . '/fields/checkbox.tpl'); ?>
                                            <?php $field = 'sort_num'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                            <?php if(!$record && !$record['identifier']) { ?>
                                            <?php $field = 'identifier'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                            <?php } else { ?>
                                            <?php $field = 'identifier'; require(HResponse::path() . '/fields/text-readonly.tpl'); ?>
                                            <?php } ?>
                                            <?php $field = 'create_time'; require(HResponse::path() . '/fields/datetime.tpl'); ?>
                                            <?php require_once(HResponse::path() . '/fields/author.tpl'); ?>
                                            <?php require_once(HResponse::path() . '/common/info-buttons.tpl'); 
?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </form>
                        <div class="clearfix"></div>
                    <!-- PAGE CONTENT ENDS HERE -->
                     </div><!--/row-->
                </div><!--/#page-content-->
			</div><!-- #main-content -->
		</div><!--/.fluid-container#main-container-->
        <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
        <script type="text/javascript"> 
            treeCheckboxList.push({
                dom: '#rights-tree',
                data: <?php echo json_encode(HResponse::getAttribute('rightsNodes')); ?>
            });
            treeCheckboxList.push({
                dom: '#left-menu-tree',
                data: <?php echo json_encode(HResponse::getAttribute('leftSidebarMenuNodes')); ?>
            });
            treeCheckboxList.push({
                dom: '#desktop-tree',
                data: <?php echo json_encode(HResponse::getAttribute('modelManagerNodes')); ?>
            });
        </script>
        <script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>/jquery/plugins/ztree/js/jquery.ztree.core-3.5.min.js?v=<?php echo $v; ?>"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>/jquery/plugins/ztree/js/jquery.ztree.excheck-3.5.min.js?v=<?php echo $v; ?>"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>js/info.js?v=<?php echo $v;?>"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>/js/actor-info.js"></script>
	</body>
</html>
