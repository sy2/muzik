<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
        <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>map/css/map.css" />
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <?php 
                    $copyRecord     = HResponse::getAttribute('copyRecord'); 
                    $record         = HResponse::getAttribute('record'); 
                ?>   
                <?php require_once(HResponse::path() . '/common/cur-location.tpl'); ?>
                    <div class="row-fluid">
                    <!-- PAGE CONTENT BEGINS HERE -->
                        <form action="<?php echo HResponse::url($modelEnName . '/' . HResponse::getAttribute('nextAction') ); ?>" method="post" enctype="multipart/form-data" id="info-form">
                            <div class="row-fluid">

                                <div class="span9 content-box">
                                <!-- PAGE CONTENT BEGINS HERE -->

                                    <?php $field = 'id'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                    <?php $record = !$record ? $copyRecord : $record; ?>
                                    <?php $field = 'name'; require(HResponse::path() . '/fields/text.tpl'); ?>
<?php $field = 'true_name'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
<?php $field = 'password'; require(HResponse::path() . '/fields/password.tpl'); ?>
<?php $field = 'salt'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
<?php $field = 'description'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
<?php $field = 'phone'; require(HResponse::path() . '/fields/text.tpl'); ?>
<?php $field = 'weixin'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
<?php $field = 'qq'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
<?php $field = 'email'; require(HResponse::path() . '/fields/text.tpl'); ?>
<?php $field = 'position'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                <!-- PAGE CONTENT ENDS HERE -->

                                </div>
                                <div class="span3">
                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4>发布</h4>
                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
<?php $field = 'birthday'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                <?php $field = 'area'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
<?php $field = 'sex'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
<?php if(in_array(HSession::getAttribute('actor', 'user'), array('root'))) { ?>
<?php $field = 'parent_id'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
<?php } ?>
<?php $field = 'hash'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                <?php $field = 'image_path'; require(HResponse::path() . '/fields/image.tpl'); ?>
                                                <?php require_once(HResponse::path() . '/common/info-buttons.tpl'); 
 ?>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="widget-box hide">
                                        <div class="widget-header">
                                            <h4>头像&LOGO</h4>
                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <?php $field = 'province'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="widget-box hide">
                                        <div class="widget-header">
                                            <h4>所在地</h4>
                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
<?php $field = 'city'; require(HResponse::path() . '/fields/text-readonly.tpl'); ?>
<?php $field = 'district'; require(HResponse::path() . '/fields/text-readonly.tpl'); ?>
<?php $field = 'street'; require(HResponse::path() . '/fields/text-readonly.tpl'); ?>
<?php $field = 'street_number'; require(HResponse::path() . '/fields/text-readonly.tpl'); ?>

                                                <?php require_once(HResponse::path() . '/common/info-buttons.tpl');  ?>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="hide"/>
                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4>维护</h4>
                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
<?php $field = 'login_time'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
<?php $field = 'ip'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                <?php $field = 'create_time'; require(HResponse::path() . '/fields/datetime.tpl'); ?>
                                                <?php require_once(HResponse::path() . '/fields/author.tpl'); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                             </div><!--/row-->

                          </div>
                         </form>
                    <!-- PAGE CONTENT ENDS HERE -->
                     </div><!--/row-->
                </div><!--/#page-content-->
			</div><!-- #main-content -->
		</div><!--/.fluid-container#main-container-->
        <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
        <div id="drag">
            <div class="title">
                <h2>1.选择位置>2.标注位置>3.保存位置</h2>
                <div>
                    <a class="max" href="javascript:;" title="最大化"></a>
                    <a class="revert" href="javascript:;" title="还原"></a>
                    <a class="close" href="javascript:;" title="关闭"></a>
                </div>
            </div>
            <div class="content">
                <div id="f_container">
                    <div id="container"></div>
                </div>
                <div id="allmap" style="width:790px;height:500px;"></div>
                <div style="margin-top: 15px;text-align: right">
                    <input type="hidden" id="lat">
                    <input type="hidden" id="lng">
                    <input type="button" id="btn-save-loc" class="btn btn-info" value="保存">
                    <input type="button" id="btn-hide-map" class="btn btn-default" value="取消">
                </div>
            </div>
        </div>
        <script type="text/javascript"> var fromId = '<?php echo HRequest::getParameter('fid');?>';</script>
        <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=91E91E12661225fcd55d760997770ded"></script>
        <script src="http://api.map.baidu.com/library/MarkerTool/1.2/src/MarkerTool_min.js" type="text/javascript"></script>
        <script type="text/javascript" src="http://api.map.baidu.com/library/CityList/1.4/src/CityList_min.js"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>map/tuodong.js"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>map/map.js"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>js/info.js?v=<?php echo $v;?>"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>/js/user-info.js"></script>
	</body>
</html>
