<div class="tabbable">
    <ul class="nav nav-tabs" id="myTab">
        <li class="active">
            <a data-toggle="tab" href="#local-file">
                <i class="green icon-home bigger-110"></i>
                本地上传
            </a>
        </li>

        <li>
            <a data-toggle="tab" href="#insert-remote">
                插入远程图片
            </a>
        </li>

        <li>
            <a data-toggle="tab" href="#from-library">
                从库中选择
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="local-file" class="tab-pane in active">
            <div id="uploader" class="wu-example">
                <!--用来存放文件信息-->
                <div id="thelist" class="uploader-list"></div>
                <div class="btns">
                    <div id="picker">选择文件</div>
                    <button id="ctlBtn" class="btn btn-default">开始上传</button>
                </div>
            </div>
        </div>
        <div id="insert-remote" class="tab-pane">
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-url"> 地址： </label>
                <div class="col-sm-9">
                    <input type="text" id="from-url" placeholder="请输入图片地址..." class="col-xs-10 col-sm-5">
                </div>
            </div>
        </div>
        <div id="from-library" class="tab-pane"> </div>
    </div>
</div>
