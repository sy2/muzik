<!DOCTYPE html>
<html lang="en">
	<head>
    <meta http-equiv="X-UA-Compatible" content="IE=8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
    <?php 
        $siteCfg        = HSession::getAttributeByDomain('siteCfg'); 
        $popo           = HResponse::getAttribute('popo'); 
        $modelZhName    = $popo->modelZhName; 
        $modelEnName    = $popo->modelEnName; 
        $langList       = HResponse::getAttribute('lang_id_list');
        $langMap        = HResponse::getAttribute('lang_id_map');
        $modelCfg       = HResponse::getAttribute('modelCfg');
        $curShopInfo    = HResponse::getAttribute('curShopInfo');
        $v              = '20170805002';
        HResponse::setAttribute('title', 'Wenzhou Kirka');
    ?>
    <title><?php echo !HResponse::getAttribute('title') ? $siteCfg['name'] : HResponse::getAttribute('title') . '-' . $siteCfg['name'];?><?php HTranslate::_('后台管理平台'); ?></title>
    <meta name="keywords" content="<?php echo $siteCfg['seo_keywords'];?>">
    <meta name="description" content="<?php echo $siteCfg['seo_desc'];?>">
    <meta name="author" content="yy">
    <meta name="viewport" content="initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
    <meta name="robots" content="all" />
    <meta name="Identifier-URL" content="http://www.hongjuzi.net" />
    <link rel="shortcut icon" href="/favicon.ico?v=2" type="image/x-icon" />
    <link rel="icon" href="/favicon.png" type="image/gif">
    <meta name="robots" content="No" />
    <meta name="Identifier-URL" content="http://www.hongjuzi.net" />
    <link rel="Bookmark" href="/favicon.ico?v=2" type="image/x-icon" />
    <!-- basic styles -->
    <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>/bootstrap/v2/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>/bootstrap/v2/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>/bootstrap/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>/font/font-awesome/v3/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>/css/animate.css" />
    <!--<link rel="stylesheet" href="<?php echo HResponse::url(); ?>vendor/min?v=<?php echo $v;?>&f=vendor/bootstrap/v2/css/bootstrap.min.css|vendor/bootstrap/v2/css/bootstrap-responsive.min.css|vendor/bootstrap/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css|vendor/font/font-awesome/v3/css/font-awesome.min.css|vendor/css/animate.css|static/template/admin/default/css/ace.css|static/template/admin/default/css/ace-responsive.min.css|static/template/admin/default/css/ace-skins.min.css">-->
    <!--[if IE 7]>
    <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>/font/font-awesome/v3/css/font-awesome-ie7.min.css" />
    <link href="http://cdn.staticfile.org/font-awesome/3.2.1/css/font-awesome-ie7.min.css" rel="stylesheet">
    <![endif]-->
    <!-- ace styles -->
    <link rel="stylesheet" href="<?php echo HResponse::uri(); ?>css/ace.css" />
    <link rel="stylesheet" href="<?php echo HResponse::uri(); ?>css/ace-responsive.min.css" />
    <link rel="stylesheet" href="<?php echo HResponse::uri(); ?>css/ace-skins.min.css" />
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="<?php echo HResponse::uri(); ?>/css/ace-ie.min.css" />
    <![endif]-->
    <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>artdialog/css/ui-dialog.css">
    <link rel="stylesheet" href="<?php echo HResponse::uri('cdn') . 'webuploader/webuploader.css'; ?>" />
    <link href="<?php echo HResponse::uri(); ?>css/alifonticon/iconfont.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo HResponse::uri('cdn') . 'css/hjz-style.css'; ?>" />
    <link rel="stylesheet" href="<?php echo HResponse::uri(); ?>css/style.css" />
    <script type="text/javascript">
        var dateList    = [];
        var selectList  = [];
        var datetimeList= [];
        var editorList  = [];
        var tagList     = [];
        var codeEditorList      = [];
        var treeCheckboxList    = [];
        var siteUrl     = '<?php echo HResponse::url(); ?>';
        var queryUrl    = '<?php echo HResponse::url(); ?>index.php/';
        var cdnUrl      = '<?php echo HResponse::uri('cdn'); ?>';
        var siteUri     = '<?php echo HResponse::uri(); ?>';
        var modelEnName = '<?php echo $modelEnName; ?>';
        var formData    = {};
        var langMap     = <?php echo !json_encode($langMap, JSON_UNESCAPED_UNICODE) ? 'null' : json_encode($langMap, JSON_UNESCAPED_UNICODE); ?>;
        var voiceTimes  = <?php echo intval($curShopInfo['voice_times']);?>;
        var refer       = '<?php echo urldecode(HString::decodeHtml(HRequest::getParameter('refer')));?>';
    </script>
    <script type="text/javascript" src="<?php echo HResponse::uri('cdn');?>jquery/jquery.js"></script>
    <script type="text/javascript" src="<?php echo HResponse::uri('cdn');?>jquery/plugins/jquery.autocomplete.min.js"></script>
    <script type="text/javascript" src="<?php echo HResponse::uri('cdn');?>bootstrap/v2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo HResponse::uri('cdn');?>bootstrap/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo HResponse::uri('cdn');?>bootstrap/plugins/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
    <script type="text/javascript" src="<?php echo HResponse::uri('cdn');?>zeroclipboard/ZeroClipboard.js"></script>
    <script type="text/javascript" src="<?php echo HResponse::uri('cdn');?>webuploader/webuploader.min.js"></script>
    <script type="text/javascript" src="<?php echo HResponse::uri('cdn');?>js/echarts.common.min.js"></script>
    <script type="text/javascript" src="<?php echo HResponse::uri('cdn');?>js/echarts.common.min.js"></script>
    <script type="text/javascript" src="<?php echo HResponse::uri('cdn');?>js/md5.min.js"></script>
    <script type="text/javascript" src="<?php echo HResponse::uri('cdn');?>js/js.cookie.js"></script>
    <script type="text/javascript" src="<?php echo HResponse::uri('cdn');?>requirejs/require.js"></script>
    <!--<script type="text/javascript" src="<?php echo HResponse::url(); ?>vendor/min?v=<?php echo $v;?>&f=vendor/jquery/jquery.js|vendor/jquery/plugins/jquery.autocomplete.min.js|vendor/bootstrap/v2/js/bootstrap.min.js|vendor/bootstrap/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js|vendor/bootstrap/plugins/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js|vendor/zeroclipboard/ZeroClipboard.js|vendor/webuploader/webuploader.min.js|vendor/js/echarts.common.min.js"></script>
    <script type="text/javascript" src="<?php echo HResponse::url(); ?>vendor/min?v=<?php echo $v;?>&f=vendor/js/md5.min.js|vendor/js/js.cookie.js|vendor/requirejs/require.js"></script>-->
    <script type='text/javascript' src="<?php echo HResponse::uri('cdn');?>hhjslib/hhjslib.min.js?v=<?php echo $v; ?>"></script>
    <script src="<?php echo HResponse::uri('cdn'); ?>artdialog/dist/dialog-plus-min.js?v=<?php echo $v; ?>"></script>
    <!-- basic scripts -->
    <!-- page specific plugin scripts -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="<?php echo HResponse::uri(); ?>/js/excanvas.min.js"></script>
    <![endif]-->
    <!-- ace scripts -->
    <script src="<?php echo HResponse::uri(); ?>js/ace-elements.min.js?v=<?php echo $v; ?>"></script>
    <script src="<?php echo HResponse::uri(); ?>js/ace.min.js?v=<?php echo $v; ?>"></script>
    <script type="text/javascript">
        HHJsLib.curLang = '<?php echo HSession::getAttribute('identifier', 'lang'); ?>';
    </script>
