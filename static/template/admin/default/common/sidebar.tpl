      <a href="#" id="menu-toggler"><span></span></a><!-- menu toggler -->
      <?php $isMiniStyle = HSession::getAttribute('isMiniStyle');?>
      <div id="sidebar" <?php echo $isMiniStyle ? 'class="menu-min"' : ''; ?>>
				<ul class="nav nav-list">
          <?php
            $leftMenuList   = HResponse::getAttribute('leftMenuList');
            foreach($leftMenuList as $item) { 
              if($item['parent_id'] > 0) { continue; }
              if($item['url'] != '###') { 
          ?>
					<li>
					  <a href="<?php echo HResponse::url($item['url']); ?>" class="single">
						<?php echo HString::decodeHtml($item['extend']);?>
            <span><?php echo $item['name'];?></span>
					  </a>
					</li>
          <?php } else {  ?>
            <li>
              <a href="###" class="dropdown-toggle" >
                <?php echo HString::decodeHtml($item['extend']);?>
                <span><?php echo $item['name'];?></span>
                <b class="arrow icon-angle-down"></b>
              </a>
              <ul class="submenu">
                <?php 
                  foreach($leftMenuList as $sub) { 
                    if($item['id'] != $sub['parent_id']) { continue; }
                ?>
                <li>
                  <a href="<?php echo HResponse::url($sub['url']); ?>">
                    <i class="icon-double-angle-right"></i> <?php echo $sub['name'];?>
                  </a>
                </li>
                <?php } ?>
              </ul>
            </li>
              <?php } ?>
            <?php } ?>
				</ul><!--/.nav-list-->
			</div><!--/#sidebar-->

