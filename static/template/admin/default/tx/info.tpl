﻿<?php require_once(HResponse::path('admin') . '/common/header.tpl'); ?>
	</head>
	<body>
        <?php require_once(HResponse::path('admin') . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path('admin') . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <?php 
                    $copyRecord     = HResponse::getAttribute('copyRecord'); 
                    $record         = HResponse::getAttribute('record'); 
                ?>   
          
                    <div class="row-fluid">
                    <!-- PAGE CONTENT BEGINS HERE -->
                        <form action="<?php echo HResponse::url($modelEnName . '/' . HResponse::getAttribute('nextAction') ); ?>" method="post" enctype="multipart/form-data" id="info-form">
                            <div class="row-fluid">
                                <div class="span9 content-box">
                                <!-- PAGE CONTENT BEGINS HERE -->
                                    <?php $field = 'id'; require(HResponse::path('admin') . '/fields/hidden.tpl'); ?>
                                    <?php $record = !$record ? $copyRecord : $record; $trecord = $record;?>
                                <!-- PAGE CONTENT ENDS HERE -->
                                    <div class="control-group">
                                        <label class="control-label">
                                            客户姓名:                                                                                                                  </label>
                                        <div class="controls">
                                            <input type="text"  readonly class="span12" value="<?php echo $record['name']; ?>">
                                        </div>
                                    </div>
                                    <?php $record = HResponse::getAttribute('curUserInfo'); ?>
                                    <div class="control-group">
                                        <label class="control-label">
                                            推荐人信息：                                                                                                                    </label>
                                        <div class="controls">
                                            <span class="span3"><label class="control-label">姓名:</label><input type="text" readonly value="<?php echo $record['true_name']; ?>"></span>
                                            <span class="span3"><label class="control-label">银行名称:</label><input type="text" readonly  value="<?php echo UserinfoPopo::$yinghangMap[$record['khh']]['name']; ?>"></span>
                                            <span class="span3"><label class="control-label">银行账号:</label><input type="text" readonly value="<?php echo $record['yhzh']; ?>"></span>
                                            <span class="span3"><label class="control-label">银行卡号:</label><input type="text" readonly value="<?php echo $record['zhm']; ?>"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">
                                            推荐人佣金                                                                                                                  </label>
                                        <div class="controls">
                                            <input type="text"  readonly class="span12" value="<?php echo $record['tuijianMoney']; ?>">
                                        </div>
                                    </div>
                                    <?php $record = HResponse::getAttribute('superUserInfo'); ?>
                                    <?php if( $record ) { ?>
                                    <div class="control-group">
                                        <label class="control-label">
                                            推荐人上级：                                                                                                                  </label>
                                        <div class="controls">
                                            <span class="span3"><label class="control-label">姓名:</label><input type="text" readonly value="<?php echo $record['true_name']; ?>"></span>
                                            <span class="span3"><label class="control-label">银行名称:</label><input type="text" readonly  value="<?php echo UserinfoPopo::$yinghangMap[$record['khh']]['name']; ?>"></span>
                                            <span class="span3"><label class="control-label">银行账号:</label><input type="text" readonly value="<?php echo $record['yhzh']; ?>"></span>
                                            <span class="span3"><label class="control-label">银行卡号:</label><input type="text" readonly value="<?php echo $record['zhm']; ?>"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">
                                            推荐人上级奖励                                                                                                                 </label>
                                        <div class="controls">
                                            <input type="text"  readonly class="span12" value="<?php echo $record['tuijianMoney']; ?>">
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <div class="control-group">
                                        <label class="control-label">
                                            状态                                                                                                                 </label>
                                        <div class="controls">
                                            <input type="text"  readonly class="span12" value="<?php echo Recompopo::$statusMap[$trecord['status']]['name']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="span3">
                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4>发布</h4>
                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <?php $field = 'create_time'; require(HResponse::path('admin') . '/fields/datetime.tpl'); ?>
                                                <?php require_once(HResponse::path('admin') . '/common/info-buttons.tpl');
 ?>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <?php 
                                        if($modelCfg && '2' == $modelCfg['has_multi_lang']) { 
                                            require_once(HResponse::path('admin') . '/common/lang-widget.tpl'); 
                                            echo '<hr/>';
                                        }
                                    ?>  

                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4>维护</h4>
                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <?php require_once(HResponse::path('admin') . '/fields/author.tpl'); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                             </div><!--/row-->

                          </div>
                         </form>
                    <!-- PAGE CONTENT ENDS HERE -->
                     </div><!--/row-->
                </div><!--/#page-content-->
			</div><!-- #main-content -->
		</div><!--/.fluid-container#main-container-->
        <?php require_once(HResponse::path('admin') . '/common/footer.tpl'); ?>
        <script type="text/javascript"> var fromId = '<?php echo HRequest::getParameter('fid');?>';</script>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>/js/info.js"></script>
	</body>
</html>
