
/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
HHJsLib.register({
    init: function() {
        this.bindTreeCheckBoxList();
        this.bindBtnCheckAll();
        this.bindInfoFormSubmit();
    },
    bindInfoFormSubmit: function() {
        var self    = this;
        $('#info-form').unbind().submit(function() {
            try {
                var $form   = $("#info-form");
                HJZInfo.autoVerifyFields();
                self.setTreeCheckNodesIds(0, '#rights-tree-ids');
                self.setTreeCheckNodesIds(1, '#left-menu-ids');
                self.setTreeCheckNodesIds(2, '#desktop-ids');
                HJZInfo.postByAjax($form, 1);
            } catch(e) {
                var name    = $('#' + _root._this.attr('name') + '-box > label').text();
                name        = $.trim(name).replace(/\s/g, '').replace('*', '');
                HHJsLib.warn(name + e);
                HJZInfo._this.focus();
                HJZInfo.showTips(e, _root._this);
            }
            return false;
        });
    },
    setTreeCheckNodesIds: function(index, field) {
        if('undefined' == typeof(this.ztreeObj[index])) {
            return HHJsLib.warn('权限资源不存在，请确认！');
        }
        var nodes   = this.ztreeObj[index].getCheckedNodes(true);
        var ids     = [];
        for(var ele in nodes) {
            ids.push(nodes[ele].id);
        }
        $(field).val(ids.join(','));
    },
    bindBtnCheckAll: function() {
        var self    = this;
        $('label.btn-check-all input').click(function() {
            var val     = $(this).val();
            if($(this).prop('checked')) {
                self.ztreeObj[val].checkAllNodes(true);
            } else {
                self.ztreeObj[val].checkAllNodes(false);
            }
        });
    },
    ztreeObj: {},
    bindTreeCheckBoxList: function() { 
        if(1 > treeCheckboxList.length) {
            return;
        }
        var self    = this;
        var setting = {
            check: {
                chkStyle: "checkbox",
                radioType: "all",
                enable: true 
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback: {
                onCheck: function() {
                }
            }
        };
        for(var ele in treeCheckboxList) {
            this.ztreeObj[ele]   = $.fn.zTree.init($(treeCheckboxList[ele].dom), setting, treeCheckboxList[ele].data);
        }
    }
});


