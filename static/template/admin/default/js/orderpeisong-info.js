
/**
 * @version $Id$
 * @create Sat 03 Aug 2013 17:50:39 CST By xjiujiu
 * @description HongJuZi Framework
 * @copyRight Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
HHJsLib.register({
    init: function() {
        this.bindSchoolChange();
        this.bindNianJiIdChange();
    },
   
    bindSchoolChange: function() {
        var self    = this;
        $('#school_id').bind('change', function() {
            var val     = $(this).val();
            //console.log(val);
            if(!val) {
                $('#naiji_id').val('<option value="">请选择</option>');
                $('#banji_id').val('<option value="">请选择</option>');
                return;
            }
            self.loadNianJiList(val);
        });
    },
    bindNianJiIdChange: function() {
        var self    = this;
        $('#nianji_id').bind('change', function() {
            var val = $(this).val();
            if(!val) {
                $('#banji_id').val('<option value="">全部</option>');
                return;
            }
            self.loadBanJiList(val);
        });
    },
    loadNianJiList: function(sid) {
        if(!sid) {
            return;
        }
        var self    = this;
        $.getJSON(
            queryUrl + 'admin/nianji/alist',
            {pid: sid}, 
            function(response) {
                if(false === response.rs) {
                    return HHJsLib.warn(response.message); 
                }
                var optHtml     = '<option value="">请选择</option>';
                for(var ele in response.data) {
                    var item    = response.data[ele];
                    optHtml     += '<option value="' + item.id + '">' + item.name + '</option>';
                }
                $('#nianji_id').html(optHtml);
                HHJsLib.autoSelect('#nianji_id');
                $('#nianji_id').trigger("chosen:updated");
                self.loadBanJiList($('#nianji_id').val());
            }
        );
    },
    loadBanJiList: function(sid) {
        if(!sid) {
            return;
        }
        $.getJSON(
            queryUrl + 'admin/banji/alist',
            {pid: sid}, 
            function(response) {
                if(false === response.rs) {
                    return HHJsLib.warn(response.message); 
                }
                var optHtml     = '<option value="">请选择</option>';
                for(var ele in response.data) {
                    var item    = response.data[ele];
                    optHtml     += '<option value="' + item.id + '">' + item.name + '</option>';
                }
                $('#banji_id').html(optHtml);
                HHJsLib.autoSelect('#banji_id');
                $('#banji_id').trigger("chosen:updated");
            }
        );
    }
});
