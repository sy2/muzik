
/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
HHJsLib.register({
    init: function() {
        this.bindGetQQ();
        this.bindProvinceChange();
        this.bindCityChange();
    },
    bindGetQQ: function() {
        $('#btn-get-qq').click(function() {
            var codeEditor = HHJsLib.codeEditor['html-source'];
            var content = codeEditor.getValue();
            var qqCodeEditor = HHJsLib.codeEditor['get-qq'];
            var reg = /<span class=\"member_id\">\([0-9]+\)<\/span>/ig;
            var qq = content.match(reg);
            var qqs = '';
            for(var item in qq){
                qqs += qq[item].match(/[0-9]+/ig) + '\n';
            }
            qqCodeEditor.setValue(qqs);
        });
    },
    bindProvinceChange: function(){
        $('#province').change(function(){
            var province = $(this).val();
            $('#city').empty();
            $('#district').empty();
            $.getJSON(
                queryUrl + '/admin/' + modelEnName + '/acity',
                {province: province},
                function(response){
                    if(response.rs === true){
                        var data = response.data;
                        if(data != '' && data != null && data.length > 0){
                            var optHtml = '';
                            for(item in data){
                                var selected = item == 0 ? 'result-selected' : '';
                                optHtml += '<option value="' + data[item]['id'] + '">' + data[item]['name'] + '</option>';
                            }
                            $('#city').html(optHtml);
                            $('#city').trigger('change');
                        }
                    }
                }
            );
        });
    },
    bindCityChange: function(){
        $('#city').change(function(){
            var city = $(this).val();
            $('#district').empty();
            $.getJSON(
                queryUrl + '/admin/' + modelEnName + '/adistrict',
                {city: city},
                function(response){
                    if(response.rs === true){
                        var data = response.data;
                        if(data != '' && data != null && data.length > 0){
                            var optHtml = '';
                            for(item in data){
                                var selected = item == 0 ? 'result-selected' : '';
                                optHtml += '<option value="' + data[item]['id'] + '">' + data[item]['name'] + '</option>';
                            }
                            $('#district').html(optHtml);
                        }
                    }
                }
            );
        });
    }
});
