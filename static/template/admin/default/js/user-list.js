
/**
 * @version $Id$
 * @create Sat 03 Aug 2013 17:50:39 CST By xjiujiu
 * @description HongJuZi Framework
 * @copyRight Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
HHJsLib.register({
    init: function() {
        this.bindLookOpenId();
    },
    bindLookOpenId: function() {
        $("a.look-open-id").click(function() {
            try{
                var id      = $(this).attr('data-id');
                if(!id){
                    return HHJsLib.warn('编号不能为空');
                }
                $.getJSON(
                    queryUrl + 'admin/user/agetopenid',
                    {id: id},
                    function(response){
                        if(false === response.rs){
                            $('#myModal').find('.modal-body').html(response.message);
                            $('#myModal').modal('show');
                            return false;
                        }
                        $('#myModal').find('.modal-body').html(response.data);
                        $('#myModal').modal('show');
                    }
                );
            }catch(e)
            {
                return HHJsLib.warn(e);
            }
        });
    }
});
