
/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */

HHJsLib.register({
    init: function() {
        this.bindBtnUserAdd('#user-id');
    },
    bindBtnUserAdd: function(dom) {
        var self = this;
        $(dom).unbind().click(function() {
            var title       = '选择发放用户';
            var content     = $('#add-user-tpl').html();
            $('#myModalLabel').html(title);
            $('#myModal').find('#modal-body').html(content);
            $('#myModal').modal('show');
            self.bindBtnSearchFormSubmit();
        });
    },
    bindBtnSearchFormSubmit: function() {
        var self    = this;
        $('#search-form').submit(function() {
            try {
                self.doSearchUserList(1, $('#category').val());
                return false;
            } catch(e) {
                return HHJsLib.warn(e);
            }
        });
    },
    doSearchUserList: function(page, catId) {
        var self        = this;
        var keyword     = $('#keyword').val();
        $.getJSON(
            queryUrl + 'admin/user/alist',
            {keyword: keyword, page: page, parent_id: catId},
            function(response) {
                if(false === response.rs) {
                    return HHJsLib.warn(response.message);
                }
                $('#search-user-page').val(page + 1);
                var tableTpl    = $('#search-user-result-tpl').html();
                var itemHtml    = self.getSearchUserListHtml(response.data, catId);
                tableTpl        = tableTpl.replace(/{content}/g, itemHtml);
                $('#user-table-box').html(tableTpl);
                var prePage     = page - 1 > 0 ? page - 1 : 1;
                var nextPage    = page + 1 > response.totalPages ? response.totalPages : page + 1;
                $('#cur-cat-id').val(catId);
                self.bindBtnAddUserToNotice();
                self.bindBtnAddAttrBatch();
                self.bindCheckUserAll();
                self.initSearchTablePages(parseInt(page), response);
                self.bindBtnPreNextSearchPages(function(page) {
                    self.doSearchUserList(page, catId);
                });
            },
            'JSON'
        );
    },
    getSearchUserListHtml: function(list, catId) {
        if(!list || 1 > list.length) {
            return '<tr><td colspan="6"><div  class="text-center">没有相关用户哦～</div></td></tr>';
        }
        var itemTpl = $('#result-user-item-tpl').html();
        var tdHtml  = '';
        for(var ele in list) {
            var item    = list[ele];
            tdHtml      += itemTpl.replace(/{id}/g, item.id)
                .replace(/{name}/g, item.name)
                .replace(/{cat_id}/g, catId)
                .replace(/{true_name}/g, item.true_name)
                .replace(/{actor}/g, item.actor)
                .replace(/{phone}/g, item.phone);
        }

        return tdHtml;
    },
    bindCheckUserAll: function() {
        $('#search-check-user-all').on('click' , function(){
            var that = this;
            $('#user-table-box tr > td input.que-checkbox').each(function(){
                this.checked = that.checked;
                $(this).closest('tr').toggleClass('selected');
            });
        });
    },
    initSearchTablePages: function(page, response) {
        var prePage     = page - 1 > 0 ? page - 1 : 1;
        var nextPage    = page + 1 > response.totalPages ? response.totalPages : page + 1;
        $('#btn-search-pre-page').attr('data-page', prePage);
        $('#btn-search-next-page').attr('data-page', nextPage);
        $('#cur-page-info').html(page + ' / ' + response.totalPages);
        $('#btn-search-next-page,#btn-search-pre-page').attr('disabled', null);
        if(1 == response.totalPages) {
            $('#btn-search-next-page,#btn-search-pre-page').attr('disabled', 'disabled');
        } else if(1 >= page) {
            $('#btn-search-pre-page').attr('disabled', 'disabled');
        } else if (page == response.totalPages) {
            $('#btn-search-next-page').attr('disabled', 'disabled');
        }
    },
    bindBtnPreNextSearchPages: function(callback) {
        $('#btn-search-next-page,#btn-search-pre-page').unbind().click(function() {
            if(null != $(this).attr('disabled')) {
                return false;
            }
            var page    = $(this).attr('data-page');
            if('undefined' !== typeof(callback)) {
                return callback(page);
            }
        });
    },
    bindBtnAddUserToNotice: function() {
        var self    = this;
        $('#user-table-box a.btn-add-search-user').click(function() {
            var id      = $(this).attr('data-id');
            var name    = $(this).attr('data-name');
            self.appendUser(id, name);
            $('#search-user-item-' + id).fadeOut('fast', function() {
                $(this).remove();
            });
        });
    },
    appendUser: function(id, name) {
        var parentName  = $('#user-id').val();
        var parentIds   = $('#user-ids').val();
        if(parentIds == 0){
            parentIds = '';
        }if(parentName == 0){
            parentName = '';
        }
        parentIds += id + ',';
        parentName += name + ',';
        $('#user-id').val(parentName);
        $('#user-ids').val(parentIds);
    },
    bindBtnAddAttrBatch: function() {
        var self    = this;
        $('#myModal a.btn-add-user-batch').unbind().on('click', function(){
            try{
                var items       = $("#search-user-list input:checked");
                if(typeof items.length == 'undefined' || items.length < 1 ) {
                    return HHJsLib.warn("请选择要添加的用户！");
                }
                items.each(function(){
                    var id      = $(this).val();
                    var name    = $(this).attr('data-name');
                    self.appendUser(id, name);
                    $('#search-user-item-' + id).fadeOut('fast', function() {
                        $(this).remove();
                    });
                });
            } catch(e) {
                return HHJsLib.warn(e);
            }
        });
    }
});
