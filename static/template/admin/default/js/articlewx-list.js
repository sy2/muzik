
/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */

HHJsLib.register({
    init: function() {
        this.bindBtnSyncWechat();
    },
    bindBtnSyncWechat: function() {
        $('#btn-sync-wechat').click(function() {
            HHJsLib.info('正在同步中，请稍候...');
            $.getJSON(
                queryUrl + 'admin/articlewx/sync',
                {},
                function(response) {
                    if(false === response.rs) {
                        return HHJsLib.warn(response.message);
                    }
                    HHJsLib.info('同步成功，自动刷新');
                    window.location.reload();
                }
            );
        });
    }
});
