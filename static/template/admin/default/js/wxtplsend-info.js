
/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
HHJsLib.register({
    init: function() {
        this.bindParentIdChange();
        $('select.title-color').ace_colorpicker();
    },
    bindParentIdChange: function() {
        $('#parent_id').bind('change blur', function() {
            var id  = $(this).val();
            if(!id) {
                return;
            }
            $.getJSON(
                queryUrl + 'admin/wxtpl/ainfo',
                {id: id},
                function(response) {
                    if(false === response.rs) {
                        return HHJsLib.warn(response.message);
                    }
                    var partHtml    = '';
                    var tpl         = $('#part-tpl').html();
                    for(var ele in response.data) {
                        var item    = response.data[ele];
                        partHtml    += tpl.replace(/{label}/g, item.label)
                        .replace(/{key}/g, item.key);
                    }
                    $('#tpl-part-box').html(partHtml);
                    $('select.title-color').ace_colorpicker();
                }
            );
        });
    }
});