
/**
 * @version $Id$
 * @author xjiujiu <xjiujiu@foxmail.com>
 * @description HongJuZi Framework
 * @copyright Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
HHJsLib.register({
    init: function() {
        this.assignNewsList();
        this.assignSupportList();
    },
    assignNewsList: function() {
        var _self = this;
        $.getJSON(
            'http://www.hongjuzi.com.cn/index.php/api/news/agetlist?jsoncallback=?',
            {identifier: 'weishop-news'},
            function(response) {
                if(false == response.rs) {
                    return HHJsLib.warn(response.message);
                }
                var tpl = '';
                for(var index in response.data) {
                    tpl +=_self._getItemTpl(response.data[index]);
                } 
                $('#news-list').empty().append(tpl);
            }
        );
    },
    _getItemTpl: function(item) {
        var url = 'http://www.hongjuzi.com.cn/index.php/article?id=' + item.id;
        var tpl = '<li class="item"><a href="' + url + '" class="">' + item.name + '</a></li>';
        
        return tpl;
    },
    assignSupportList: function() {
        var _self = this;
        $.getJSON(
            'http://www.hongjuzi.com.cn/index.php/api/support/agetlist?jsoncallback=?',
            {identifier: 'weishop-support'},
            function(response) {
                if(false == response.rs) {
                    return HHJsLib.warn(response.message);
                }
                var tpl = '';
                for(var index in response.data) {
                    tpl +=_self._getItemTpl(response.data[index]);
                } 
                $('#help-list').empty().append(tpl);
            }
        );
    }
});