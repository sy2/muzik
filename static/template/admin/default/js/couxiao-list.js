HHJsLib.register({
    init: function() {
        this.bindBtnCxActivity();
        this.bindCouXiaoBtnEdit();
        this.bindBtnCancelCx();
        this.bindBtnCouXiaoDel();
        this.bindBtnSaveCx();
    },
    bindBtnCxActivity: function() {
        $('#btn_cx_activity_id_del').click(function(){
            $('#cx_activity_id').val(0);
            $('#cx_activity_id-box').children('div.controls').hide();
        });
    },
    bindBtnCouXiaoDel: function() {
        $('a.btn-cx-del').click(function() {
            var id  = $(this).attr('data-id');
            if(!confirm('您真的要删除此促销吗？删除会检测是否有订单销售，对于有销量的促销不能删除哦！')) {
                return false;
            }
            $.getJSON(
                queryUrl + 'admin/couxiao/adelete',
                {id: id},
                function(response) {
                    if(false === response.rs) {
                        return HHJsLib.warn(response.message);
                    }
                    $('#row-' + id).fadeOut('fast', function() {
                        $(this).remove();
                    });
                }
            );
        });
    },
    bindBtnSaveCx: function() {
        $('a#btn-save-cx').click(function() {
            try {
                HHJsLib.isEmptyByDom('#id', '商品编号');
                HHJsLib.isEmptyByDom('#shop_id', '供货商编号');
                HHJsLib.isEmptyByDom('#cx_type', '促销类别');
                HHJsLib.isEmptyByDom('#cx_price', '促销价格');
                HHJsLib.isEmptyByDom('#cx_start_time', '促销开始时间');
                HHJsLib.isEmptyByDom('#cx_end_time', '促销结束时间');
                HHJsLib.isEmptyByDom('#cx_number', '参与促销最少数量');
                HHJsLib.isEmptyByDom('#cx_buy_number', '用户限够数量');
                if(!confirm('您真的要更新此促销吗？')) {
                    return false;
                }
                var data = {
                    parent_id: $('#id').val(),
                    shop_id: $('#shop_id').val(),
                    id: $('#cx_id').val(),
                    type: $('#cx_type').val(),
                    price: $('#cx_price').val(),
                    activity_id: $('#cx_activity_id').val(),
                    start_time: $('#cx_start_time').val(),
                    end_time: $('#cx_end_time').val(),
                    buy_number: $('#cx_buy_number').val(),
                    number: $('#cx_number').val(),
                    comment: $('#cx_comment').val(),
                    status: $('#cx_status').val()
                };
                $.post(
                    queryUrl + 'admin/couxiao/asave',
                    data,
                    function(response) {
                        if(false === response.rs) {
                            return HHJsLib.warn(response.message);
                        }
                        HHJsLib.succeed('恭喜您，促销保存成功！');
                        $('#btn-cancel-cx').click();
                        $('#cx_id').val(response.data.id);
                    },
                    'json'
                );
                return true;
            } catch(e) {
                return HHJsLib.warn(e);
            }
        });
    },
    bindBtnCancelCx: function() {
        $('#btn-cancel-cx').click(function() {
            $(this).hide();
            $('#cx-title').html('快速添加促销');
            $('#cx_id').val('');
            $('#cx_price').val('0');
            $('#cx_type').attr('data-cur', '0');
            $('#cx_activity_id').attr('data-cur', '0');
            $('#cx_start_time').val('');
            $('#cx_end_time').val('');
            $('#cx_buy_number').val('');
            $('#cx_number').val('');
            $('#cx_comment').val('');
            $('#cx_status').attr('data-cur', '0');
            HHJsLib.autoSelect('#cx_type, #cx_activity_id');
        });
    },
    bindCouXiaoBtnEdit: function() {
        $('a.btn-cx-edit').click(function() {
            var id  = $(this).attr('data-id');
            $.getJSON(
                queryUrl + 'admin/couxiao/ainfo',
                {id: id},
                function(response) {
                    if(false === response.rs) {
                        return HHJsLib.warn(response.message);
                    }
                    $('#btn-save-cx').attr('data-id', response.data.id);
                    $('#cx-title').html('<i class="icon-pencil bigger-130"></i> 编辑商品促销编号：#' + response.data.id);
                    $('#cx-tools-box a.btn').show();
                    $('#cx_id').val(response.data.id);
                    $('#cx_type').attr('data-cur', response.data.type);
                    $('#cx_price').val(response.data.price);
                    $('#cx_activity_id').attr('data-cur', response.data.activity_id);
                    $('#cx_start_time').val(response.data.start_time);
                    $('#cx_end_time').val(response.data.end_time);
                    $('#cx_buy_number').val(response.data.buy_number);
                    $('#cx_comment').val(response.data.comment);
                    $('#cx_number').val(response.data.number);
                    $('#cx_status').attr('data-cur', response.data.status);
                    $('#cx_type option[selected=selected]').attr('selected', null);
                    $('#cx_status option[selected=selected]').attr('selected', null);
                    $('#cx_activity_id option[selected=selected]').attr('selected', null);
                    HHJsLib.autoSelect('#cx_type, #cx_activity_id, #cx_status');
                }
            );
        });
    },
    settingCXLimitNumberShow: function(val) {
        if(val == 3) {
            $('#cx_number-box').parent().show();
        } else {
            $('#cx_number').val(0);
            $('#cx_number-box').parent().hide();
        }
    }
});