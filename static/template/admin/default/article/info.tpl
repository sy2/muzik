<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
        <link rel="stylesheet" href="<?php echo HResponse::uri('cdn');?>/jquery/plugins/ztree/css/metroStyle/metroStyle.css" />
        <link rel="stylesheet" href="<?php echo HResponse::uri();?>css/colorpicker.css">
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <?php 
                    $copyRecord     = HResponse::getAttribute('copyRecord');
                    $record         = HResponse::getAttribute('record'); 
                ?>    
                <?php require_once(HResponse::path() . '/common/cur-location.tpl'); ?>
                    <form  action="<?php echo HResponse::url($modelEnName . '/' . HResponse::getAttribute('nextAction') ); ?>" method="post" enctype="multipart/form-data" id="info-form">
                        <div class="row-fluid">
                            <div class="span9 content-box">
                            <!-- PAGE CONTENT BEGINS HERE -->
                                <?php $field = 'id'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                <?php $field = 'parent_path'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                <?php $record = !$record ? $copyRecord : $record; ?>
                                <?php $field = 'name'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                <?php $field = 'tags'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                <?php $field = 'content'; require(HResponse::path() . '/fields/editor.tpl'); ?>
                                <div class="clearfix"></div>
                                <hr/>
                                <?php $field = 'extend'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                            <!-- PAGE CONTENT ENDS HERE -->
                            </div>
                            <div class="span3">
                                <div class="widget-box">
                                    <div class="widget-header">
                                        <h4>发布</h4>
                                        <div class="widget-toolbar">
                                            <a href="#" data-action="collapse">
                                                <i class="icon-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <?php $field = 'parent_id'; require(HResponse::path() . '/fields/select.tpl'); ?>
                                            <?php $field = 'sort_num'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                            <?php $field = 'identifier'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                            <?php $field = 'status'; require(HResponse::path() . '/fields/select.tpl'); ?>
                                            <?php require_once(HResponse::path() . '/common/info-buttons.tpl'); ?>
                                        </div>
                                    </div>
                                </div>
                                <hr/>

                                <div class="widget-box">
                                    <div class="widget-header">
                                        <h4>封面</h4>
                                        <div class="widget-toolbar">
                                            <a href="#" data-action="collapse">
                                                <i class="icon-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <?php $field = 'image_path'; require(HResponse::path() . '/fields/image.tpl'); ?>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="widget-box">
                                    <div class="widget-header">
                                        <h4>维护</h4>
                                        <div class="widget-toolbar">
                                            <a href="#" data-action="collapse">
                                                <i class="icon-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <?php $field = 'total_great'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                            <?php $field = 'total_comments'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                            <?php $field = 'total_visits'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                            <?php $field = 'edit_time'; require(HResponse::path() . '/fields/datetime.tpl'); ?>
                                            <?php $field = 'create_time'; require(HResponse::path() . '/fields/datetime.tpl'); ?>
                                            <?php require_once(HResponse::path() . '/fields/author.tpl'); ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                         </div><!--/row-->
                     </form>
                </div><!--/#page-content-->
			</div><!-- #main-content -->
		</div><!--/.fluid-container#main-container-->
        <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
        <script type="text/javascript" src="<?php echo HResponse::uri(); ?>js/info.js?v=<?php echo $v;?>"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>/jquery/plugins/ztree/js/jquery.ztree.core-3.5.min.js"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>/jquery/plugins/ztree/js/jquery.ztree.excheck-3.5.min.js"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri(); ?>js/bootstrap-colorpicker.min.js"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri(); ?>/js/article.js"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri(); ?>/js/sidebar-category.js"></script>
        <script type="text/template"  id="add-category-box-tpl">
            <form method="post" id="form-new-cat">
                <div class="add-category-box">
                    <div class="row-fluid">
                        <div class="control-group">
                            <label class="control-label" for="new-category-name">名称：</label>
                            <div class="controls">
                                <input type="text" id="new-cat-name" class="span12" value="" placeholder="请添输入分类名称"/>
                            </div>
                            <div class="clearfix"></div>
                        </div> 
                        <div class="control-group">
                            <label class="control-label" for="new-parent-id">父类： </label>
                            <div class="controls">
                                <select id="new-parent-id" data-cur="" class="auto-select span12">
                                    <option value="">请选择父类</option>
                                    {opts}
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </form>
        </script>
	</body>
</html>
