<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-dashboard"></i> <a href="<?php echo HResponse::url('index'); ?>">后台桌面</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li>
                            更多
                        </li>
                    </ul><!--.breadcrumb-->
                    <div id="nav-search">
                        <span id="time-info">正在加载时钟...</span>
                    </div><!--#nav-search-->
                </div><!--#breadcrumbs-->
                <div id="page-content" class="clearfix">
                    <div class="page-header position-relative">
                        <h1>
                            <a href="javascript:history.go(-1);" class="pull-right btn btn-info btn-mini mt-5">
                                <i class="icon-arrow-left"></i> 返回
                            </a>
                            更多<small> <i class="icon-double-angle-right"></i> 您可以轻松使用系统功能 </small>
                        </h1>
                    </div><!--/page-header-->
                    <div class="row-fluid">
                        <?php 
                            $list   = HResponse::getAttribute('list');
                        ?>
                        <div class="apps-cat-box position-relative">
                            <?php
                                foreach($list as $model) {
                                    if($model['parent_id'] > 0){
                                        continue;
                                    }
                            ?>
                                <a href="<?php echo HResponse::url('apps/subapps', 'id=' . $model['id']);?>" class="item-box apps-box">
                                    <?php if($model['image_path']) { ?>
                                    <img src="<?php echo HResponse::touri($model['image_path']); ?>" alt="" class="apps-icon pull-left mr-10">
                                    <?php } ?>
                                    <h3 class="title"><?php echo $model['name'];?></h3>
                                    <div class="desc"><?php echo $model['description']; ?></div>
                                </a>
                            <?php } ?>
                            <div class="clearfix"></div>
                        </div><!--/page-header-->
                    </div>
                </div><!--/#page-content-->
			</div><!-- #main-content -->
		</div><!--/.fluid-container#main-container-->
        <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
    </body>
</html>
