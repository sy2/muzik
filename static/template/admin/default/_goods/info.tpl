﻿<?php require_once(HResponse::path() . '/common/header.tpl'); ?>
        <link rel="stylesheet" href="<?php echo HResponse::uri('cdn'); ?>map/css/map.css" />
	</head>
	<body>
        <?php require_once(HResponse::path() . '/common/navmenu.tpl'); ?>
		<div class="container-fluid" id="main-container">
            <?php require_once(HResponse::path() . '/common/sidebar.tpl'); ?>
			<div id="main-content" class="clearfix">
                <?php 
                    $copyRecord     = HResponse::getAttribute('copyRecord'); 
                    $record         = HResponse::getAttribute('record'); 
                ?>   
                    <div id="breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="icon-dashboard"></i> <a href="<?php echo HResponse::url('', '', 'admin'); ?>">后台桌面</a>
                                <span class="divider"><i class="icon-angle-right"></i></span>
                            </li>
                            <li><a href="<?php echo HResponse::url($modelEnName); ?>"><?php echo $modelZhName; ?></a> <span class="divider"><i class="icon-angle-right"></i></span></li>
                            <li class="active"><?php echo $modelZhName; ?><?php HTranslate::_('内容'); ?></li>
                        </ul><!--.breadcrumb-->
                        <div id="nav-search">
                            <span id="time-info">正在加载时钟...</span>
                        </div><!-- #nav-search -->
                    </div><!-- #breadcrumbs -->
                    <div id="page-content" class="clearfix">
                        <div class="page-header position-relative">
                            <h1>
                                <?php
                                    if(!empty($record)) {
                                        HTranslate::_('编辑');
                                        echo ' - ';
                                        echo isset($record['name']) ? $record['name'] : 'ID：' . $record['id'];
                                    } else {
                                        $title  = HResponse::getAttribute('title');
                                        if(!$title) {
                                            HTranslate::_($modelZhName);
                                        } else {
                                            echo $title;
                                        }
                                        echo $copyRecord ? '(' . HTranslate::__('基于') . 'ID：' . $copyRecord['id'] . ')' : '';
                                    }
                                    $catId  = !HRequest::getParameter('cat') ? '' : 'cat=' . HRequest::getParameter('cat');
                                ?>
                            </h1>
                        </div><!--/page-header-->   
                    <div class="row-fluid">
                    <!-- PAGE CONTENT BEGINS HERE -->
                        <form action="<?php echo HResponse::url($modelEnName . '/' . HResponse::getAttribute('nextAction') ); ?>" method="post" enctype="multipart/form-data" id="info-form">
                            <div class="row-fluid">

                                <div class="span9 content-box">
                                <!-- PAGE CONTENT BEGINS HERE -->

                            <div class="tabbable">
                                            <ul class="nav nav-tabs" id="myTab">
                                                <li class="active">
                                                    <a data-toggle="tab" href="#home">
                                                        <i class="green icon-home bigger-110"></i>
                                                        信息
                                                    </a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div id="home" class="tab-pane in active">
                                    <?php $field = 'id'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                    <?php $record = !$record ? $copyRecord : $record; ?>
                                    <?php $field = 'name'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                    <div class="row-fluid">
                                        <div class="span6">
                                        <?php $field = 'image_path'; require(HResponse::path() . '/fields/image.tpl'); ?></div>
                                        <div class="span6">
                                        <?php $field = 'adv_img'; require(HResponse::path() . '/fields/image.tpl'); ?></div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span3">
                                            <?php $field = 'loupan'; require(HResponse::path() . '/fields/text.tpl'); ?></div>
                                        <div class="span3">
                                            <?php $field = 'max_price'; require(HResponse::path() . '/fields/date.tpl'); ?></div>
                                        <div class="span3">
                                            <?php $field = 'min_vip_price'; require(HResponse::path() . '/fields/text.tpl'); ?></div>
                                        <div class="span3">
                                            <?php $field = 'max_vip_price'; require(HResponse::path() . '/fields/text.tpl'); ?></div>
                                    </div>

                                    <div class="row-fluid">
                                        <div class="span3">
                                            <?php $field = 'mk_price'; require(HResponse::path() . '/fields/text.tpl'); ?></div>
                                        <div class="span3">
<?php $field = 'attr_data'; require(HResponse::path() . '/fields/text.tpl'); ?></div>
                                        <div class="span3">
<?php $field = 'attr_cat_data'; require(HResponse::path() . '/fields/text.tpl'); ?>
<?php $field = 'is_must'; require(HResponse::path() . '/fields/hidden.tpl'); ?></div>
                                        <div class="span3">
<?php $field = 'is_yushou'; require(HResponse::path() . '/fields/text.tpl'); ?></div>
                                    </div>
                                    <div class="row-fluid">
                                        <span class="span4">
<?php $field = 'is_discount'; require(HResponse::path() . '/fields/text.tpl'); ?>
</span>
              <span class="span4">
<?php $field = 'ext_1'; require(HResponse::path() . '/fields/text.tpl'); ?>
</span>
  <span class="span4">
<?php $field = 'tags'; require(HResponse::path() . '/fields/text.tpl'); ?>
</span>
</div>
<?php $field = 'comment_words'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
<?php $field = 'com_address'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
<div class="row-fluid">
     <div style="margin-bottom: 21px;">
     <a href="javascript:void(0);" class="btn btn-sm btn-info" id="btn-add-new-category">
     <i class="icon icon-plus"></i>添加新户型</a></div>
     <table class="table-sku-list table table-striped table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th width="10%">户型名称</th>
                                                            <th width="10%">户型类型</th>
                                                            <th width="20%">状态</th>
                                                            <th width="30%">图片</th>
                                                            <th width="30%">操作</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="sku-list">
                                                    <?php
                                                        $skuMap         = HResponse::getAttribute('skuMap');
                                                        $skuGoodsList   = HResponse::getAttribute('skuGoodsList');
                                                        foreach($skuGoodsList as $key => $item) {
                                                            $sku        = $skuMap[$item['sku_id']];
                                                    ?>
                                                    <tr class="tr-item tr-id-<?php echo $key;?>" data-id="<?php echo $key;?>">
                                                        <td>
                                                            <input type="text" class="sku-name" name="sku_name[]" value="<?php echo $item['name'];?>" id="sku-name-<?php echo $key;?>" />
                                                        </td>
                                                        <td>
                                                            <input type="text" class="sku-type" name="sku_type[]" value="<?php echo $item['type'];?>" id="sku-type-<?php echo $key; ?>" />
                                                        </td>
                                                        <td>
                                                            <input type="text" name="sku_status[]" class="sku-status" value="<?php echo $item['status'];?>" id="sku-status-<?php echo $key;?>" />
                                                        </td>
                                                        <td>
                                                            <div class="old-file-box">
                                                                <a  target="_blank" href="<?php echo HResponse::touri($item['image_path']); ?>" class="lightbox" id="sku-img-<?php echo $key;?>-lightbox">
                                                                    <?php if($item['image_path']) { ?>
                                                                    <img src="<?php echo HResponse::touri(HFile::getImageZoomTypePath($item['image_path'], 'small')); ?>" />
                                                                    <?php }?>
                                                               </a>
                                                            </div>
                                                            <button style="width: 94%;text-align: center;" type="button"
                                                                data-field="image_path"
                                                                data-type="image"
                                                                data-target="sku-img-<?php echo $key;?>"
                                                                class="btn btn-info btn-mini span12 btn-file btn-sku-img"
                                                            ><i class="icon-file"></i></button>
                                                            <input type="hidden" name="sku_image_path[]" id="sku-img-<?php echo $key;?>" value="<?php echo $item['image_path'];?>" />
                                                            <script type="text/javascript">
                                                                <?php $timestamp    = time() . rand(1000, 9999); ?>
                                                                formData['sku-img-<?php echo $key;?>']   = {
                                                                    'timestamp' : '<?php echo $timestamp; ?>',
                                                                    'token'     : '<?php echo md5($timestamp);?>',
                                                                    'model'     : 'goods',
                                                                    'field'     : 'image_path',
                                                                    'nolinked'  : 1
                                                                };
                                                            </script>
                                                        </td>
                                                        <td>
                                                            <span style="width:80%;" class="btn btn-info btn-del-item" data-id="<?php echo $key; ?>">删除</span>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
 </div>
<?php $field = 'content'; require(HResponse::path() . '/fields/editor.tpl'); ?>
<?php $field = 'hash'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                </div>

                                                <div id="orderpay" class="tab-pane">
                                                    <div id="ws-box">
    <?php $field = 'ws_address'; require(HResponse::path() . '/fields/textarea.tpl'); ?>
    <?php $field = 'ws_time'; require(HResponse::path() . '/fields/text.tpl'); ?>
    <?php $field = 'ws_min_price'; require(HResponse::path() . '/fields/text.tpl'); ?>
    <?php $field = 'ws_price'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span4">
    <?php $field = 'alipay_account'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                        </div>
                                                        <div class="span4">
    <?php $field = 'weixin_account'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                        </div>
                                                        <div class="span4">
    <?php $field = 'is_need_autopay'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                        </div>
                                                    </div>
<?php $field = 'bank_account'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                </div>
                                            </div>
                                        </div>

                                    
                                    
                                <!-- PAGE CONTENT ENDS HERE -->

                                </div>
                                <div class="span3">
                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4>发布</h4>
                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <?php $field = 'status'; require(HResponse::path() . '/fields/select.tpl'); ?>
                                                <?php $field = 'is_pintuan'; require(HResponse::path() . '/fields/checkbox.tpl'); ?>
                                                <?php require_once(HResponse::path() . '/common/info-buttons.tpl'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4>所在位置</h4>
                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
                                            <?php $field = 'area'; require(HResponse::path() . '/fields/text.tpl'); ?>
                                            <?php $field = 'position'; require(HResponse::path() . '/fields/text-readonly.tpl'); ?>
                                            <?php require_once(HResponse::path() . '/common/info-buttons.tpl');  ?>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4>维护</h4>
                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
<?php $field = 'total_money'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
<?php $field = 'brand_id'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
<?php $field = 'parent_id'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
<?php $field = 'tidian'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
<?php $field = 'rate'; require(HResponse::path() . '/fields/hidden.tpl'); ?>
                                                <?php $field = 'create_time'; require(HResponse::path() . '/fields/datetime.tpl'); ?>
                                                <?php require_once(HResponse::path() . '/fields/author.tpl'); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                             </div><!--/row-->
                           
                          </div>
                         </form>
                    <!-- PAGE CONTENT ENDS HERE -->
                     </div><!--/row-->
                </div><!--/#page-content-->
			</div><!-- #main-content -->
		</div><!--/.fluid-container#main-container-->
        <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
        <script type="text/javascript"> var fromId = '<?php echo HRequest::getParameter('fid');?>';</script>
        <div id="drag">
            <div class="title">
                <h2>1.选择位置>2.标注位置>3.保存位置</h2>
                <div>
                    <a class="max" href="javascript:;" title="最大化"></a>
                    <a class="revert" href="javascript:;" title="还原"></a>
                    <a class="close" href="javascript:;" title="关闭"></a>
                </div>
            </div>
            <div class="content">
                <div id="f_container">
                    <div id="container"></div>
                </div>
                <div id="allmap" style="width:790px;height:500px;"></div>
                <div style="margin-top: 15px;text-align: right">
                    <input type="hidden" id="lat">
                    <input type="hidden" id="lng">
                    <input type="button" id="btn-save-loc" class="btn btn-info" value="保存">
                    <input type="button" id="btn-hide-map" class="btn btn-default" value="取消">
                </div>
            </div>
        </div>
         <script id="new-item-tpl" type="text/template">
            <tr class="tr-item tr-id-{key}" data-id="{key}">
            <td>
                <input type="text" class="sku-name" name="sku_name[]" value="" id="sku-name-{key}" />
            </td>
            <td>
                <input type="text" class="sku-type" name="sku_type[]" value="" id="sku-type-{key}" />
            </td>
            <td>
                <input type="text" name="sku_status[]" class="sku-status" value="" id="sku-status-{key}" />
            </td>
            <td>
                <div class="old-file-box">
                    <a  target="_blank" href="" class="lightbox" id="sku-img-{key}-lightbox">
                   </a>
                </div>
                <button style="width: 94%;text-align: center;" type="button"
                    data-field="image_path"
                    data-type="image"
                    data-target="sku-img-{key}"
                    class="btn btn-info btn-mini span12 btn-file btn-sku-img"
                ><i class="icon-file"></i></button>
                <input type="hidden" name="sku_image_path[]" id="sku-img-{key}" value="" />
                
            </td>
            <td>
                <span style="width:80%;" class="btn btn-info btn-del-item" data-id="{key}">删除</span>
            </td>
            <script type="text/javascript">
                    <?php $timestamp    = time() . rand(1000, 9999); ?>
                    formData['sku-img-{key}']   = {
                        'timestamp' : '<?php echo $timestamp; ?>',
                        'token'     : '<?php echo md5($timestamp);?>',
                        'model'     : 'goods',
                        'field'     : 'image_path',
                        'nolinked'  : 1
                    };
        </script> 
        </tr>
        </script>   
        <script type="text/javascript" src="https://api.map.baidu.com/api?v=2.0&ak=91E91E12661225fcd55d760997770ded"></script>
        <script src="https://api.map.baidu.com/library/MarkerTool/1.2/src/MarkerTool_min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>map/baidu/CityList_min.js"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>map/tuodong.js"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('cdn'); ?>map/map.js"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri('admin'); ?>js/info.js?v=<?php echo $v;?>"></script>
        <script type="text/javascript" src="<?php echo HResponse::uri(); ?>js/shop-info.js?v=<?php echo $v;?>"></script>
	</body>
</html>
