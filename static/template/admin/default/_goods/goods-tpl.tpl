
        <!-- Modal -->
        <div id="myModal" class="modal hide fade search-modal-box" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel"></h3>
            </div>
            <div class="modal-body" id="modal-body"></div>
            <div class="modal-footer" id="modal-footer">
                <strong class="pull-left">页数：<span id="cur-page-info">0 / 0</span></strong>
                <a href="javascript:void(0);" id="btn-search-pre-page" disabled data-page="0" class="btn-search-pre-page btn btn-sm btn-info"><i class="icon icon-arrow-left"></i> 上一页</a>
                <a href="javascript:void(0);" data-page="0" disabled id="btn-search-next-page" class="btn-search-next-page btn btn-sm btn-info"><i class="icon icon-arrow-right"></i> 下一页</a>
                <a href="javascript:void(0);" class="btn-add-attr-batch btn btn-sm btn-info"><i class="icon icon-plus"></i> 批量添加</a>
                <button class="btn btn-sm" data-dismiss="modal" aria-hidden="true">关闭</button>
            </div>
        </div>
        <?php require_once(HResponse::path() . '/common/footer.tpl'); ?>
        <script type="javascript/template" id="add-attribute-tpl">
            <div id="search-attr-box" class="search-box">
                <form id="attr-search-form" class="form-inline">
                    <div class="row-fluid">
                        <div class="span2">
                            <strong>筛选条件：</strong>
                        </div>
                        <div class="span4">
                            <input type="text" class="input-small span12" placeholder="请输入关键词..." id="attr-keyword" />
                        </div>
                        <div class="span4">
                          <select id="category"  class="span12">
                              <option value="">请选择分类</option>
                              <?php foreach(HResponse::getAttribute('attrCatList') as $item) { ?>
                              <option value="<?php echo $item['id'];?>"><?php echo $item['name'];?></option>
                              <?php } ?>
                          </select>
                        </div>
                        <div class="span2 text-right">
                            <button type="submit" class="btn btn-mini btn-info pull-right">
                                <i class="icon icon-search"></i> 开始搜索
                            </button>
                        </div>
                    </div>
                    <hr>
                </form>
            </div>
            <div class="attr-table-bo