var system = {
	win: false,
	mac: false,
	xll: false,
	ipad:false
};
var p = navigator.platform;
system.win = p.indexOf("Win") == 0;
system.mac = p.indexOf("Mac") == 0;
system.x11 = (p == "X11") || (p.indexOf("Linux") == 0);
system.ipad = (navigator.userAgent.match(/iPad/i) != null)?true:false;

if (system.win || system.mac || system.xll) {
} else {
	$(function() {
		$("#mobile_nav .menu").click(function () {
			if ($("#mobile_nav .menu ul").css("display") == "none") {
				$("#mobile_nav .menu ul").css("display", "block");
			}
			else {
				$("#mobile_nav .menu ul").css("display", "none");
			}
		});
	});
}

function showMobilePic(img) {
	if (system.win || system.mac || system.xll) {
 		document.write('<img src="'+img+'" width="100%" style="display:block" data-bd-imgshare-binded="1"/>');
	} else if (system.ipad){
		document.write();
		var arr = img.replace(".jpg","_p.jpg");
		document.write('<img src="'+arr+'" width="100%" style="display:block" data-bd-imgshare-binded="1"/>');
	} else {
		var arr = img.replace(".jpg","_m.jpg");
		document.write('<img src="'+arr+'" width="100%" style="display:block" data-bd-imgshare-binded="1"/>');
	}
}
function showMobileCode() {
	
}
var page = 1;
function showNewsPage(id) {
	var data = "page="+(page+1)+"&id="+id;
	var obj = function(msg) {
		var result = eval(msg);
		if (result.result) {
			var list = result.list;
			var strTemp = "";
			for (var i=0; i<list.length; i++) {
				strTemp += "<div class='item clearfix";
				strTemp += "'>";
				strTemp += "<div class='time'>";
				strTemp += "<p class='p1'>"+list[i].m+"</p>";
				strTemp += "<p class='p2'>"+list[i].y+"</p>";
				strTemp += "</div>";
				strTemp += "<div class='list'><h4><a href='"+list[i].link+"' >"+unescape(list[i].title)+"</a></h4>";
				strTemp += "<p>"+unescape(list[i].intro)+"</p>";
				strTemp += "</div></div>";
			}
			//var old = $("#cMoreList").html();
			//$("#cMoreList").html(old+strTemp);
			$("#cMoreList").append(strTemp);
			strTemp = null;
			page = page + 1;
		}
	}
	doPost("catchNewsPage",data,obj);
}
function showProductPage(id) {
	var data = "page="+(page+1)+"&id="+id;
	var obj = function(msg) {
		var result = eval(msg);
		if (result.result) {
			var list = result.list;
			var strTemp = "";
			for (var i=0; i<list.length; i++) {
				strTemp += "<div class='p2 nofirst"+(i+1)+"'>";
				strTemp += "<a href='"+list[i].link+"'><figure><div class='item'>";
				strTemp += "<img src='"+list[i].img+"' width='100%' style='display:block' />";
				strTemp += "<div class='info-mask'></div></div>";
				strTemp += "<figcaption>"+unescape(list[i].title)+"</figcaption></figure></a></div>";
			}
			//var old = $("#cMoreList").html();
			//$("#cMoreList").html(old+strTemp);
			$("#cMoreList").append(strTemp);
			strTemp = null;
			page = page + 1;
		}
	}
	doPost("catchProductPage",data,obj);
}
function showSearchPage(id) {
	var data = "page="+(page+1)+"&keyword="+id;
	var obj = function(msg) {
		var result = eval(msg);
		if (result.result) {
			var list = result.list;
			var strTemp = "";
			for (var i=0; i<list.length; i++) {
				strTemp += "<div class='p2 nofirst"+(i+1)+"'>";
				strTemp += "<a href='"+list[i].link+"' target='_blank'><figure>";
				strTemp += "<img src='"+list[i].img+"' width='100%' alt='' />";
				strTemp += "<figcaption>"+unescape(list[i].title)+"</figcaption></figure></a></div>";
			}
			//var old = $("#cMoreList").html();
			//$("#cMoreList").html(old+strTemp);
			$("#cMoreList").append(strTemp);
			strTemp = null;
			page = page + 1;
		}
	}
	doPost("catchSearchPage",data,obj);
}
function showHonorPage(id) {
	var data = "page="+(page+1)+"&id="+id;
	var obj = function(msg) {
		var result = eval(msg);
		if (result.result) {
			var list = result.list;
			var strTemp = "";
			for (var i=0; i<list.length; i++) {
				strTemp += "<a href='"+list[i].link+"'>";
				strTemp += "<div class='m_series_p3 nohonor"+(i+1)+"'><figure><p>";
				strTemp += "<img src='"+list[i].img+"' width='100%' style='display:block' /></p>";
				strTemp += "<figcaption>"+unescape(list[i].title)+"</figcaption></figure></div></a>";
			}
			//var old = $("#cMoreList").html();
			//$("#cMoreList").html(old+strTemp);
			$("#cMoreList").append(strTemp);
			strTemp = null;
			page = page + 1;
		}
	}
	doPost("catchHonorPage",data,obj);
}
function doPost(Action,data,obj) {
	$.ajax({
	   dataType:"json",
	   type:"POST",
	   data:"",
	   url:"../inc/ajax.php?Action="+Action+"&"+data+"&Time=" + (new Date().getTime()),
	   error: function() {
			alert("服务器出现异常，请联系管理员!");
	   },
	   success: obj
	});
}