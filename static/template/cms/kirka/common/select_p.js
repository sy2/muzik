$(function(){
    $(".close").click(function(){
        $("#faqbg").css("display","none");
        $("#faqdiv").css("display","none");
    });
    var selectList = {
        tabSelect: function() {
            $('.tab-menu li').on('click', function() {
                var str = $(this).html().toLowerCase();
                $('.tab-content div').hide();
                $('.box' + str).show();
                $('.tab-menu li').removeClass('active');
                $(this).addClass('active');
            });
        },
        tabColor: function() {
            $('.color_tag label').on('mouseover', function() {
                $('#imgqh' + $(this).attr('data-id') + ' img').attr('src', $(this).attr('data-image'));
            });
            $('.color_tag label').on('mouseout', function() {
                var $dataimg=$(this).parent().children("label[class='f_tag']").attr('data-image');
                $('#imgqh' + $(this).attr('data-id') + ' img').attr('src',$dataimg);
            });
        },
        btnLike: function() {
            $('.likes').on('click', function() {
                var dataId = $(this).find('a').attr('data-id');
                var onlike = $(this).hasClass('share_tag2');
                if (onlike) {
                    car.splice($.inArray(dataId, car), 1);
                } else {
                    car.push(dataId);
                }
                $.cookie('Cart', car, {expires: 3600,path: '/'});
                $(".topNavtag a").html(car.length);
                if (onlike) {
                    $(this).removeClass('share_tag2').addClass('share_tag');
                } else {
                    $(this).removeClass('share_tag').addClass('share_tag2');
                }

            });
        },
        shopDetail: function() {
            $(".click_pop").click(function(){
                var dataId = $(this).attr('data-id');
                if (!dataId) {
                    return false;
                }
                var url = $(this).attr('data-url');
                $.ajax({
                    type: 'post',
                    url : url,
                    dataType: "json",
                    data: {id: dataId},
                    success: function (data) {
                        data = data.data;
                        $("#faqbg").css({display:"block",height:$(document).height()+200});
                        var yscroll = document.documentElement.scrollTop;
                        $("#faqdiv").css("top", "0px");
                        $("#faqdiv").css("display", "block");
                        document.documentElement.scrollTop = yscroll;
                        $('.main-image').attr('src',  '/' + data.image_path);
                        $('.picdowbox').html("<img src=\"/static/template/cms/kirka/images/boxtag.png\" onclick=\"pdfinit('"+data.name+"')\"/>");
                        $('.paraTitle').html(data.name);
                        $('.paraclass').html(data.class_name);
                        $('.onesharp').html(data.kuan);
                        $('.towsharp').html(data.zhai);
                        $('.shrsharp').html(data.height);
                        $('.foursharp').html(data.length);
                        htmlData = '';
                        $.each(data.images, function (key, val) {
                            htmlData += '<li class="picfirst' + (key + 1) + '">'
                                + '<div>'
                                + '<a href="#"><img src="' + '/' +  val.image_path + '" title="' + val.brand + '" /></a>'
                                + '<span>' + (val.brand == undefined ? '' : val.brand ) + '</span>'
                                + '</div>'
                                + '</li>'
                        });
                        $('.other_list ul').html(htmlData);
                    }
                });
            });
        },
        checkOption: function() {
            var self = this;
            $('input[type=checkbox]').on('click', function() {
                self.doSearch();
            });
        },
        doSearch: function(page) {
            var param = 'is_new=' + newid + '&keyword=' + keyword + '&color=' + color + '&rim=' + rim
                + '&shape=' + shape + '&material=' + mater + '&page=' + page + '&parent_id=' + cate + '&cart=' + cart;
            var self = this;
            $('.nm_pro_list').load(queryUrl + 'goods/info?' + param, function() {
                self.btnLike();
                self.shopDetail();
                self.btnPrev();
                self.btnNext();
                self.btnPageNumber();
                self.tabColor();
            });
        },
        btnPrev: function() {
            var self = this;
            $('.previous').on('click', function() {
                page = $('.current').html();
                if (page != 1) {
                    page = parseInt(page) - 1;
                }
                self.doSearch(page);
            });
        },
        btnNext: function() {
            var self = this;
            $('.next').on('click', function() {
                page = $('.current').html();
                page = parseInt(page) + 1;
                if (page > $(this).attr('data-page')) {
                    page = $(this).attr('data-page');
                }
                self.doSearch(page);
            });
        },
        btnPageNumber: function() {
            var self = this;
            $('.pageNumber').on('click', function() {
                page = $(this).html();
                self.doSearch(page);
            });
        },
        showQuestionBox: function() {
            $(".click_pop2").click(function(){
                $("#faqbg2").css({display:"block",height:$(document).height()});
                var yscroll =document.documentElement.scrollTop;
                $("#faqdiv2").css("top","50px");
                $("#faqdiv2").css("display","block");
                document.documentElement.scrollTop=0;
            });
            $(".close").click(function(){
                $("#faqbg2").css("display","none");
                $("#faqdiv2").css("display","none");
            });
        },
        postQuestion: function() {

        }
    }
    selectList.tabSelect();
    selectList.checkOption();
    var page = 1;
    selectList.doSearch(page);
    selectList.showQuestionBox();
})


