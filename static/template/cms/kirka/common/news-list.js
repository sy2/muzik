var newList = {
    btnPrev: function() {
        $('.previous').on('click', function() {
            page = $('.current').html();
            if (page != 1) {
                page = parseInt(page) - 1;
            }
            window.location.href = queryUrl + 'article?parent_id=' + parentId + '&page=' + page;
        });
    },
    btnNext: function() {
        $('.next').on('click', function() {
            page = $('.current').html();
            page = parseInt(page) + 1;
            if (page > $(this).attr('data-page')) {
                page = $(this).attr('data-page');
            }
            window.location.href = queryUrl + 'article?parent_id=' + parentId + '&page=' + page;
        });
    },
}

newList.btnPrev();
newList.btnNext();