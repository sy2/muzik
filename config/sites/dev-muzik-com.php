<?php 
return array (
  'STATIC_URL' => 'http://dev.muzik.com/',
  'CDN_URL'    => 'http://dev.muzik.com/vendor/',
  'SALT'       => 'a2f17d728b3e44fc9c01d3e79eafe23f',
  'TIME_ZONE'  => 'Asia/Shanghai',
  'RES_DIR'    => 'static/uploadfiles/'
); ?>
