<?php
  return array (
  'DEF_APP'     => 'cms',
  'OPEN_SHORT_URL' => false,
  'MESSAGE_THEME' => 'mobile',
  'DATABASE' =>
  array (
    'tablePrefix'    => 'hjz_',
    'dbHost'         => 'localhost',
    'dbPort'         => '3306',
    'dbType'         => 'mysql',
    'dbDriver'       => 'PDO',
    'dbCharset'      => 'utf8',
    'dbName'         => 'muzik',
    'dbUserName'     => 'root',
    'dbUserPassword' => 'root',
  ),
  'PM_TIME' => '18:00',
  'MAIL' =>
  array (
    'charset' => 'UTF-8',
    'ssl' => '',
    'mailMethod' => 'smtp',
    'mailHost' => 'mail.hongjuzi.com.cn',
    'mailPort' => '25',
    'mailUserName' => 'do-not-reply',
    'mailUserPasswd' => 'eWMvZHqRaLfJDBu2',
    'mailFromEmail' => 'do-not-reply@hongjuzi.com.cn',
    'mailFromName' => '派兵点匠客服团队',
    'mailReplyEmail' => 'service@hongjuzi.com.cn',
    'mailReplyName' => 'service@hongjuzi.com.cn'
  ),
  'WECHAT' => array(
      'name' => '雅姿礼品',
      'appid' => 'wx120173e273fda177',
      'secret' => '6fbf72a74599a385fbe220bb7d360146',
      'key' => '',
      'token' => ''
  ),
  'WEIXIN_PAY' => array(
      'appid' => '',
      'mchid' => '',
      'key' => '',
      'secret' => ''
  ),
  'XIAOCHENGXU' => array(
      'appid' => 'wxd3ac876b84d84c7d',
      'secret' => '0353fbd56fe7d73689a8bd4f878191c7'
  ),
  'QQ' => array(
    'appid' => '',
    'key' => '',
    'server_name' => ''//'119.147.19.43'
  ),
  'SMS' => array(
    'appid' => '',
    'secret' => '',
    'sign_name' => ''
  ),
  'OPEN_RESOURCE_HOSTING' => false,
  'QINIU' => array(
      'access_key' => '',
      'secret_key' => '',
      'domain' => ''
  ),
  'IMAGE_URL' => 'http://orxr6bqam.bkt.clouddn.com/',
  'STATIC_URL' => 'http://m.huaya.net/',
  'CDN_URL' => 'http://m.huaya.net/vendor/',
  'CUR_THEME' => 'ace',
  'YUN_FEE' => '8',
  'IS_ONLINE' => false, //是否上线
  'ADMIN_EMAIL' => 'xjiujiu@foxmail.com',
  'COOKIE_ENCODE' => 'SFSDF*)&FSDF',
  'TIME_ZONE' => 'Asia/Shanghai',
  'PAGE_STYLE' => 'bootstrap',
  'PAGES_NUMBER' => 5, //显示5页
  'URL_MODE' => 'pathinfo',
  'VENDOR_DIR' => 'vendor',
  'RUNTIME_DIR' => 'runtime',
  'TPL_DIR' => 'static/template',
  'RES_DIR' => 'static/uploadfiles',
  'CONFIG_TPL' => 'config/config.tpl',
  'LOG' =>
  array (
    'dir' => 'runtime/log',
    'size' => 50,
    'method' =>
    array (
      'file' => 'error, wran',
    ),
    'tpl' => 'public/template/common/email-log.tpl',
  ),
  'RSS_TTL' => '120',
  'SYS_NAME' => 'Wooc-红橘子科技',
  'SYS_VERSION' => '1.0.0',
  'STATIC_VERSION' => '20131213',
  'DEBUG' => false,
); ?>

