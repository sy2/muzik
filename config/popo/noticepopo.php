<?php 

/**
 * @version			$Id$
 * @create 			2015-11-22 14:11:04 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class NoticePopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '消息';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'notice';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = 'user';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_notice';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';

    /**
     * @var public static $statusMap      状态类别
     */
    public static $statusMap      = array(
        '1' => array('id' => 1, 'name' => '未读'),
        '2' => array('id' => 2, 'name' => '已读'),
    );

    public $dataGridType = 4;
    /**
     * @var public static $typeMap      消息类别
     */
    public static $typeMap      = array(
        '1' => array('id' => 1, 'name' => '系统消息'),
        '2' => array('id' => 2, 'name' => '订单通知'),
        '3' => array('id' => 3, 'name' => '站内私信'),
        '4' => array('id' => 4, 'name' => '提醒外送'),
        '5' => array('id' => 5, 'name' => '提醒退款'),
        '6' => array('id' => 6, 'name' => '提醒备菜'),
        '7' => array('id' => 7, 'name' => '加菜'),
        '8' => array('id' => 8, 'name' => '完成付款')
    );

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('id' => array(
            'name' => 'ID', 'is_order' => 'DESC', 
            'verify' => array(),
            'comment' => '系统编号','is_show' => true, 
        ),'shop_id' => array(
            'name' => '主题', 'default' => '0',
            'verify' => array( 'numeric' => true,),
            'comment' => '主题','is_show' => true,
        ),'parent_id' => array(
            'name' => '用户编号', 'default' => 0,
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '用户编号','is_show' => false,
        ),'email' => array(
            'name' => '邮箱', 'default' => 0,
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '邮箱','is_show' => true,
        ),'content' => array(
            'name' => '内容', 
            'verify' => array('null' => false,), 'is_search' => true,
            'comment' => '消息内容','is_show' => true, 
        ),'type' => array(
            'name' => '类型', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '1系统,2私信,消息类型','is_show' => false, 
        ),'status' => array(
            'name' => '状态', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '消息所属的状态','is_show' => false, 
        ),'user_name' => array(
            'name' => '名字', 'default' => '无',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '名字','is_show' => true,
        ),'from_id' => array(
            'name' => '来源用户', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '来源用户编号','is_show' => false, 
        ),'hash' => array(
            'name' => '签名', 'default' => '0',
            'verify' => array('null' => true),
            'comment' => '签名重复性检测','is_show' => false, 
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10 08:09:09', 'is_show' => true
        ),'author' => array(
            'name' => '维护员', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '上一次修改的管理员','is_show' => false, 
        ),);

}

?>
