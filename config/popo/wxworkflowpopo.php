<?php 

/**
 * @version			$Id$
 * @create 			2015-10-24 16:10:38 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class WxworkflowPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '微信事务';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'wxworkflow';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = 'user';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_wx_workflow';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';

    /**
     * @var public static $statusMap    文章状态映射
     */
    public static $statusMap    = array(
        '1' => array('id' => '1', 'name' => '录入'),
        '2' => array('id' => '2', 'name' => '发布'),
        '3' => array('id' => '3', 'name' => '取消'),
    );

    /**
     * @var public static $statusMap    文章状态映射
     */
    public static $typeMap    = array(
        '1' => array('id' => '1', 'name' => '发布帮助'),
        '2' => array('id' => '2', 'name' => '发布工作内推'),
        '3' => array('id' => '3', 'name' => '发布项目合作'),
        '4' => array('id' => '4', 'name' => '发布项目外包'),
        '5' => array('id' => '5', 'name' => '发布活动聚会')
    );

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('id' => array(
            'name' => 'ID', 
            'verify' => array(),
            'comment' => '只能是数字','is_show' => true, 'is_order' => 'DESC', 
        ),'openid' => array(
            'name' => '编号', 
            'verify' => array('null' => false,),
            'comment' => '第三方编号OPENID','is_show' => true, 
        ),'parent_id' => array(
            'name' => '所属用户', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '请正确选取','is_show' => true, 
        ),'type' => array(
            'name' => '事务类型', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '消息处理事务类型','is_show' => true, 
        ),'step' => array(
            'name' => '步骤', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '执行到的步骤','is_show' => true, 
        ),'end_time' => array(
            'name' => '有效时间', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '使用截止时间','is_show' => true, 
        ),'status' => array(
            'name' => '状态', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '处事处理的状态','is_show' => true, 
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10','is_show' => true, 
        ),'author' => array(
            'name' => '维护人', 'default' => '-1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '最后一次修改人员','is_show' => true, 
        ),);

}

?>
