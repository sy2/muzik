<?php 

/**
 * @version			$Id$
 * @create 			2016-09-10 14:09:19 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class SkugoodsPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = 'sku与商品关联表';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'skugoods';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = 'goods';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_sku_goods';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('sort_num' => array(
            'name' => '排序', 
            'verify' => array(),
            'comment' => '只能是数字，默认为：当前时间。','is_show' => true, 'is_order' => 'ASC', 
        ),'id' => array(
            'name' => 'ID', 
            'verify' => array(),
            'comment' => '只能是数字','is_show' => true, 'is_order' => 'ASC', 
        ),'sku_id' => array(
            'name' => 'sku', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => 'sku','is_show' => true, 
        ),'price' => array(
            'name' => '价格', 'default' => '0.00',
            'verify' => array('null' => false,),
            'comment' => '价格','is_show' => true, 
        ),'cheap_price' => array(
            'name' => '折扣价', 'default' => '0.00',
            'verify' => array('null' => false,),
            'comment' => '折扣价','is_show' => true, 
        ),'miaosha_price' => array(
            'name' => '秒杀价', 'default' => '0.00',
            'verify' => array('null' => false,),
            'comment' => '秒杀价格','is_show' => true, 
        ),'yusho_price' => array(
            'name' => '预售价', 'default' => '0.00',
            'verify' => array('null' => false,),
            'comment' => '预售价','is_show' => true,
        ),'tuan_price' => array(
            'name' => '拼团价', 'default' => '0.00',
            'verify' => array('null' => false,),
            'comment' => '商品在拼团活动上的价格','is_show' => true, 
        ),'choujiang_price' => array(
            'name' => '抽奖价', 'default' => '0.00',
            'verify' => array('null' => false,),
            'comment' => '商品在抽奖活动上的价格','is_show' => true, 
        ),'zhongliang' => array(
            'name' => '重量', 'default' => '0.00',
            'verify' => array('null' => false,),
            'comment' => '规格货品对应的重量','is_show' => true, 
        ),'goods_id' => array(
            'name' => '商品id', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '商品id','is_show' => true, 
        ),'total_number' => array(
            'name' => '库存', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '库存数量','is_show' => true,
        ),'total_orders' => array(
            'name' => '销量', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '销量数量','is_show' => true,
        ),'image_path' => array(
            'name' => '对应图片', 
            'verify' => array( 'len' => 255,),
            'comment' => '请选择允许的类型。','is_show' => true, 
            'zoom' => array('small' =>  array(300, 320)), 'type' => array('.png', '.jpg', '.gif'), 'size' => 0.5
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10','is_show' => true, 
        ),'author' => array(
            'name' => '维护人', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '最后一次维护人员','is_show' => true, 
        ),);

}

?>
