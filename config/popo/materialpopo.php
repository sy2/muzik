<?php 

/**
 * @version			$Id$
 * @create 			2015-05-04 22:05:05 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class MaterialPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '材质管理';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'material';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = '';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_material';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';


    public static $statusMap = array(
        1 => array('id' => 1, 'name' => '未启用'),
        2 => array('id' => 2, 'name' => '已启用'),
    );
    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('sort_num' => array(
            'name' => '排序', 'default' => '999',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '显示的前后关系','is_show' => true, 'is_order' => 'ASC', 
        ),'id' => array(
            'name' => 'ID', 
            'verify' => array(),
            'comment' => '只能是数字','is_show' => true, 'is_order' => 'DESC', 
        ),'name' => array(
            'name' => '名称',
            'verify' => array('null' => false, 'len' => 255,),
            'comment' => '名称','is_show' => true,
        ),'content' => array(
            'name' => '材质值',
            'verify' => array('null' => false,),
            'comment' => '材质值,如材质','is_show' => true,
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10',
        ),'status' => array(
            'name' => '状态',
            'verify' => array('null' => false, 'len' => 255,),
            'comment' => '状态','is_show' => true,
        ),'author' => array(
            'name' => '维护人', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '最后一次维护人员',
        ),);
}
?>
