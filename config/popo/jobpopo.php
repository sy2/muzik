<?php 

/**
 * @version			$Id$
 * @create 			2018-06-26 11:06:33 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class JobPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '求职';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'job';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = '';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_job';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';

    public static $statusMap   = array(
        '1' => array('id' => 1, 'name' => '未启用'),
        '2' => array('id' => 2, 'name' => '已启用'),
    );

    public static $tagsMap   = array(
        '1' => array('id' => 1, 'name' => '五险一金'),
        '2' => array('id' => 2, 'name' => '交通补贴'),
        '3' => array('id' => 3, 'name' => '奖金提成'),
    );

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('id' => array(
            'name' => '编号', 
            'verify' => array(),
            'comment' => '自动编号','is_show' => true, 
        ),'name' => array(
            'name' => '标题', 'default' => '0',
            'comment' => '标题','is_show' => true,'is_search'=>true, 
        ),'guimo' => array(
            'name' => '规模', 
            'verify' => array('null' => false, 'len' => 255,),
            'comment' => '规模','is_show' => true, 
        ),'tags' => array(
            'name' => '标签', 
            'verify' => array('null' => false, 'len' => 30,),
            'comment' => '标签,多个标签,号隔开','is_show' => true,
        ),'address' => array(
            'name' => '位置', 'default' => '0',
            'comment' => '位置','is_show' => true, 
        ),'jingyan' => array(
            'name' => '经验', 
            'verify' => array( 'len' => 255,),
            'comment' => '经验','is_show' => true, 
        ),'xueli' => array(
            'name' => '学历', 
            'verify' => array('null' => false,),
            'comment' => '学历','is_show' => true, 
        ),'number' => array(
            'name' => '招聘人数', 'default' => '1',
            'verify' => array('null' => false,),
            'comment' => '招聘人数','is_show' => true, 
        ),'jobzhize' => array(
            'name' => '工作职责', 'default' => '1',
            'verify' => array('null' => false,),
            'comment' => '工作职责','is_show' => false, 
        ),'jobscrib' => array(
            'name' => '工作描述', 'default' => '1',
            'verify' => array('null' => false,),
            'comment' => '工作描述','is_show' => false, 
        ),'yaoqiu' => array(
            'name' => '岗位要求', 'default' => '1',
            'verify' => array('null' => false,),
            'comment' => '岗位要求','is_show' => false, 
        ),'status' => array(
            'name' => '状态', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '任务执行状态：0停止，1开始，2暂停','is_show' => true, 
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10','is_show' => true, 
        ),'author' => array(
            'name' => '维护人', 'default' => '0',
            'verify' => array( 'numeric' => true,),
            'comment' => '最近一次维护人员','is_show' => true, 
        ),);
}

?>
