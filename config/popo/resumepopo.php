<?php 

/**
 * @version			$Id$
 * @create 			2018-06-30 15:06:22 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class ResumePopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '简历';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'resume';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = '';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_resume';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = '';

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('id' => array(
            'name' => 'ID', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '系统编号','is_show' => true, 
        ),'company' => array(
            'name' => '公司名称', 'default' => '0',
            'verify' => array('null' => false, 'len' => 50,),
            'comment' => '公司名称','is_show' => true, 
        ),'starttime' => array(
            'name' => '开始时间', 
            'verify' => array('null' => false,),
            'comment' => '开始时间','is_show' => true, 
        ),'endtime' => array(
            'name' => '結束时间', 
            'verify' => array('null' => false,),
            'comment' => '結束时间','is_show' => true, 
        ),'parent_id' => array(
            'name' => '用戶', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '用戶','is_show' => true, 
        ),'jingli' => array(
            'name' => '经历描述', 
            'verify' => array(),
            'comment' => '经历描述','is_show' => true, 
        ),'extend' => array(
            'name' => '扩展字段', 
            'verify' => array('null' => false,),
            'comment' => '扩展字段','is_show' => true, 
        ),'status' => array(
            'name' => '是否查看', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '是否查看','is_show' => true, 
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10 08:09:09',
        ),'author' => array(
            'name' => '维护人', 'default' => '0',
            'verify' => array( 'numeric' => true,),
            'comment' => '最近一次维护人员','is_show' => true, 
        ),);

}

?>
