<?php 

/**
 * @version			$Id$
 * @create 			2018-10-25 10:10:54 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class YouhuijuanrulePopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '优惠券规则';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'youhuijuanrule';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = '';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_youhuijuan_rule';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = '';

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('id' => array(
            'name' => 'ID', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '只能是数字','is_show' => true, 'is_order' => 'DESC', 
        ),'name' => array(
            'name' => '规则名称', 
            'verify' => array('null' => false, 'len' => 250,),
            'comment' => '请正确填写','is_show' => true, 'is_search' => true, 
        ),'parent_id' => array(
            'name' => '发送规则', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '发送规则','is_show' => true, 'is_search' => true, 
        ),'yhj_id' => array(
            'name' => '优惠券ID', 
            'verify' => array('null' => false,),
            'comment' => '优惠券ID',
        ),'yhj_name' => array(
            'name' => '优惠券名称', 
            'verify' => array('null' => false,),
            'comment' => '优惠券名称','is_show' => true, 
        ),'content' => array(
            'name' => '详情', 
            'verify' => array('null' => false,),
            'comment' => '详情','is_show' => true, 
        ),'days' => array(
            'name' => '有效时间', 'default' => '6',
            'verify' => array( 'len' => 200,),
            'comment' => '有效时间','is_show' => true, 
        ),'shop_id' => array(
            'name' => '供货商', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '供货商编号','is_show' => true, 
        ),'status' => array(
            'name' => '状态', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '1草稿，2正常,3删除,4领完','is_show' => true, 
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10','is_show' => true, 
        ),'author' => array(
            'name' => '维护人', 'default' => '0',
            'verify' => array( 'numeric' => true,),
            'comment' => '最近一次维护人员','is_show' => true, 
        ),);

}

?>
