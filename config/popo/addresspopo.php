<?php 

/**
 * @version			$Id$
 * @create 			2015-10-17 17:10:55 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class AddressPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '地址';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'address';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = 'user';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_address';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('id' => array(
            'name' => 'ID', 
            'verify' => array(),
            'comment' => '只能是数字','is_show' => true, 'is_order' => 'DESC', 
        ),'name' => array(
            'name' => '用户名称', 
            'verify' => array('null' => false, 'len' => 50,),
            'comment' => '长度范围：2~255。','is_show' => true, 'is_search' => true, 
        ),'phone' => array(
            'name' => '联系方式', 
            'verify' => array('null' => false, 'len' => 255,),
            'comment' => '长度范围：2~255。','is_show' => true, 
        ),'parent_id' => array(
            'name' => '用户', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '对应用户','is_show' => true, 
        ),'province' => array(
            'name' => '省', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '所有省编号','is_show' => true, 
        ),'city' => array(
            'name' => '所在市', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '所在市编号','is_show' => true, 
        ),'district' => array(
            'name' => '区', 
            'verify' => array( 'len' => 100,),
            'comment' => '所在区位置名称','is_show' => true, 
        ),'street' => array(
            'name' => '街道', 
            'verify' => array( 'len' => 255,),
            'comment' => '所在街道','is_show' => true, 
        ),'street_no' => array(
            'name' => '街道编号', 
            'verify' => array('null' => false, 'len' => 255,),
            'comment' => '详细街道编号','is_show' => true, 
        ),'detail' => array(
            'name' => '全地址', 
            'verify' => array('null' => false,),
            'comment' => '地址全信息','is_show' => true, 
        ),'peisong_detail' => array(
            'name' => '配送详细地址', 'default' => null,
            'verify' => array('null' => false,),
            'comment' => '配送地址全信息','is_show' => true, 
        ),'loc' => array(
            'name' => '坐标', 
            'verify' => array('null' => false, 'len' => 50,),
            'comment' => '位置坐标','is_show' => true, 
        ),'code' => array(
            'name' => '邮政编号', 'default' => '418000',
            'verify' => array('null' => true, 'len' => 20,),
            'comment' => '邮政编号','is_show' => true, 
        ),'is_default' => array(
            'name' => '默认', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '默认地址','is_show' => true, 
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10','is_show' => true, 
        ),'author' => array(
            'name' => '维护人', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '最后一次维护人员','is_show' => true, 
        ),);

}

?>
