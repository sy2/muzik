<?php 

/**
 * @version			$Id$
 * @create 			2017-09-16 11:09:53 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class WxreplyPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '微信自动回复';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'wxreply';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = 'shop';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_wx_reply';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('id' => array(
            'name' => 'ID', 
            'verify' => array(),
            'comment' => '只能是数字','is_show' => true, 'is_order' => 'DESC', 
        ),'shop_id' => array(
            'name' => '商家', 'default' => '0',
            'verify' => array( 'numeric' => true,),
            'comment' => '对应商家','is_show' => false, 
        ),'name' => array(
            'name' => '关键词', 
            'verify' => array('null' => false, 'len' => 255,),
            'comment' => '多个用逗号（“,”）隔开。','is_show' => true, 'is_search' => true, 
        ),'content' => array(
            'name' => '回复内容', 
            'verify' => array('null' => false,),
            'comment' => '长度10000字以内。',
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10','is_show' => true, 
        ),'author' => array(
            'name' => '维护人', 'default' => '0',
            'verify' => array( 'numeric' => true,),
            'comment' => '最近一次维护人员','is_show' => true, 
        ),);

}

?>
