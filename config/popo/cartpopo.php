<?php 

/**
 * @version			$Id$
 * @create 			2015-10-17 10:10:06 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class CartPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '购物车';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'cart';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = 'user';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_cart';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';

    /**
     * @var public static $statusMap    文章状态映射
     */
    public static $statusMap    = array(
        '1' => array('id' => '1', 'name' => '正常'),
        '2' => array('id' => '2', 'name' => '删除'),
        '3' => array('id' => '3', 'name' => '生成订单'),
        '4' => array('id' => '4', 'name' => '直接购买')
    );

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('id' => array(
            'name' => 'ID', 
            'verify' => array(),
            'comment' => '系统编号','is_show' => true, 
        ),'goods_id' => array(
            'name' => '商品销售', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '订单所属的家教ID','is_show' => true, 
        ),'number' => array(
            'name' => '购买数量', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '只能是数字','is_show' => true, 
        ),'attrs' => array(
            'name' => '商品属性',
            'verify' => array('null' => false),
            'comment' => 'JSON数据','is_show' => true, 
        ),'parent_id' => array(
            'name' => '用户', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '订单所属的用户','is_show' => true, 
        ),'cuxiao_type' => array(
            'name' => '促销类型', 'default' => '2',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '促销类型','is_show' => true,
        ),'group_id' => array(
            'name' => '属性规格组合id',
            'verify' => array('len' => 255,),
            'comment' => '属性规格组合id','is_show' => true,
        ),'group_name' => array(
            'name' => '属性规格组合名称',
            'verify' => array('len' => 255,),
            'comment' => '属性规格组合名称','is_show' => true,
        ),'price' => array(
            'name' => '商品价格', 'price' => 0,
            'verify' => array('len' => 255,),
            'comment' => '商品的价格','is_show' => true,
        ),'image_path' => array(
            'name' => '图片路径',
            'verify' => array('len' => 255,),
            'comment' => '商品图片路径','is_show' => true,
        ),'status' => array(
            'name' => '状态', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '1表示在正常，2表示删除，3表示生成到订单','is_show' => true, 
        ),'end_time' => array(
            'name' => '过期时间', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '商品过期时间','is_show' => true, 
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10 08:09:09',
        ),'author' => array(
            'name' => '维护员', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '上一次修改的管理员','is_show' => true, 
        ),);

}

?>
