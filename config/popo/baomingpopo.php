<?php 

/**
 * @version			$Id$
 * @create 			2018-07-09 20:07:42 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class BaomingPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '楼盘报名';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'baoming';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = '';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_baoming';
    
    public static $statusMap = array(
       1  => array('id' => 1, 'name' => '推荐未到访'),
       2  => array('id' => 2, 'name' => '已到访'),
       3  => array('id' => 3, 'name' => '已认购'),
       4  => array('id' => 4, 'name' => '已签约'),
       5  => array('id' => 5, 'name' => '已结佣'),
       6  => array('id' => 6, 'name' => '已过期'),
    );
    
    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = '';

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('id' => array(
            'name' => '编号', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '活动编号','is_show' => true, 
        ),'parent_id' => array(
            'name' => '用户', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '用户','is_show' => true, 
        ),'name' => array(
            'name' => '名字', 
            'verify' => array('null' => false, 'len' => 20,),
            'comment' => '名字','is_show' => true, 
        ),'phone' => array(
            'name' => '联系电话', 'default' => '0',
            'verify' => array('null' => false, 'len' => 20,),
            'comment' => '联系电话','is_show' => true, 
        ),'loupan' => array(
            'name' => '户型', 'default' => '0',
            'verify' => array('null' => false, 'len' => 100,),
            'comment' => '户型','is_show' => true, 
        ),'yysj' => array(
            'name' => '预约时间', 
            'verify' => array('null' => false,),
            'comment' => '预约时间','is_show' => true, 
        ),'zygw' => array(
            'name' => '置业顾问', 
            'verify' => array('null' => false, 'len' => 20,),
            'comment' => '置业顾问','is_show' => true, 
        ),'pro' => array(
            'name' => '产品类型', 
            'verify' => array(),
            'comment' => '产品类型','is_show' => true, 
        ),'hx' => array(
            'name' => '户型', 
            'verify' => array(),
            'comment' => '户型','is_show' => true, 
        ),'gzd' => array(
            'name' => '购房关注点', 
            'verify' => array(),
            'comment' => '购房关注点','is_show' => true, 
        ),'status' => array(
            'name' => '状态', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '状态','is_show' => false, 
        ),'room_id' => array(
            'name' => '楼盘', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '楼盘','is_show' => false, 
        ),'create_time' => array(
            'name' => '报名时间',
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10 08:09:09','is_show'=> true,
        ),'author' => array(
            'name' => '维护人', 'default' => '0',
            'verify' => array( 'numeric' => true,),
            'comment' => '最近一次维护人员','is_show' => true, 
        ),);
}
?>
