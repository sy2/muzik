<?php 

/**
 * @version			$Id$
 * @create 			2018-07-10 09:07:42 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class UserinfoPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '用户完善信息';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'userinfo';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = '';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_user_info';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = '';

    public static $typeMap    = array(
        '1' => array('id' =>1 , 'name' => '老用户'),
        '2' => array('id' =>2 , 'name' => '新用户')
    );

    public static $yinghangMap    = array(
        1 => array('id' =>1 , 'name' => '中国建设银行'),
        2 => array('id' =>2 , 'name' => '中国农业银行'),
        3 => array('id' =>3 , 'name' => '中国工商银行'),
        4 => array('id' =>4 , 'name' => '中国银行'),
        5 => array('id' =>5 , 'name' => '中国交通银行'),
    );
    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('id' => array(
            'name' => 'ID', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '系统编号','is_show' => true, 
        ),'parent_id' => array(
            'name' => '所属用户', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '所属用户','is_show' => true, 
        ),'true_name' => array(
            'name' => '姓名', 
            'verify' => array('null' => false, 'len' => 50,),
            'comment' => '姓名','is_show' => true, 'is_search' => true,
        ),'phone' => array(
            'name' => '手机号码', 
            'verify' => array('null' => false, 'len' => 20,),
            'comment' => '总消息数','is_show' => true, 'is_search' => true,
        ),'idcard' => array(
            'name' => '身份证号码', 
            'verify' => array('null' => false, 'len' => 20,),
            'comment' => '身份证号码','is_show' => true, 'is_search'=>true,
        ),'address' => array(
            'name' => '常住地', 
            'verify' => array('null' => false,),
            'comment' => '常住地','is_show' => true, 
        ),'type' => array(
            'name' => '身份类型', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '身份类型','is_show' => true, 
        ),'khh' => array(
            'name' => '开户行', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '开户行','is_show' => true, 
        ),'yhzh' => array(
            'name' => '银行账号', 'default' => '0',
            'verify' => array('null' => false, 'len' => 50,),
            'comment' => '银行账号','is_show' => true, 
        ),'zhm' => array(
            'name' => '账户名', 'default' => '0',
            'verify' => array('null' => false, 'len' => 50,),
            'comment' => '账户名','is_show' => true, 
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10 08:09:09',
        ),'author' => array(
            'name' => '维护人', 'default' => '0',
            'verify' => array( 'numeric' => true,),
            'comment' => '最近一次维护人员','is_show' => true, 
        ),);

}

?>
