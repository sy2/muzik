<?php 

/**
 * @version			$Id$
 * @create 			2016-03-13 15:03:49 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class WxmenuPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '微信菜单';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'wxmenu';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = 'wxmenu';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_wx_menu';

    /**
     * @var string $typeMap 类别映射
     */
    public static $typeMap      = array(
        'click' => array('id' => 'click', 'name' => '点击推事件'),
        'view' => array('id' => 'view', 'name' => '跳转URL'),
        'scancode_push' => array('id' => 'scancode_push', 'name' => '扫码推事件'),
        'scancode_waitmsg' => array('id' => 'scancode_waitmsg', 'name' => '扫码推事件提示框'),
        'pic_sysphoto' => array('id' => 'pic_sysphoto', 'name' => '弹出系统拍照发图'),
        'pic_photo_or_album' => array('id' => 'pic_photo_or_album', 'name' => '弹出拍照或者相册发图'),
        'pic_weixin' => array('id' => 'pic_weixin', 'name' => '弹出微信相册发图器'),
        'location_select' => array('id' => 'location_select', 'name' => '弹出地理位置选择器'),
        'media_id' => array('id' => 'media_id', 'name' => '下发消息（除文本消息）'),
        'view_limited' => array('id' => 'view_limited', 'name' => '跳转图文消息URL')
    );

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('sort_num' => array(
            'name' => '排序', 'default' => '999',
            'verify' => array('null' => false,),
            'comment' => '只能是数字，默认为：当前时间。','is_show' => true, 'is_order' => 'ASC', 
        ),'id' => array(
            'name' => 'ID', 
            'verify' => array(),
            'comment' => '只能是数字','is_show' => true, 'is_order' => 'DESC', 
        ),'shop_id' => array(
            'name' => '商家', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '所属商家ID','is_show' => false, 
        ),'name' => array(
            'name' => '名称', 
            'verify' => array('null' => false, 'len' => 255,),
            'comment' => '长度范围：2~255。','is_show' => true, 'is_search' => true, 
        ),'url' => array(
            'name' => '跳转链接', 
            'verify' => array('null' => false, 'len' => 255,),
            'comment' => '合法的URL发址','is_show' => true, 
        ),'parent_id' => array(
            'name' => '父级菜单', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '请正确选取','is_show' => true, 
        ),'type' => array(
            'name' => '类型', 
            'verify' => array('null' => false),
            'comment' => '菜单类型','is_show' => true, 
        ),'key' => array(
            'name' => '菜单KEY值', 
            'verify' => array( 'len' => 50,),
            'comment' => '用于消息接口推送,128字节,click等点击类型必须',
        ),'media_id' => array(
            'name' => '素材编号', 
            'verify' => array( 'len' => 50,),
            'comment' => '调用新增永久素材接口返回的合法media_id','is_show' => true, 
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10',
        ),'author' => array(
            'name' => '维护员', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '所属会员',
        ),);

}

?>
