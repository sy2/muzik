<?php 

/**
 * @version			$Id$
 * @create 			2015-11-28 10:11:42 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class UserextendPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '用户扩展信息';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'userextend';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = 'user';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_user_extend';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('id' => array(
            'name' => 'ID', 
            'verify' => array(),
            'comment' => '系统编号','is_show' => true, 
        ),'parent_id' => array(
            'name' => '所属用户', 'default' => '-1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '所属用户','is_show' => true, 
        ),'total_pay' => array(
            'name' => '总支出', 'default' => '0.00',
            'verify' => array('null' => false,),
            'comment' => '总支出','is_show' => true, 
        ),'total_notices' => array(
            'name' => '总消息数', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '总消息数','is_show' => true, 
        ),'total_focus' => array(
            'name' => '总店铺关注数', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '总店铺关注数','is_show' => true, 
        ),'total_collect' => array(
            'name' => '总服务收藏数', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '总服务收藏数','is_show' => true, 
        ),'total_visits' => array(
            'name' => '总访问数', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '总访问数','is_show' => true, 
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10 08:09:09',
        ),'author' => array(
            'name' => '维护员', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '上一次修改的管理员','is_show' => true, 
        ),);

}

?>
