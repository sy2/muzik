<?php 

/**
 * @version			$Id$
 * @create 			2017-10-31 15:10:57 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class AccounttotalPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '账号总金额';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'accounttotal';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = '';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_account_total';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';

    public $dataGridType        = 3;

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('id' => array(
            'name' => 'ID', 
            'verify' => array(),
            'comment' => '系统编号','is_show' => true, 
        ),'shop_id' => array(
            'name' => '店铺', 'default' => '0',
            'verify' => array( 'numeric' => true,),
            'comment' => '对应店铺','is_show' => true, 
        ),'money' => array(
            'name' => '充值总金额', 'default' => '0.00',
            'verify' => array(),
            'comment' => '充值总金额','is_show' => true, 'is_search' => true
        ),'amount' => array(
            'name' => '零钱总金额', 'default' => '0.00',
            'verify' => array(),
            'comment' => '零钱总金额','is_show' => true,
        ),'description' => array(
            'name' => '相关描述', 
            'verify' => array(),
            'comment' => '相关描述','is_show' => true, 
        ),'parent_id' => array(
            'name' => '所属用户', 'default' => '0',
            'verify' => array( 'numeric' => true,),
            'comment' => '所属用户','is_show' => true, 
        ),'status' => array(
            'name' => '状态', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '状态',
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10 08:09:09',
        ),'author' => array(
            'name' => '维护人', 'default' => '0',
            'verify' => array( 'numeric' => true,),
            'comment' => '最近一次维护人员','is_show' => false,
        ),);

}

?>
