<?php 

/**
 * @version			$Id$
 * @create 			2018-07-11 00:07:22 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class UsersonPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '用户子用户';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'userson';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = '';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_user_son';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('id' => array(
            'name' => 'ID', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '只能是数字','is_show' => true, 'is_order' => 'DESC', 
        ),'parent_id' => array(
            'name' => '邀请人', 'default' => '0','is_search' => 1,
            'verify' => array( 'numeric' => true,),
            'comment' => '邀请人编号','is_show' => true, 
        ),'son_id' => array(
            'name' => '被邀请人',
            'verify' => array('null' => false, 'len' => 11,),
            'comment' => '被邀请人编号','is_show' => true,
        ),'extend' => array(
            'name' => '备注', 'default' => '备注',
            'comment' => '备注','is_show' => false,
        ),'create_time' => array(
            'name' => '邀请时间',
            'verify' => array('null' => false,),
            'comment' => '创建时间','is_show' => true, 
        ),'author' => array(
            'name' => '维护人', 'default' => '0',
            'verify' => array( 'numeric' => true,),
            'comment' => '最近一次维护人员','is_show' => true, 
        ),);

}

?>
