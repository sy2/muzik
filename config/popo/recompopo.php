<?php 

/**
 * @version			$Id$
 * @create 			2018-07-07 17:07:16 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class RecomPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '推荐客户';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'recom';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = '';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_recom';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = '';
    public static $statusMap    = array(
       1  => array('id' => 1, 'name' => '推荐未到访'),
       2  => array('id' => 2, 'name' => '已到访'),
       3  => array('id' => 3, 'name' => '已认购'),
       4  => array('id' => 4, 'name' => '已签约'),
       5  => array('id' => 5, 'name' => '已结佣'),
       6  => array('id' => 6, 'name' => '已过期'),
    );
    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('sort_num' => array(
            'name' => '次序', 'default' => '999',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '显示的前后关系','is_show' => true, 'is_order' => 'ASC', 
        ),'id' => array(
            'name' => 'ID', 
            'comment' => '只能是数字','is_show' => true, 'is_order' => 'DESC', 
        ),'name' => array(
            'name' => '客户姓名', 
            'verify' => array('null' => false, 'len' => 255,),
            'comment' => '客户姓名','is_show' => true,'is_search' => true
        ),'phone' => array(
            'name' => '联系方式', 
            'verify' => array('null' => false,),
            'comment' => '联系方式','is_show' => true,'is_search' => true
        ),'roomid' => array(
            'name' => '楼盘',
            'comment' => '楼盘','is_show' => true,
        ),'loupan' => array(
            'name' => '意向户型', 
            'verify' => array( 'len' => 50,),
            'comment' => '意向户型','is_show' => true, 
        ),'kfdate' => array(
            'name' => '看房时间', 
            'verify' => array('null' => false,),
            'comment' => '看房时间。','is_show' => true, 
        ),'note' => array(
            'name' => '备注', 
            'verify' => array(),
            'comment' => '备注','is_show' =>true, 
        ),'parent_id' => array(
            'name' => '推荐用户', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '推荐用户','is_show' => true,'is_search' => true 
        ),'create_time' => array(
            'name' => '推荐时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10','is_show' => true, 
        ),'status' => array(
            'name' => '状态', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '状态','is_show' => true, 
        ),'author' => array(
            'name' => '维护人', 'default' => '0',
            'verify' => array( 'numeric' => true,),
            'comment' => '最近一次维护人员','is_show' => true, 
        ),);

}

?>
