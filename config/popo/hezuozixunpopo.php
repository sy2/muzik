<?php 

/**
 * @version			$Id$
 * @create 			2018-06-30 17:06:46 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class HezuozixunPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '合作咨询';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'hezuozixun';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = '';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_hezuozixun';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = '';
    
    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('id' => array(
            'name' => 'ID', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '只能是数字','is_show' => true, 'is_order' => 'DESC', 
        ),'parent_id' => array(
            'name' => '用户', 'default' => '0',
            'verify' => array( 'numeric' => true,),
            'comment' => '用户','is_show' => true, 
        ),'phone' => array(
            'name' => '电话', 
            'verify' => array('null' => false, 'len' => 255,),
            'comment' => '联系方式说明信息','is_show' => true, 
        ),'content' => array(
            'name' => '内容', 
            'verify' => array('null' => false,),
            'comment' => '联系方式内容。','is_show' => true, 
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10',
        ),'author' => array(
            'name' => '维护人', 'default' => '0',
            'verify' => array( 'numeric' => true,),
            'comment' => '最近一次维护人员','is_show' => true, 
        ),);

}

?>
