<?php 

/**
 * @version			$Id$
 * @create 			2017-09-21 16:09:21 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class WxsettingPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '微信公众号平台设置';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'wxsetting';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = 'shop';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_wx_setting';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';

    /**
     * 是否上架map
     */
    public static $encodeMethodMap    = array(
        1 => array('id' => 1, 'name' => '明文加密'),
        2 => array('id' => 2, 'name' => '密文加密')
    );

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('id' => array(
            'name' => 'ID', 
            'verify' => array(),
            'comment' => '只能是数字','is_show' => true, 'is_order' => 'DESC', 
        ),'shop_id' => array(
            'name' => '店铺', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '对应店铺','is_show' => true, 
        ),'name' => array(
            'name' => '公众号名称', 
            'verify' => array('null' => false, 'len' => 255,),
            'comment' => '请输入。','is_show' => true, 'is_search' => true, 
        ),'wx_name' => array(
            'name' => '微信号', 
            'verify' => array( 'len' => 50,),
            'comment' => '请输入准确','is_show' => true, 
        ),'app_id' => array(
            'name' => '开发者ID', 
            'verify' => array('null' => false, 'len' => 250,),
            'comment' => '请准确填写','is_show' => true, 
        ),'app_secret' => array(
            'name' => '开发者密码', 
            'verify' => array('null' => false, 'len' => 250,),
            'comment' => '请准确填写','is_show' => false, 
        ),'url' => array(
            'name' => '服务器地址', 
            'verify' => array('null' => false, 'len' => 250,),
            'comment' => '请准确填写','is_show' => true, 
        ),'token' => array(
            'name' => '令牌', 
            'verify' => array('null' => false, 'len' => 32,),
            'comment' => '请准确填写','is_show' => false, 
        ),'encoding_aeskey' => array(
            'name' => '消息加解密密钥', 
            'verify' => array( 'len' => 43,),
            'comment' => '请准确填写','is_show' => false, 
        ),'encode_method' => array(
            'name' => '消息加解密方式', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '请准确填写','is_show' => false, 
        ),'src_id' => array(
            'name' => '原始ID', 
            'verify' => array('null' => false,),
            'comment' => '请准确输入','is_show' => false, 
        ),'verify_txt' => array(
            'name' => '校验Txt文件', 
            'verify' => array( 'len' => 250,), 'file_name_enc' => 'src', 'no_date_dir' => 2,
            'comment' => '微信验证域名Txt文件。','is_show' => false, 'is_file' => true, 'size' => 0.5
        ),'image_path' => array(
            'name' => '微信二维码', 
            'verify' => array( 'len' => 250,),
            'comment' => '微信二维码图片。','is_show' => true, 'is_file' => true, 'zoom' => array('small' => array(100, 120)), 'type' => array('.png', '.jpg', '.gif'), 'size' => 0.5
        ),'welcome' => array(
            'name' => '关注欢迎词', 
            'verify' => array('null' => false, 'len' => 250,),
            'comment' => '请输入','is_show' => false, 
        ),'func_list' => array(
            'name' => '空关键词回复', 
            'verify' => array( 'len' => 250,),
            'comment' => '请准备输入','is_show' => false, 
        ),'wx_shop_name' => array(
            'name' => '商户名称', 
            'verify' => array( 'len' => 20,),
            'comment' => '请一定要保持填写同微信公众号平台“商户名称”一致！','is_show' => false, 
        ),'mchid' => array(
            'name' => '受理商ID', 
            'verify' => array( 'len' => 20,),
            'comment' => '请准确输入','is_show' => false, 
        ),'pay_key' => array(
            'name' => '商户支付密钥', 
            'verify' => array( 'len' => 32,),
            'comment' => '请准确输入','is_show' => false, 
        ),'wft_mchid' => array(
            'name' => '威富通商户号',
            'verify' => array( 'len' => 255,),
            'comment' => '威富通商户号','is_show' => false,
        ),'wft_key' => array(
            'name' => '威富通支付密钥',
            'verify' => array( 'len' => 255,),
            'comment' => '威富通支付密钥','is_show' => false,
        ),'sslcert_path' => array(
            'name' => '证书路径cert', 
            'verify' => array( 'len' => 250,),
            'comment' => '如：apiclient_cert.pem','is_show' => false, 'is_file' => true, 'size' => 2, 'type' => '*'
        ),'sslkey_path' => array(
            'name' => '证书路径key', 
            'verify' => array( 'len' => 250,),
            'comment' => '如：apiclient_key.pem','is_show' => false, 'is_file' => true, 'size' => 2, 'type' => array('.pem')
        ),'sslca_path' => array(
            'name' => '证书路径p12', 
            'verify' => array( 'len' => 250,),
            'comment' => '如：rootca.pem或apiclient_cert.p12','is_show' => false, 'is_file' => true, 'size' => 2, 'type' => array('.p12', '.pem')
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10','is_show' => true, 
        ),'author' => array(
            'name' => '维护人', 'default' => '0',
            'verify' => array( 'numeric' => true,),
            'comment' => '最近一次维护人员','is_show' => true, 
        ),);

}

?>
