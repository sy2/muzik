<?php 

/**
 * @version			$Id$
 * @create 			2015-10-24 12:10:49 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class WxmsgPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '微信推送';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'wxmsg';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = 'user';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_wx_msg';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('id' => array(
            'name' => 'ID', 
            'verify' => array(),
            'comment' => '只能是数字','is_show' => true, 'is_order' => 'DESC', 
        ),'shop_id' => array(
            'name' => '商家', 'default' => '0',
            'verify' => array('null' => true, 'numeric' => true,),
            'comment' => '所属商家ID','is_show' => false, 
        ),'name' => array(
            'name' => '称呼', 
            'verify' => array('null' => false, 'len' => 255,),
            'comment' => '长度范围：2~255。','is_show' => true, 'is_search' => true, 
        ),'parent_id' => array(
            'name' => '所属分类', 'default' => '1',
            'verify' => array('null' => true, 'numeric' => true,),
            'comment' => '请正确选取','is_show' => true, 
        ),'msg_type' => array(
            'name' => '消息类型', 
            'verify' => array('null' => false, 'len' => 50,),
            'comment' => '消息处理类型','is_show' => true, 
        ),'content' => array(
            'name' => '详细内容', 
            'verify' => array('null' => true,),
            'comment' => '长度10000字以内。','is_show' => true
        ),'raw' => array(
            'name' => '原数据', 
            'verify' => array('null' => false,),
            'comment' => '原始数据','is_show' => false, 
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => true,),
            'comment' => '格式：2013-04-10','is_show' => true, 
        ),'author' => array(
            'name' => '维护人', 'default' => '0',
            'verify' => array('null' => true, 'numeric' => true,),
            'comment' => '最后一次修改人员','is_show' => true, 
        ),);

}

?>
