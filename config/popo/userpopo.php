<?php 

/**
 * @version			$Id$
 * @create 			2016-04-10 12:04:59 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class UserPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '用户';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'user';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = 'actor';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_user';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';

    public $dataGridType        = 4;

    /**
     * @var public static $shopVerifyMap 店铺证认证情况
     */
    public static $shopVerifyMap = array(
        '0' => array('id' => '0', 'name' => '未添加', 'label' => 'label-info'),
        '1' => array('id' => '1', 'name' => '等待审核', 'label' => 'label-warning'),
        '2' => array('id' => '2', 'name' => '已审核', 'label' => 'label-success'),
        '3' => array('id' => '3', 'name' => '审核失败', 'label' => 'label-danger')
    );

    /**
     * @var public static $sexMap   性别映射
     */
    public static $sexMap   = array(
        '1' => array('id' => '1', 'name' => '男'),
        '2' => array('id' => '2', 'name' => '女')
    );

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array(
        'id' => array(
            'name' => 'ID', 
            'verify' => array(),
            'comment' => '系统编号','is_show' => true, 'is_order' => 'DESC',
        ),'name' => array(
            'name' => '账号', 
            'verify' => array('null' => false, 'len' => 50,),
            'comment' => '登录系统使用的账号','is_show' => true, 'is_search' => true
        ),'true_name' => array(
            'name' => '代理/公司名字',
            'verify' => array( 'len' => 50,),
            'comment' => '代理/公司名字','is_show' => true,  'is_search' => true
        ),'password' => array(
            'name' => '密码', 
            'verify' => array( 'len' => 32,),
            'comment' => '登录系统密码，6-20位字符，设空自动生成。',
        ),'parent_id' => array(
            'name' => '类别',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '用户所具有的权限','is_show' => true
        ),'salt' => array(
            'name' => '加密因子', 
            'verify' => array('null' => false, 'len' => 5,),
            'comment' => '配合密码的加密因子，固定5个字符长度！','is_show' => false, 
        ),'sex' => array(
            'name' => '性别', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '用户性别：1，男；2，女；3. 其它。','is_show' => true, 
        ),'description' => array(
            'name' => '简介', 
            'verify' => array( 'len' => 255,),
            'comment' => '用户信息简介',
        ),'position' => array(
            'name' => '地址',
            'verify' => array( 'len' => 255,),
            'comment' => '地址',
        ),'image_path' => array(
            'name' => '头像', 
            'verify' => array( 'len' => 255,),
            'comment' => '用户头像,支持jpg','is_show' => true, 'is_file' => true, 'zoom' => array('small' => array(320, 320)), 'type' => array('.png', '.jpg', '.gif'), 'size' => 0.5
        ),'birthday' => array(
            'name' => '出生日期', 
            'verify' => array(),
            'comment' => '出生日期','is_show' => false,
        ),'area' => array(
            'name' => '认证情况',
            'verify' => array( 'len' => 255,),
            'comment' => '认证情况','is_show' => false,
        ),'position' => array(
            'name' => '地址',
            'verify' => array('null' => true, 'len' => 255,),
            'comment' => '地址','is_show' => false,
        ),'province' => array(
            'name' => 'LOGO',
            'verify' => array( 'len' => 255,),
            'comment' => 'LOGO','is_show' => false, 'is_file' => true, 'zoom' => array('small' => array(320, 320)), 'type' => array('.png', '.jpg', '.gif'), 'size' => 0.5
        ),'city' => array(
            'name' => '市', 
            'verify' => array( 'len' => 255,),
            'comment' => '所在市','is_show' => false, 
        ),'district' => array(
            'name' => '区、乡（镇）', 
            'verify' => array( 'len' => 255,),
            'comment' => '所在区、乡（镇）','is_show' => false, 
        ),'street' => array(
            'name' => '街道地址', 
            'verify' => array( 'len' => 255,),
            'comment' => '所在街道地址','is_show' => false, 
        ),'street_number' => array(
            'name' => '街道编号', 
            'verify' => array( 'len' => 255,),
            'comment' => '得道编号信息','is_show' => false, 
        ),'qq' => array(
            'name' => 'QQ', 
            'verify' => array( 'len' => 20,),
            'comment' => '常用QQ号','is_show' => false,  'is_search' => true
        ),'weixin' => array(
            'name' => '售后服务号码',
            'verify' => array( 'len' => 255,),
            'comment' => '售后服务号码','is_show' => false,
        ),'email' => array(
            'name' => '邮箱', 
            'verify' => array( 'len' => 50,),
            'comment' => '常用邮箱地址','is_show' => false,  'is_search' => true
        ),'phone' => array(
            'name' => '联系方式',
            'verify' => array('null' => false,),
            'comment' => '前台咨询号码','is_show' => true,  'is_search' => true
        ),'money' => array(
            'name' => '佣金',
            'comment' => '佣金','is_show' => true,
        ),'hash' => array(
            'name' => '哈希', 
            'verify' => array( 'len' => 60,),
            'comment' => '记录用户的登陆状态',
        ),'login_time' => array(
            'name' => '登陆时间', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '最近一次修改时间',
        ),'ip' => array(
            'name' => '登陆IP', 'default' => '127.0.0.1',
            'verify' => array('null' => true, 'len' => 128,),
            'comment' => '上一次登陆IP','is_show' => false, 
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10 08:09:09', true, 
        ),'author' => array(
            'name' => '维护员', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '上一次修改的管理员','is_show' => true, 
        ),);

}

?>
