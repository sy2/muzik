<?php 

/**
 * @version			$Id$
 * @create 			2016-09-11 12:09:15 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class GoodsPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '产品';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'goods';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = 'goodscategory';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_goods';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';

    /**
     * @var public static $statusMap    商品状态映射
     */
    public static $statusMap    = array(
        '1' => array('id' => '1', 'name' => '下架'),
        '2' => array('id' => '2', 'name' => '上架'),
        '3' => array('id' => '3', 'name' => '删除')
    );


    /**
     * 商品推荐位置映射
     * @var array
     */
    public static $positionMap  = array(
        '0' => array('id' => '0', 'name' => '不推荐'),
        '1' => array('id' => '1', 'name' => '热卖推荐')
    );

    /**
     * 是否预售Map
     * @var array
     */
    public static $isYushouMap  = array(
        '1' => array('id' => '1', 'name' => '否'),
        '2' => array('id' => '2', 'name' => '是')
    );

    /**
     * 是否必须附加
     * @var array
     */
    public static $isMustMap  = array(
        '1' => array('id' => '1', 'name' => '否'),
        '2' => array('id' => '2', 'name' => '在店必须，默认为1件'),
        '3' => array('id' => '3', 'name' => '在店必须，默认为1/2用餐人数'),
        '4' => array('id' => '4', 'name' => '在店必须，默认为用餐人数'),
        '5' => array('id' => '5', 'name' => '外送必须，默认1件'),
        '6' => array('id' => '6', 'name' => '外送必须，默认等于菜件数'),
        '7' => array('id' => '7', 'name' => '都必须，默认1件')
    );

    /**
     * 产品类型
     * @var array
     */
    public static $priceMap  = array(
        1 => array('id' => 1, 'min' => 0, 'max' => 30, 'name' => '0-30元'),
        2 => array('id' => 2, 'min' => 30, 'max' => 50, 'name' => '30-50元'),
        3 => array('id' => 3, 'min' => 50, 'max' => 100, 'name' => '50-100元'),
        4 => array('id' => 4, 'min' => 100, 'max' => 200, 'name' => '100-200元'),
        5 => array('id' => 5, 'min' => 200, 'max' => 500, 'name' => '200-500元'),
        6 => array('id' => 6, 'min' => 500, 'max' => 1000, 'name' => '500-1000元'),
        7 => array('id' => 7, 'min' => 1000, 'max' => 0, 'name' => '1000元以上'),
    );

    /**
     * 户型
     * @var array
     */
    public static $huxingMap  = array(
        '1' => array('id' => '1', 'name' => '一室'),
        '2' => array('id' => '2', 'name' => '两室'),
        '3' => array('id' => '3', 'name' => '三室'),
        '4' => array('id' => '4', 'name' => '四室'),
        '5' => array('id' => '5', 'name' => '五室'),
    );

    /**
     * 购房关注点
     * @var array
     */
    public static $guanzhuMap  = array(
        '1' => array('id' => '1', 'name' => '地段'),
        '2' => array('id' => '2', 'name' => '学区'),
        '3' => array('id' => '3', 'name' => '周边配套'),
        '4' => array('id' => '4', 'name' => '区域发展前景'),
        '5' => array('id' => '5', 'name' => '居住环境'),
        '6' => array('id' => '6', 'name' => '精装修'),
        '7' => array('id' => '7', 'name' => '现房'),
        '8' => array('id' => '8', 'name' => '物业服务'),
    );
    
    /**
     * @var public static $isNewMap   是否新品映射
     */
    public static $isNewMap    = array(
        '1' => array('id' => '1', 'name' => '否'),
        '2' => array('id' => '2', 'name' => '是')
    );

    /**
     * @var public static $isNewMap   是否新品映射
     */
    public static $isShowMap    = array(
        '1' => array('id' => '1', 'name' => '否'),
        '2' => array('id' => '2', 'name' => '是')
    );

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('sort_num' => array(
            'name' => '排序',
            'verify' => array(), 'default' => 99999,
            'comment' => '只能是数字，默认为：99999。','is_show' => true, 'is_order' => 'ASC',
        ),'id' => array(
            'name' => 'ID', 
            'verify' => array(),
            'comment' => '只能是数字','is_show' => true, 'is_order' => 'DESC', 
        ),'name' => array(
            'name' => '名称', 
            'verify' => array('null' => false, 'len' => 255,),
            'comment' => '商品名称','is_show' => true, 'is_search' => true,
        ),'kuan' => array(
            'name' => '宽', 'default' => '0',
            'verify' => array('null' => false, 'len' => 100,),
            'comment' => '宽','is_show' => false,
        ),'zhai' => array(
            'name' => '窄', 'default' => '0',
            'verify' => array('null' => false, 'len' => 100,),
            'comment' => '窄','is_show' => false,
        ),'height' => array(
            'name' => '高', 'default' => '10',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '高','is_show' => false,
        ),'parent_id' => array(
            'name' => '分类', 'default' => 0 ,
            'verify' => array('null' => true, 'numeric' => false,),
            'comment' => '分类','is_show' => true,
        ),'length' => array(
            'name' => '长度', 'default' => '10',
            'verify' => array( 'numeric' => false,),
            'comment' => '长度','is_show' => false,
        ),'color_id' => array(
            'name' => '颜色值', 'default' => '0',
            'comment' => '颜色值','is_show' => false,
        ),'parent_path' => array(
            'name' => '层级关系', 
            'verify' => array( 'len' => 50,),
            'comment' => '分类层级关系','is_show' => false, 
        ),'rim' => array(
            'name' => '边框', 'default' => '',
            'comment' => '请准确输入','is_show' => false,
        ),'shape' => array(
            'name' => '形状', 'default' => '0',
            'comment' => '形状','is_show' => false,
        ),'material' => array(
            'name' => '材质', 'default' => '0.00',
            'verify' => array('null' => false,),
            'comment' => '请准确输入','is_show' => false, 
        ),'content' => array(
            'name' => '详细内容介绍',
            'verify' => array('null' => false,),
            'comment' => '服务详细介绍',
        ),'attr_data' => array(
            'name' => '交货天数', 'default' => 1,
            'verify' => array('null' => false,'number'=>true),
            'comment' => '','is_show' => false
        ),'hash' => array(
            'name' => '相册编号', 'default'=>'hash',
            'comment' => '相册校验码','zoom' => array('small' => array(300, 320)), 'type' => array('.png', '.jpg', '.gif', '.bmp'), 'size' => 0.5,
            'limit' => 5
        ),'image_path' => array(
            'name' => '封面', 
            'verify' => array( 'len' => 255,),
            'zoom' => array('middel' => array(300, 320), 'small' => array(440, 460)), 
            'type' => array('.png', '.jpg', '.gif', '.bmp'), 'size' => 0.5,
            'comment' => '大小要求：宽480px x 高 480px','is_show' => true, 
        ),'adv_img' => array(
            'name' => '推荐广告图', 
            'is_file' => true,
            'verify' => array( 'len' => 255,),
            'zoom' => array('middel' => array(300, 320), 'small' => array(440, 460)), 'type' => array('.png', '.jpg', '.gif', '.bmp'), 
            'size' => 0.4,
            'comment' => '大小要求：宽750px x 高 360px','is_show' => false, 
        ),'is_new' => array(
            'name' => '是否新品', 'default' => '1',
            'verify' => array('null' => false,),
            'comment' => '是否为新品。','is_show' => true, 
        ),'is_show' => array(
            'name' => '是否推荐', 'default' => '1',
            'verify' => array('null' => false,),
            'comment' => '是否推荐。','is_show' => true,
        ),'buy_type' => array(
            'name' => '购买方式', 'default' => '1',
            'verify' => array('null' => false,),
            'comment' => '购买方式','is_show' => false,
        ),'score' => array(
            'name' => '总体评分', 'default' => '5.00',
            'verify' => array('null' => false,),
            'comment' => '服务总体评分',
        ),'zhiliang_score' => array(
            'name' => '商品质量', 'default' => '0.00',
            'verify' => array('null' => false,),
            'comment' => '质量打分',
        ),'taidou_score' => array(
            'name' => '服务态度', 'default' => '0.00',
            'verify' => array('null' => false,),
            'comment' => '态度打分','is_show' => false,
        ),'time_score' => array(
            'name' => '配送准时', 'default' => '0.00',
            'verify' => array('null' => false,),
            'comment' => '配送打分',
        ),'tui_rate' => array(
            'name' => '奖励金额', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '奖励金额','is_show' => false,
        ),'total_number' => array(
            'name' => '售后服务', 'default' => '0',
            'verify' => array('null' => false),
            'comment' => '库存数量','is_show' => false,
        ),'total_comments' => array(
            'name' => '总评价数', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '评价统计次数','is_show' => false, 
        ),'total_great' => array(
            'name' => '起订数量', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '点赞总数','is_show' => false, 
        ),'total_save' => array(
            'name' => '收藏数', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '收藏数统计','is_show' => false, 
        ),'total_orders' => array(
            'name' => '前台咨询', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '前台咨询','is_show' => false
        ),'total_visits' => array(
            'name' => '访问量', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '总访问量统计','is_show' => false, 
        ),'status' => array(
            'name' => '是否上架', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '1 下架 2 上架','is_show' => true, 
        ),'ext_1' => array(
            'name' => '佣金', 
            'verify' => array( 'len' => 50,),
            'comment' => '佣金','is_show' => false,
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10',
        ),'author' => array(
            'name' => '维护员', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '所属会员',
        ),'is_pintuan' => array(
            'name' => '是否组团', 'default' => '·',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '是否组团',
        ),'is_pintuan' => array(
            'name' => '是否组团', 'default' => '1',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '是否组团',
        ),'printer_id' => array(
            'name' => '组团人数', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '组团人数',
        ),'extend' => array(
            'name' => '房型信息', 'default' => '',
            'comment' => '房型信息',
        ));
}
?>
