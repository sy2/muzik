<?php 

/**
 * @version			$Id$
 * @create 			2017-10-29 10:10:58 By xjiujiu 
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

/**
 * 模块工具的基本信息类 
 * 
 * 用于记录单模块的配置信息 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		config.popo
 * @since 			1.0.0
 */
class WxaccesstokenPopo extends HPopo
{

    /**
     * @var string $modelZhName 模块中文名称 
     */
    public $modelZhName         = '微信认证口令';

    /**
     * @var string $modelEnName 模块英文名称 
     */
    public $modelEnName         = 'wxaccesstoken';

    /**
     * @var string $_parentTable 父表名 
     */
    protected $_parent          = '';

    /**
     * @var string $_table 模块表名 
     */
    protected $_table           = '#_wx_accesstoken';

    /**
     * @var string $primaryKey 表主键
     */
    public $primaryKey          = 'id';

    /**
     * @var array $_fields 模块字段配置 
     */
    protected $_fields          = array('id' => array(
            'name' => 'ID', 
            'verify' => array(),
            'comment' => '只能是数字','is_show' => true, 'is_order' => 'DESC', 
        ),'type' => array(
            'name' => '类别', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '1微信,2qq','is_show' => true, 
        ),'shop_id' => array(
            'name' => '店铺', 'default' => '0',
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '对应店铺','is_show' => true, 
        ),'name' => array(
            'name' => 'Token', 
            'verify' => array('null' => false, 'len' => 255,),
            'comment' => '请输入','is_show' => true, 'is_search' => true, 
        ),'parent_id' => array(
            'name' => '用户', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '对应用户','is_show' => true, 
        ),'content' => array(
            'name' => '详细内容', 
            'verify' => array('null' => false,),
            'comment' => '长度10000字以内。',
        ),'end_time' => array(
            'name' => '结束时间', 
            'verify' => array('null' => false, 'numeric' => true,),
            'comment' => '请准确输入','is_show' => true, 
        ),'create_time' => array(
            'name' => '创建时间', 
            'verify' => array('null' => false,),
            'comment' => '格式：2013-04-10','is_show' => true, 
        ),'author' => array(
            'name' => '维护人', 'default' => '0',
            'verify' => array( 'numeric' => true,),
            'comment' => '最近一次维护人员','is_show' => true, 
        ),);

}

?>
