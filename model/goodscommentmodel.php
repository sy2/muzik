<?php 

/**
 * @version			$Id$
 * @create 			2015-10-07 11:10:29 By xjiujiu
 * @description     HongJuZi Framework
 * @copyRight 		Copyright (c) 2011-2012 http://www.xjiujiu.com.All right reserved
 */
defined('_HEXEC') or die('Restricted access!');

//导入引用文件
HClass::import('model.BaseModel');

/**
 * 家教评价模块 
 * 
 * 自动生成模块对应的类及数据库表,实现简单的CURD功能 
 * 
 * @author 			xjiujiu <xjiujiu@foxmail.com>
 * @package 		model
 * @since 			1.0.0
 */
class GoodscommentModel extends BaseModel
{

    /**
     * 得到评分平均分
     * 
     * @author xjiujiu <xjiujiu@foxmail.com>
     * @access public
     * @param $where 条件
     * @return Array 评分信息
     */
    public function getScoreAvg($where)
    {
        $this->_db->getSql()
            ->table($this->_popo->get('table'))
            ->fields('AVG(`score`) AS score, AVG(`time_score`) AS time_score, AVG(`zhiliang_score`) AS zhiliang_score, AVG(`taidou_score`) as taidou_score')
            ->where(
                HSqlHelper::mergeWhere(array($this->_getMustWhere(), $where), 'AND')
            );
        
        return $this->_db->select()->getRecord();
    }

}

?>
